from map.models import (
    AreeProtette,Comuni,Province
)
from biodiv.models import Observation
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometryField
from rest_framework_gis.fields import GeometryField

from biostream.serializers import DynamicFieldsMixin


class AreeProtetteSerializer(DynamicFieldsMixin, GeoFeatureModelSerializer):
    """
    Serializzatore per le geometrie delle aree protette entro il Progetto 
    """
    geom = GeometryField(precision=0)

    class Meta:
        model = AreeProtette
        auto_bbox = True
        geo_field = 'geom'
        id_field = 'id'
        fields = ('id', 'geom','name')

class ProvinceSerializer(DynamicFieldsMixin, GeoFeatureModelSerializer):
    """
    Serializzatore per le geometrie delle province entro il Progetto 
    """
    geom = GeometryField(precision=0)

    class Meta:
        model = Province
        auto_bbox = True
        geo_field = 'geom'
        id_field = 'id'
        fields = ('id','prov_name', 'geom',)

class ComuniSerializer(DynamicFieldsMixin, GeoFeatureModelSerializer):
    """
    Serializzatore per le geometrie dei comuni entro il Progetto 
    """
    geom = GeometryField(precision=0)

    class Meta:
        model = Comuni
        auto_bbox = True
        geo_field = 'geom'
        id_field = 'id'
        fields = ('id','com_name', 'geom',)

class AreeProtetteCheckListSerializer(DynamicFieldsMixin, ModelSerializer):
    """
    Serializzatore per gli attributi non geometrici delle aree protette entro il progetto
    """
    class Meta:
        model = AreeProtette
        fields = ('code', 'specie', 'habitat',
                  'specie_num', 'habitat_num', 'biogeographic_region', 'administrative_region', 'site_type', 'sic_zsc', 'zps',)

class ObservationCheckListSerializer(DynamicFieldsMixin, ModelSerializer):
    classname = serializers.CharField(read_only=True) 
    species = serializers.CharField(read_only=True)
    species_n = serializers.IntegerField(read_only=True)

    class Meta:
        model = Observation
        fields = ('classname', 'species', 'species_n',)

class ObservationCheckListDetailedSerializer(DynamicFieldsMixin, ModelSerializer):
    classname = serializers.CharField(read_only=True) 
    specie = serializers.CharField(read_only=True)
    n = serializers.IntegerField(read_only=True)

    class Meta:
        model = Observation
        fields = ('classname', 'specie', 'n',)