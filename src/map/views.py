from dal import autocomplete
from django.views.generic.base import TemplateView
from django.contrib.gis.db.models import F, Subquery,Q

from biodiv.models import (
    Observation, Institution, DataOrigin
)
from taxon.models import TaxonomyNormalized, TaxonProtectionNormalized
from lu_tables.models import LuBaseOss
from map.models import AreeProtette, BioSTREAMArea, Comuni
from .models import AreeSearch

class GisPage(TemplateView):
    """
    La vista estrae dei dati di contesto utili ai filtri
    per la pagina WebGIS

    .. image:: ../../_static/manual/biostream_4.png
        :width: 400
        :alt: Immagine dei filtri web popolati dalla vista

    """
    template_name = "gis.html"

    def get_context_data(self, **kwargs):
        """
        Ritorna informazioni di contesto estratte dal totale delle osservazioni
        """
        context = super().get_context_data(**kwargs)

        # For filters
        regions = DataOrigin.objects.prefetch_related('institution_set').order_by(
            'institution__region').values('institution__region').distinct()
        context['dataorigins_regions'] = {}
        for region in regions:
            context['dataorigins_regions']["{}".format(region["institution__region"])] = DataOrigin.objects.prefetch_related('institution_set').order_by(
                'origin_name', 'institution__region').values('id', 'origin_name').filter(institution__region=region['institution__region'])

        context['institutions'] = Institution.objects.values(
            'id', 'institution_name').order_by('institution_name')
        context['year_max'] = Observation.objects.filter(date_year__isnull=False).order_by('-date_year').values_list('date_year')[0][0]
        context['year_min'] = Observation.objects.filter(date_year__isnull=False).order_by('date_year').values_list('date_year')[0][0]
        baseoss = Observation.objects.values_list(
            'baseoss', flat=True).distinct().exclude(baseoss=None)
        baseoss = list(
            set([words for segments in baseoss for words in segments.replace(' / ', '/').split('/')]))
        baseoss.sort()
        context['baseoss'] = LuBaseOss.objects.filter(code__in=baseoss).values('code', 'description')
        return context


class CheckListPage(TemplateView):

    template_name = "checklist.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sic_zps'] = AreeProtette.objects.filter(geom__intersects=Subquery(
            BioSTREAMArea.objects.values('geom')
        )).distinct('code', 'name').values('code', 'name').order_by('name')
        return context
####
# AUTOCOMPLETE VIEWS
####

class AreeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = AreeSearch.objects.all()
        if self.q:
            qs = qs.filter(nome__icontains=self.q)
        return qs

    def get_results(self, context):
        return [
            {
                'id': self.get_result_value(result),
                'text': self.get_result_label(result),
                'type': str(result.tipo_auto),
                'area_id': str(result.area_id),
                'selected_text': self.get_selected_result_label(result),
            } for result in context['object_list']
        ]

class ComuniAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Comuni.objects.all()
        if self.q:
            qs = qs.filter(com_name__icontains=self.q)
        return qs