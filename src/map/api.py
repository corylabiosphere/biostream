from django.db.models import F, Count, TextField, ExpressionWrapper
from django.contrib.postgres.aggregates import StringAgg
from djgeojson.serializers import Serializer as GeoJSONSerializer

from rest_framework import (viewsets, permissions)
#from rest_framework_filters.backends import RestFrameworkFilterBackend
from rest_framework.settings import api_settings

from rest_framework_gis.filters import InBBoxFilter
from rest_framework_csv import renderers as r

from map.models import (
    AreeProtette, Comuni, Province
)
from biodiv.models import Observation
from .serializers import (
    AreeProtetteSerializer, AreeProtetteCheckListSerializer,ComuniSerializer, ObservationCheckListSerializer, ProvinceSerializer, ObservationCheckListDetailedSerializer
)
from .filters import ObservationsFilterOnlyGeom
from map.models import Comuni, AreeProtette, AreeProtetteCheckList

app_name = __package__

def filterbyarea(queryset,self):
    areaprotetta = self.request.query_params.get('areaprotetta',None)
    comune = self.request.query_params.get('comune',None)
    provincia = self.request.query_params.get('provincia',None)
    geometry = self.request.query_params.get('geometry',None)
    geom = None
    if areaprotetta:
        geom = AreeProtette.objects.filter(id=areaprotetta).values_list('geom',flat=True)[0]
    if comune:
        geom = Comuni.objects.filter(id=comune).values_list('geom',flat=True)[0]
    if provincia:
        geom = Province.objects.filter(id=provincia).values_list('geom',flat=True)[0]
    if geometry:
        geom = geometry
    if geom:
        queryset =  queryset.filter(geom__intersects=geom) 
    return queryset   


class AreeProtetteViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista geometrica per le aree protette entro il progetto
    """
    queryset =  AreeProtette.objects.all().order_by('name')
    serializer_class = AreeProtetteSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['id','name']

class AreeProtetteCheckListViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista geometrica per le aree protette entro il progetto
    """
    queryset =  AreeProtetteCheckList.objects.all().order_by('name')
    serializer_class = AreeProtetteCheckListSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['code']

class ComuniViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista geometrica (lenta) per le aree protette entro il progetto
    """
    queryset =  Comuni.objects.all().order_by('com_name')
    serializer_class = ComuniSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['com_name','id']

class ProvinceViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista geometrica (lenta) per le aree protette entro il progetto
    """
    queryset =  Province.objects.all().order_by('prov_name')
    serializer_class = ProvinceSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['prov_name','id']

class ObservationChecklistViewSet(viewsets.ReadOnlyModelViewSet):
    queryset =  Observation.objects.filter(taxon_rank='SPECIES')
    serializer_class = ObservationCheckListSerializer
    filter_class = ObservationsFilterOnlyGeom
    filter_backends = (InBBoxFilter,)
    permission_classes = [permissions.AllowAny]
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.CSVRenderer, )

    def paginate_queryset(self, queryset, view=None):
        if 'no_page' in self.request.query_params:
            return None
        else:
            return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_queryset(self):             
        queryset = super(ObservationChecklistViewSet, self).get_queryset()
        queryset = filterbyarea(queryset,self)            
        queryset = queryset.annotate(
            classname=ExpressionWrapper(F('taxon__nameusage__class_fk__name__canonical_name'),output_field=TextField()),specie=ExpressionWrapper(F('taxon__canonical_name'),output_field=TextField())
        )
        queryset =  queryset.values('classname').annotate(
            species=ExpressionWrapper(StringAgg('specie',distinct=True,delimiter=','),output_field=TextField()),species_n=Count('specie',distinct=True)#,species_ids=ArrayAgg('taxon_id',distinct=True,delimiter=',')
        ).order_by('classname') # order_by is needed to force the group_by

        return queryset

class ObservationChecklistDetailedViewSet(viewsets.ReadOnlyModelViewSet):
    queryset =  Observation.objects.filter(taxon_rank='SPECIES')
    serializer_class = ObservationCheckListDetailedSerializer
    filter_backends = (InBBoxFilter,)
    filter_class = ObservationsFilterOnlyGeom
    permission_classes = [permissions.AllowAny]
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.CSVRenderer, ) 

    def paginate_queryset(self, queryset, view=None):
        if 'no_page' in self.request.query_params:
            return None
        else:
            return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_queryset(self):             
        queryset = super(ObservationChecklistDetailedViewSet, self).get_queryset()
        queryset = filterbyarea(queryset,self)            
        queryset = queryset.annotate(classname=ExpressionWrapper(F('taxon__nameusage__class_fk__name__canonical_name'),output_field=TextField()),specie=ExpressionWrapper(F('taxon__canonical_name'),output_field=TextField())
        )
        queryset =  queryset.values('classname','specie').annotate(
            n=Count('specie')#,species_ids=ArrayAgg('taxon_id',distinct=True,delimiter=',')
        ).order_by('classname','specie')

        return queryset