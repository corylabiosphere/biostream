from rest_framework_gis.filterset import GeoFilterSet
from rest_framework_gis.filters import GeometryFilter
from biodiv.models import (
    Observation
)

class ObservationsFilterOnlyGeom(GeoFilterSet):
    geometry = GeometryFilter(
        field_name='geom',
        lookup_expr='intersects',
        help_text="""Filtro per le specie i cui punti di presenza intersecano la geometria data in input"""
    )
    
    class Meta:
        model = Observation
        fields = ['geometry']