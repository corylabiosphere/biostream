from django.contrib.admin import ModelAdmin, register
from map.models import Comuni
from leaflet.admin import LeafletGeoAdminMixin


@register(Comuni)
class ComuniAdmin(LeafletGeoAdminMixin, ModelAdmin):
    list_display = ('com_name', 'prov_name', 'reg_name',)
    readonly_fields = ('id',)

    search_fields = ('com_name',)
    #list_filter = ('attivo', ('regione', RelatedOnlyFieldListFilter),)
    list_filter = ('prov_name', 'reg_name',)
    ordering = ('com_name',)