from django.contrib.gis.db import models
from biostream.models import (
    ShortTextField,GeoModel
)


class BioSTREAMArea(GeoModel):
    """
    Il modello immagazzina l'area del Progetto BioSTREAM
    """
    id = models.AutoField("ID", primary_key=True)
    geom = models.PolygonField("Geometria", blank=True, null=True, srid=3035)

    class Meta:
        managed = False
        db_table = '"data_adm"."biostream_area"'
        verbose_name = 'Area Progetto BioSTREAM'
        verbose_name_plural = 'Aree Progetto BioSTREAM'
        ordering = ['id']
        app_label = 'biostream'

    def __str__(self):
        return "%s" % (self.id)


class AreeProtetteCheckList(GeoModel):
    """
    Il modello rende disponibili vari dettagli relativi alle aree protette,
    comprese alcune informazioni riassuntive delle stesse. ATTENZIONE:
    i dettagli riassuntivi vengono estratti da una vista materializzata del database
    """
    id = models.IntegerField("ID",primary_key=True)
    code = ShortTextField("Codice")
    site_type = ShortTextField("Tipo sito", blank=True, null=True)
    name = ShortTextField("Denominazione", blank=True, null=True)
    biogeographic_region = ShortTextField("Regione biogeografica", blank=True, null=True)
    administrative_region = ShortTextField("Regione", blank=True, null=True)
    sic_zsc = ShortTextField("SIC o ZSC?", blank=True, null=True)
    zps = ShortTextField("ZPS?", blank=True, null=True)
    geom = models.MultiPolygonField("Geometria", blank=True, null=True, srid=3035)
    specie = models.TextField("Lista delle specie", blank=True, null=True)
    habitat = models.TextField("Lista degli habitat", blank=True, null=True)
    specie_num = models.TextField("Numero delle specie", blank=True, null=True)
    habitat_num = models.TextField("Numero di habitat", blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'data_biodiv"."areeprotette_checklist'
        verbose_name = 'Checklist SIC / ZPS'
        verbose_name_plural = 'Checklist SIC / ZPS'
        ordering = ['code']
        app_label = 'biostream'

    def __str__(self):
        if self.sic_zsc and self.zps:
            return "%s/%s - %s" % (self.sic_zsc, self.zps, self.code)
        elif self.sic_zsc:
            return "%s - %s" % (self.sic_zsc, self.code)
        elif self.zps:
            return "%s - %s" % (self.zps, self.code)
        else:
            return "%s" % (self.code)

class AreeProtette(GeoModel):
    """
    Modello che include le aree protette entro i confini del Progetto. Non creabile come vista materializzata a causa di dipendenze, quindi:
    /*
    CREATE TABLE data_adm.biostream_area_areeprotette
    TABLESPACE pg_default
    AS
    SELECT it_sic_zps.id,it_sic_zps.code,it_sic_zps.name,it_sic_zps.sic_zsc,
        it_sic_zps.zps,it_sic_zps.geom
    FROM it_sic_zps,biostream_area
    WHERE st_intersects(it_sic_zps.geom, biostream_area.geom)
    WITH DATA;

    ALTER TABLE data_adm.biostream_area_areeprotette
        ADD CONSTRAINT biostream_area_areeprotette_pk PRIMARY KEY (id);
    CREATE INDEX IF NOT EXISTS biostream_area_areeprotette_geom_idx
        ON data_adm.biostream_area_areeprotette USING gist
        (geom);                
    */
    """
    id = models.IntegerField("ID",primary_key=True)
    code = ShortTextField("Codice")
    name = ShortTextField("Nome")
    sic_zsc = ShortTextField("SIC o ZSC?")
    zps = ShortTextField("ZPS?")
    geom = models.MultiPolygonField("Geometria", blank=True, null=True, srid=3035)

    class Meta:
        managed = False
        db_table = 'data_adm"."biostream_area_areeprotette'
        verbose_name = 'SIC / ZPS / ZSC'
        verbose_name_plural = 'SIC / ZPS / ZSC'
        ordering = ['id']
        app_label = 'biostream'

    def __str__(self):
        if self.sic_zsc and self.zps:
            return "%s/%s - %s" % (self.sic_zsc, self.zps, self.code)
        elif self.sic_zsc:
            return "%s - %s" % (self.sic_zsc, self.code)
        elif self.zps:
            return "%s - %s" % (self.zps, self.code)
        else:
            return "%s" % (self.code)

class Comuni(GeoModel):
    """
    Modello che include i comuni entro i confini del Progetto. Non creabile come vista materializzata a causa di dipendenze, quindi:
    /*
    CREATE TABLE data_adm.biostream_area_comuni
    TABLESPACE pg_default
    AS
    SELECT it_comuni.id,it_comuni.reg_cod,it_comuni.prov_cod,it_comuni.com_name,
        it_comuni.com_cod,it_comuni.prov_name,it_comuni.reg_name,it_comuni.geom
    FROM it_comuni,biostream_area
    WHERE st_intersects(it_comuni.geom, biostream_area.geom)
    WITH DATA;

    ALTER TABLE data_adm.biostream_area_comuni
        ADD CONSTRAINT biostream_area_comuni_pk PRIMARY KEY (id);
    CREATE INDEX IF NOT EXISTS biostream_area_comuni_geom_idx
        ON data_adm.biostream_area_comuni USING gist
        (geom);        
    */    
    """
    id = models.AutoField("ID", primary_key=True)
    com_name = models.TextField("Comune")
    prov_name = models.TextField("Provincia")
    prov_name = models.TextField("Denominazione")
    reg_name = models.TextField("Regione")
    geom = models.MultiPolygonField("Geometria", blank=True, null=True, srid=3035)

    class Meta:
        managed = False
        db_table = '"data_adm"."biostream_area_comuni"'
        verbose_name = 'Comune entro il Progetto BioSTREAM'
        verbose_name_plural = 'Comuni entro il Progetto BioSTREAM'
        ordering = ['com_name']
        app_label = 'biostream'

    def __str__(self):
        return "%s" % (self.com_name)

class Province(GeoModel):
    """
    Modello che include le province entro i confini del Progetto. Non creabile come vista materializzata a causa di dipendenze, quindi:
    /*
    CREATE TABLE data_adm.biostream_area_province
    TABLESPACE pg_default
    AS
    SELECT it_province.id,it_province.cod_reg,it_province.cod_prov,it_province.den_uts,it_province.geom
    FROM it_province,biostream_area
    WHERE st_intersects(it_province.geom, biostream_area.geom)
    WITH DATA;

    ALTER TABLE data_adm.biostream_area_province
        ADD CONSTRAINT biostream_area_province_pk PRIMARY KEY (id);
    CREATE INDEX IF NOT EXISTS biostream_area_province_geom_idx
        ON data_adm.biostream_area_province USING gist
        (geom);
    */    
    """
    id = models.AutoField("ID", primary_key=True)
    cod_reg = models.IntegerField("Codice Regione")
    cod_prov = models.IntegerField("Codice Provincia")
    prov_name = models.TextField("Denominazione",db_column='den_uts')
    geom = models.MultiPolygonField("Geometria", blank=True, null=True, srid=3035)

    class Meta:
        managed = False
        db_table = '"data_adm"."biostream_area_province"'
        verbose_name = 'Province entro il Progetto BioSTREAM'
        verbose_name_plural = 'Province entro il Progetto BioSTREAM'
        ordering = ['prov_name']
        app_label = 'biostream'

    def __str__(self):
        return "%s" % (self.prov_name)


class Regioni(GeoModel):
    """
    Modello che include le regioni entro i confini del Progetto. Non creabile come vista materializzata a causa di dipendenze, quindi:
    /*
    CREATE TABLE data_adm.biostream_area_regioni
    TABLESPACE pg_default
    AS
    SELECT it_regioni.id,it_regioni.geom,cod_rip,cod_reg,den_reg
    FROM it_regioni,biostream_area
    WHERE st_intersects(it_regioni.geom, biostream_area.geom)
    WITH DATA;

    ALTER TABLE data_adm.biostream_area_regioni
        ADD CONSTRAINT biostream_area_regioni_pk PRIMARY KEY (id);
    CREATE INDEX IF NOT EXISTS biostream_area_regioni_geom_idx
        ON data_adm.biostream_area_regioni USING gist
        (geom);
    */    
    """
    id = models.AutoField("ID", primary_key=True)
    geom = models.MultiPolygonField("Geometria", blank=True, null=True, srid=3035)
    cod_rip = models.IntegerField("Codice Ripartizione")
    cod_reg = models.IntegerField("Codice Regione")
    den_reg = models.TextField("Denominazione")

    class Meta:
        managed = False
        db_table = '"data_adm"."biostream_area_regioni"'
        verbose_name = 'Regioni entro il Progetto BioSTREAM'
        verbose_name_plural = 'Regioni entro il Progetto BioSTREAM'
        ordering = ['den_reg']
        app_label = 'biostream'

    def __str__(self):
        return "%s" % (self.den_reg)


class AreeSearch(models.Model):
    id = models.AutoField("ID",primary_key=True)
    area_id = models.IntegerField("Area ID",blank=True, null=True)
    nome = models.TextField("Nome",blank=True, null=True)
    tipo = models.TextField("Tipo area (legenda)",blank=True, null=True)
    tipo_auto = models.TextField("Tipo area",blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'aree_search'
        ordering = ['nome']

    def __str__(self):
        return '{nome} ({tipo})'.format(nome=self.nome,tipo=self.tipo)