    var styleFn = function (f, res) {
        var opacity = Math.min(1, f.get('features').length / max);
        return [new ol.style.Style({
            fill: new ol.style.Fill({
                color: [0, 0, 255, opacity]
            })
        })];
    };

    // Create HexBin and calculate min/max
    binSize = 200;
    var features;
    hexbin = new ol.source.HexBin({
        source: source, // source of the bin
        size: size // hexagon size (in map unit)
    });
    layer = new ol.layer.Vector({
        source: hexbin,
        opacity: 0.5,
        style: styleFn,
        renderMode: 'vector'
    });
    features = hexbin.getFeatures();
    // Calculate min/ max value
    min = Infinity;
    max = 0;
    for (var i = 0, f; f = features[i]; i++) {
        var n = f.get('features').length;
        if (n < min) min = n;
        if (n > max) max = n;
    }
    var dl = (max - min);
    maxi = max;
    min = Math.max(1, Math.round(dl / 4));
    max = Math.round(max - dl / 3);
    //$(".min").text(min);
    //$(".max").text(max);


    <
    script >
        //specie = "Tadorna tadorna";
        //specie_href = "{% url 'taxonstats' %}/" + specie;
        //var a = document.getElementById('yourlinkId');
        //a.href = specie_href;
        //a.data-target = "modal-specie-"+ replace(" ","_",tolower(specie));
        <
        /script>




    ########### SELECT INTERACTION

    var popupVector = new ol.Overlay.PopupFeature({
        popupClass: 'default',
        select: selectInteractionForGrids,
        canFix: true,
        template: {
            title: 'specie',
            attributes: {
                'obs_count': {
                    title: 'Numero osservazioni'
                },
                'res': {
                    title: 'Risoluzione griglia'
                }
            }
        }
    });
    ol.Overlay.PopupFeature.prototype.show = function (coordinate, features) {
        if (coordinate instanceof ol.Feature ||
            (coordinate instanceof Array && coordinate[0] instanceof ol.Feature)) {
            features = coordinate;
            coordinate = null;
        }
        if (!(features instanceof Array)) features = [features];
        if (features[0]) {
            var featCenter = features[0].getGeometry().getFirstCoordinate();
            if (features[0].values_.features) {
                features = features[0].values_.features;
            };
        };
        this._features = features.slice();
        if (!this._count) this._count = 1;
        // Calculate html upon feaures attributes
        this._count = 1;
        var html = this._getHtml(features[0]);
        this.hide();
        if (html) {
            if (!coordinate || features[0].getGeometry().getType() === 'Point') {
                if (features[0].values_.features) {
                    coordinate = featCenter;
                } else {
                    coordinate = features[0].getGeometry().getFirstCoordinate();
                }
            }
            ol.Overlay.Popup.prototype.show.call(this, coordinate, html);
        }
    };


    ol.Overlay.PopupFeature.prototype._getHtml = function (feature) {
        if (!feature) return '';
        var html = ol.ext.element.create('DIV', {
            className: 'ol-popupfeature'
        });
        if (this.get('canFix')) {
            ol.ext.element.create('I', {
                    className: 'ol-fix',
                    parent: html
                })
                .addEventListener('click', function () {
                    this.element.classList.toggle('ol-fixed');
                }.bind(this));
        }
        if (this._features.length > 1) {
            var template = {
                title: 'specie',
                //attributes: {
                //	'specie': {title: 'Specie'}
                //}
            }
        } else {
            var template = this._template;
        }
        // calculate template
        if (!template || !template.attributes) {
            template = template || {};
            template.attributes = {};
            for (var i in feature.getProperties())
                if (i != 'geometry') {
                    template.attributes[i] = i;
                }
        }
        // Display title
        if (template.title) {
            var title;
            if (typeof template.title === 'function') {
                title = template.title(feature);
            } else {
                title = feature.get(template.title);
            }
            ol.ext.element.create('H1', {
                html: title,
                parent: html
            });
        }
        // Display properties in a table
        if (template.attributes) {
            var tr, table = ol.ext.element.create('TABLE', {
                parent: html
            });
            var atts = template.attributes;
            for (var att in atts) {
                var a = atts[att];
                var content, val = feature.get(att);
                // Get calculated value
                if (typeof (a.format) === 'function') {
                    val = a.format(val, feature);
                }
                // Is entry visible?
                var visible = true;
                if (typeof (a.visible) === 'boolean') {
                    visible = a.visible;
                } else if (typeof (a.visible) === 'function') {
                    visible = a.visible(feature, val);
                }
                if (visible) {
                    tr = ol.ext.element.create('TR', {
                        parent: table
                    });
                    ol.ext.element.create('TD', {
                        html: a.title || att,
                        parent: tr
                    });
                    // Show image or content
                    if (this.get('showImage') && /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/.test(val)) {
                        content = ol.ext.element.create('IMG', {
                            src: val
                        });
                    } else {
                        content = (a.before || '') + val + (a.after || '');
                        var maxc = this.get('maxChar') || 200;
                        if (typeof (content) === 'string' && content.length > maxc) content = content.substr(0, maxc) + '[...]';
                    }
                    // Add value
                    ol.ext.element.create('TD', {
                        html: content,
                        parent: tr
                    });
                }
            }
        }
        // Zoom button
        ol.ext.element.create('BUTTON', {
                className: 'ol-zoombt',
                parent: html
            })
            .addEventListener('click', function () {
                if (feature.getGeometry().getType() === 'Point') {
                    this.getMap().getView().animate({
                        center: feature.getGeometry().getFirstCoordinate(),
                        zoom: Math.max(this.getMap().getView().getZoom(), 18)
                    });
                } else {
                    var ext = feature.getGeometry().getExtent();
                    this.getMap().getView().fit(ext, {
                        duration: 1000
                    });
                }
            }.bind(this));
        // Counter
        if (this._features.length > 1) {
            var div = ol.ext.element.create('DIV', {
                className: 'ol-count',
                parent: html
            });
            ol.ext.element.create('DIV', {
                    className: 'ol-prev',
                    parent: div
                })
                .addEventListener('click', function () {
                    this._count--;
                    if (this._count < 1) this._count = this._features.length;
                    html = this._getHtml(this._features[this._count - 1]);
                    ol.Overlay.Popup.prototype.show.call(this, this.getPosition(), html);
                }.bind(this));
            ol.ext.element.create('TEXT', {
                html: this._count + '/' + this._features.length,
                parent: div
            });
            ol.ext.element.create('DIV', {
                    className: 'ol-next',
                    parent: div
                })
                .addEventListener('click', function () {
                    this._count++;
                    if (this._count > this._features.length) this._count = 1;
                    html = this._getHtml(this._features[this._count - 1]);
                    ol.Overlay.Popup.prototype.show.call(this, this.getPosition(), html);
                }.bind(this));
        }
        // Use select interaction
        if (this._select) {
            this._noselect = true;
            this._select.getFeatures().clear();
            this._select.getFeatures().push(feature);
            this._noselect = false;
        }
        return html;
    };
    map.addOverlay(popupVector);



    ##############
    COTROLLI DI CREAZIONELAYER SPECIE INUTILIZZATI

    var createSpeciesVectorLayer = function (taxonName, url, col1, col2) {

        var vectorSource = loadGeoJSONVectorSource(url);

        // Cluster Source
        var clusterSource = new ol.source.Cluster({
            distance: 40,
            source: vectorSource
        });

        // Animated cluster layer
        // Style for the clusters
        var styleCache = {};
        return new ol.layer.AnimatedCluster({
            title: taxonName,
            name: taxonName,
            colors: [col1, col2],
            geotype: 'point',
            layerkind: 'species-point',
            source: clusterSource,
            style: function (feature, resolution) {
                var size = feature.get('features').length;
                var style = styleCache[size];
                if (!style) {
                    baseColor = col1;
                    color_two = col2;
                    var radius = Math.max(8, Math.min(size * 0.75, 20));
                    style = styleCache[size] = [new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: radius + 2,
                                stroke: new ol.style.Stroke({
                                    color: baseColor,
                                    width: 4
                                })
                            })
                        }),
                        new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: radius,
                                fill: new ol.style.Fill({
                                    color: color_two
                                })
                            }),
                            text: new ol.style.Text({
                                text: size.toString(),
                                fill: new ol.style.Fill({
                                    color: 'white',
                                }),
                                stroke: new ol.style.Stroke({
                                    color: color_two,
                                    width: 3
                                }),
                            })
                        })
                    ];
                }
                return style;
            }
        });
    };




    var createSpeciesSupportVectorLayer = function (taxonName, url, col1, col2) {
        var vectorSource = loadGeoJSONVectorSource(url);

        var styles = [
            /* We are using two different styles for the polygons:
             *  - The first style is for the polygons themselves.
             *  - The second style is to draw the vertices of the polygons.
             *    In a custom `geometry` function the vertices of a polygon are
             *    returned as `MultiPoint` geometry, which will be used to render
             *    the style.
             */
            new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: col1,
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: col2
                })
            }),
            new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: 'orange'
                    })
                }),
                geometry: function (feature) {
                    // return the coordinates of the first ring of the polygon
                    var coordinates = feature.getGeometry().getCoordinates()[0];
                    return new ol.geom.MultiPoint(coordinates);
                }
            })
        ];

        return new ol.layer.Vector({
            title: taxonName,
            name: taxonName,
            colors: [col1, col2],
            geotype: 'polygon',
            layerkind: 'species-support',
            source: vectorSource,
            style: styles
        });
    };