from rest_framework import routers
from django.urls import path
from .api import (
    AreeProtetteCheckListViewSet, AreeProtetteViewSet,ComuniViewSet, ObservationChecklistViewSet,
    ProvinceViewSet, ComuniViewSet, ObservationChecklistDetailedViewSet
)
from .views import AreeAutocomplete, ComuniAutocomplete

router = routers.DefaultRouter()
router.register(r'province', ProvinceViewSet)
router.register(r'comuni', ComuniViewSet)
router.register(r'areeprotette', AreeProtetteViewSet)
router.register(r'aree-protette-check', AreeProtetteCheckListViewSet,basename='aree-protette-check')
router.register(r'observations-taxon-check', ObservationChecklistViewSet,basename='observations-taxon-check')
router.register(r'observations-taxon-detailed-check', ObservationChecklistDetailedViewSet,basename='observations-taxon-detailed-check')


urlpatterns = [    
    path('aree-autocomplete',AreeAutocomplete.as_view(),name="aree-autocomplete"),
    path('comuni-autocomplete/',ComuniAutocomplete.as_view(),name='comuni-autocomplete'),

]