#!/usr/bin/env python

import requests
import zipfile
import os
import shutil

file = 'dwc.zip'
dir = 'dwc_repo'
if os.path.exists(file):
	os.remove(file)
if os.path.exists(dir):
	shutil.rmtree(dir)

url = 'https://github.com/tdwg/dwc/archive/refs/heads/master.zip'
r = requests.get(url)
open('dwc.zip','wb').write(r.content)

with zipfile.ZipFile('dwc.zip', 'r') as zip_ref:
    zip_ref.extractall('dwc_repo')
os.remove(file)

for file_name in os.listdir(os.path.join(dir,'dwc-master')):
	shutil.move(os.path.join(dir,'dwc-master',file_name),dir)
shutil.rmtree(os.path.join(dir,'dwc-master'))