from django.views.generic.base import TemplateView

from django.contrib.auth import get_user_model

from django.contrib.gis.db.models import F, Func, Value, Subquery, Union
from django.db.models.functions import Lower
from django.shortcuts import render
from biodiv.models import (
    Observation
)
from map.models import (
    AreeProtette, BioSTREAMArea
)


class TableSpeciePage(TemplateView):
    template_name = "table_specie.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['species'] = Observation.objects.all().distinct(
            'taxon').values_list('taxon', flat=True)
        return context


class TableSICPage(TemplateView):
    template_name = "table_sic.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sic_zps'] = AreeProtette.objects.filter(code__in=Subquery(
            Observation.objects.all().distinct(
                'protectedarea_code').values_list('protectedarea_code', flat=True)
        )).distinct('code', 'name').values('code', 'name').order_by('name')
        return context
