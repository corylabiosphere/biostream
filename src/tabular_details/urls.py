from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path
import profiles.urls
import accounts.urls
from . import views
from rest_framework import routers

from biodiv.api import (
    ObservationViewSet
)

#router = routers.DefaultRouter()
# router.register(r'observationsic', ObservationViewSet,
#                base_name='observationsic')

urlpatterns = [
    path('table/specie/', views.TableSpeciePage.as_view(), name="table-specie"),
    path('table/sic/', views.TableSICPage.as_view(), name="table-sic"),
]
