from django.apps import AppConfig


class TabularDetailsConfig(AppConfig):
    name = 'tabular_details'
