from biodiv.models import (
    Institution,
    DataOrigin,
    Observation, ObservationGrid
)
from rest_framework import serializers, relations
from rest_framework.serializers import ModelSerializer
from rest_framework.fields import SkipField

from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField
from rest_framework_gis.fields import GeometryField

from collections import OrderedDict
from biostream.utils import DynamicFieldsMixin
