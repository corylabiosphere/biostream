from django.core.management.base import BaseCommand, CommandError
from data_requests.models import DataRequest
from datetime import datetime, timedelta


class Command(BaseCommand):
    help = 'Setta come non valide richieste di dati più vecchie di dieci giorni'

    def handle(self, *args, **options):
        DataRequest.objects.filter(
            created__lte=datetime.now()-timedelta(days=10)).update(expired=True)
        self.stdout.write('Deallocate richieste più vecchie di dieci giorni')
