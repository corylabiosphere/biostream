from __future__ import unicode_literals
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import HttpRequest, JsonResponse
from biodiv.models import Observation
from biostream.tasks import export_data_to_excel

import json
from biostream.forms import AjaxableResponseMixin

from .forms import DataRequestForm
from .models import DataRequest

from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView


class DataRequestsListView(LoginRequiredMixin, ListView):
    """
    Richieste dati precisi effettuate dall'utente non di staff
    """
    model = DataRequest

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show_user'] = self.request.user
        return context

    def get_queryset(self):
        return DataRequest.objects.filter(created_by=self.request.user)


class DataRequestsCreate(AjaxableResponseMixin, CreateView):
    """
    Crea una nuova richiesta di dati precisi
    """
    model = DataRequest
    form_class = DataRequestForm
    #fields = ['geom', 'description', 'habitat_only']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show_user'] = self.request.user
        return context


class DataRequestsUpdate(AjaxableResponseMixin, UpdateView):
    """
    Aggiorna una richiesta di dati precisi per utente non di staff
    """
    model = DataRequest
    form_class = DataRequestForm
    #fields = ['geom', 'description', 'habitat_only']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show_user'] = self.request.user
        return context


class DataRequestsDelete(DeleteView):
    """
    Rimuove una richiesta di dati precisi
    """
    model = DataRequest
    success_url = reverse_lazy('datarequests-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show_user'] = self.request.user
        return context

def extract_and_send_db_via_email(request: HttpRequest) -> JsonResponse:
    """Handle the post requests for sending emails."""
    if request.method == 'POST':
        request_data = json.loads(request.body)
        email = request_data['userEmail']
        typ = request_data['extractionType']
        
        export_data_to_excel.delay(email,typ)
        return JsonResponse({'message': "Dati in estrazione: attendi l'arrivo della mail"}, status=200)
    return JsonResponse({'message': "Errore nell'estrazione! Contatta l'amministratore di sistema"}, status=500)

class DataRequestFull(TemplateView):
    template_name = "datarequest_full_form.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show_user'] = self.request.user
        return context    