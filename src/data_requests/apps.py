from django.apps import AppConfig


class DataRequestsConfig(AppConfig):
    name = 'data_requests'
    verbose_name = 'Gestore delle richieste dati'
