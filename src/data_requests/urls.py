from django.urls import path

from .views import (
    DataRequestsListView,
    DataRequestsCreate, DataRequestsDelete,
    DataRequestFull,extract_and_send_db_via_email
)

urlpatterns = [
     path('list/', DataRequestsListView.as_view(),
          name='datarequests-list'),
     path('list/add/', DataRequestsCreate.as_view(), name='datarequests-add'),
     # richieste non editabili una volta fatte, per ordine
     #path('list/<int:pk>/', DataRequestsUpdate.as_view(),
     #     name='datarequests-update'),
     #<div class="col-sm-10"><a href="{% url 'datarequests-update' datarequest.id%}" class="btn btn-primary">Modifica</a>

     path('list/delete/<int:pk>', DataRequestsDelete.as_view(),
          name='datarequests-delete'),
     path('full-form/', DataRequestFull.as_view(), name='extract_full_form'),                    
     path('extract-full/', extract_and_send_db_via_email, name='extract_full'),          
]
