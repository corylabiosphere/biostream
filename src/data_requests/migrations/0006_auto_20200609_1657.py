# Generated by Django 2.2 on 2020-06-09 16:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_requests', '0005_auto_20200609_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datarequestsacceptances',
            name='id',
            field=models.AutoField(db_column='data_requests_acceptances_id', primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
