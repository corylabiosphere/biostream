# Generated by Django 2.2 on 2020-06-10 05:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_requests', '0010_auto_20200609_1823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datarequestacceptance',
            name='accepted',
            field=models.BooleanField(default=False, verbose_name='Richiesta accettata?'),
        ),
        migrations.AlterField(
            model_name='datarequestacceptance',
            name='motivation',
            field=models.TextField(blank=True, null=True, verbose_name='Motivazione per richiesta rifiutata'),
        ),
    ]
