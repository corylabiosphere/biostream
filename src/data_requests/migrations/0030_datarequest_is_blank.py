# Generated by Django 2.2 on 2021-10-25 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_requests', '0029_auto_20211015_0630'),
    ]

    operations = [
        migrations.AddField(
            model_name='datarequest',
            name='is_blank',
            field=models.BooleanField(default=True, verbose_name='La richiesta contiene dati rispetto ai parametri impostati?'),
        ),
    ]
