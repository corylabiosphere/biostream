from io import StringIO
from io import BytesIO
import zipfile
import re
import logging

from adminactions.templatetags.actions import verbose_name
from django.conf import settings
from django.core.files import File
from django.utils.timezone import now
from datetime import timedelta
def expiration():
    return now() + timedelta(days=15)

from django_currentuser.middleware import get_current_user
from django.urls import reverse
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db.models import Q

from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Transform
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import Polygon, MultiPolygon

from django.core.exceptions import ValidationError

from biodiv.models import Institution, Observation
from biostream.models import (GeoModel, ShortTextField, TimeStampedModel,
                              UserReferencedModel)
from map.models import Comuni
# a) quando un utente inserisce una richiesta dati, notifica i responsabili degli enti preposti
# 1) crea automaticamente csv con tutti i dati richiesti, invisibili all'utente fino a punto 2)
# 2) quando tutte le richieste di accettazione sono validate, manda un segnale all'utente riguardo l'accettazione, magari con link ai dati
from biostream.utils import convert_to_dataframe
from profiles.models import Profile
from taxon.models import Taxonomy

from django.core.mail import EmailMessage, EmailMultiAlternatives
from biostream.utils import get_or_null,get_admin_url
class DataRequestQuerySet(models.QuerySet):
    def accepted(self):
        acceptances = DataRequestAcceptance.objects.filter(
            data_request=self).values_list('accepted', flat=True)
        if all([x == True for x in acceptances]):
            return self
        else:
            return None

#class DataRequestManager(models.Manager):
#    def get_queryset(self):
        return super().get_queryset().filter(expiration_date>now)
    #def get_queryset(self):
    #    return DataRequestQuerySet(self.model, using=self._db)
    

class DataRequest(UserReferencedModel, TimeStampedModel, GeoModel):
    id = models.AutoField(
        "ID", primary_key=True, db_column='data_requests_id')
    name = ShortTextField('Nome identificativo della richiesta',blank=True,null=True)
    geom = models.MultiPolygonField("Geometria della richiesta", srid=4326,blank=True,null=True)
    description = models.TextField("Motivo della richiesta")
    comune = models.ForeignKey(Comuni,verbose_name="Comune entro il quale si vuole effettuare la richiesta",
        blank=True,null=True,help_text="ATTENZIONE: selezionare un comune ha precedenza rispetto al disegnare / inserire manualmente una geometria!",on_delete=models.SET_NULL)
    habitat_only = models.BooleanField(
        "Richiesti solo i taxa protetti secondo la Direttiva Habitat?", blank=True, null=True)
    #expiration_date = models.DateTimeField(default=expiration,verbose_name='Scade in',blank=True,null=True)
    expired = models.BooleanField("Richiesta scaduta", default=False)
    accepted = models.BooleanField(
        "Richiesta accettata da tutti i Fornitori", default=False)
    geofile = models.FileField("File contenente la geometria",blank=True,null=True,help_text="KML, GeoJSON o GPKG")
    is_blank = models.BooleanField("La richiesta contiene dati rispetto ai parametri impostati?",default=True)
    #objects = DataRequestManager()

    class Meta:
        managed = True
        verbose_name = 'Richiesta dati'
        verbose_name_plural = 'Richieste dati'
        ordering = ['expired', 'id']
        app_label = 'data_requests'

    @property
    def is_accepted(self):
        acceptances = DataRequestAcceptance.objects.filter(
            data_request=self).values_list('accepted', flat=True)
        if all([x == True for x in acceptances]):
            return True
        else:
            return False

    def clean(self):
        super(DataRequest, self).clean()
        if self.comune is None and self.geom is None and self.geofile is None:
            raise ValidationError({
            'geom': ["Deve essere disegnata una geometria, oppure definito un comune, o effettuato l'upload di un file!"],
            'comune': ["Deve essere definito un comune, oppure disegnata una geometria, oppure effettuato l'upload di un file!"],
            'comune': ["Deve essere effettuato l'upload di un file, oppure definito un comune, oppure disegnata una geometria!"],
        })


    def __str__(self):
        return "Richiesta dati dell'utente {} del {}".format(self.modified_by, self.created)

    def get_absolute_url(self):
        return reverse('datarequests-list')

    def save(self, *args, **kwargs):
        """
        Creo automaticamente le richieste di accettazione dati 
        per ogni istituzione che ha dati entro l'area indicata ed
        estraggo i dati della richiesta impacchettandoli in uno .zip
        """

        # Se la richiesta esiste...
        if self.pk:
            # ...ne recupero i dati relativi...
            orig = DataRequest.objects.get(pk=self.pk)
            # ...e se la richiesta ha una geometria ed è accettata, mi fermo qua
            if orig.geom == self.geom:
                if orig.is_accepted:
                    self.accepted = True
                super(DataRequest, self).save(*args, **kwargs)
            # ...altrimenti, se la richiesta non ha una geometria, vuol dire che è stata appena creata...
            else:
                # ... quindi prima di tutto la salvo, eventualmente aggiungendo la geometria del comune...
                if self.comune:
                    geom = self.comune.geom
                    self.geom = self.comune.geom
                elif self.geofile:
                    super(DataRequest, self).save(*args, **kwargs)
                    geom = DataSource(self.geofile.path)
                    self.geom = geom
                else:
                    geom = self.geom
                geom = self.geom
                super(DataRequest, self).save(*args, **kwargs)
                # ...poi compilo i form di accettazione per ogni istituzione che ha dati entro la richiesta
                # ...se l'utente ha selezionato un comune, prendo quello come geometria di riferimento
                # la trasformazione in diversi SRID avviene in automatico: https://docs.djangoproject.com/en/3.2/ref/contrib/gis/tutorial/
                # estraggo i nomi delle istituzioni che hanno dati
                obss = Observation.objects.filter(geom__intersects=geom)
                institutions = Observation.objects.filter(
                    geom__intersects=geom).values_list('dataorigin__institution__id').distinct()
                # se la richiesta non contiene dati, mi fermo
                if not institutions:
                    self.is_blank = True
                    super(DataRequest, self).save(*args, **kwargs)                    
                else:
                    if self.habitat_only and not obss.filter(
                        Q(taxon__taxonprotection__habitat_ap_2=True) | Q(taxon__taxonprotection__habitat_ap_2_prior=True) | Q(taxon__taxonprotection__habitat_ap_4=True) | Q(taxon__taxonprotection__habitat_ap_5=True)
                    ):
                        self.is_blank = True
                        super(DataRequest, self).save(*args, **kwargs)                    
                    else:
                        # se c'erano già istituzioni, vuol dire che l'utente ha cambiato geometria: elimino quindi le vecchie richieste di accettazione
                        DataRequestAcceptance.objects.filter(
                            data_request=DataRequest.objects.filter(id=self.id)[0]).delete()

                        ###
                        # Per ogni istituzione creo il set di dati appartenente e lo rendo disponibile come file
                        # in modo che il validatore possa rapidamente capire che cosa viene richiesto
                        ###
                        # rimuovo i campi che non mi interessano
                        for institution in institutions:
                            obs = obss.filter(dataorigin__institution__id__in=institution).defer('geom')
                            if self.habitat_only:
                                obs = obs.filter(
                                    Q(taxon__taxonprotection__habitat_ap_2=True) | 
                                    Q(taxon__taxonprotection__habitat_ap_2_prior=True) |
                                    Q(taxon__taxonprotection__habitat_ap_4=True) |
                                    Q(taxon__taxonprotection__habitat_ap_5=True)
                                )
                            # se dopo l'eventuale filtro la richiesta ha ancora osservazioni, proseguo
                            if obs:
                                # nei queryset non posso aggiungere ai valori risultati di metodi. devo quindi
                                # appenderli manualmente, e dato che le queryset sono dizionari...
                                for ob in obs:
                                    ob.lat = ob.latitude_wgs84
                                    ob.lon = ob.longitude_wgs84
                                df = convert_to_dataframe(obs,fields=['id','uuid','dataorigin_row_id','dataorigin__origin_name','dataorigin__institution__institution_name','taxon','time','date_complete','date_year','date_day','date_month','count','count_modifier','count_max','count_min','count_method','baseoss','behaviour','death','sampling_method','lon','lat','coord_precision','sampling_precision','locality','it_municipality','it_province','recorder','details','meteo','habitat_code','protectedarea_code','bibliography','note_observation','note_event','taxon__taxonprotection__habitat_ap_2','taxon__taxonprotection__habitat_ap_2_prior','taxon__taxonprotection__habitat_ap_4','taxon__taxonprotection__habitat_ap_5','dataorigin__validated'],addcoords=True)
                                fp = StringIO()
                                fp.write(df.to_csv())

                                # compress to zipfile
                                fp2 = BytesIO()
                                richiestaname = 'richiestadati_{}_{}_{}.csv'.format(
                                    now().strftime('%Y-%m-%d'),
                                    get_current_user().name.replace(' ', '_'),
                                    Institution.objects.filter(id__in=institution).values_list('institution_name',flat=True)[0]
                                )
                                richiestaname = re.sub(r'[^\w\d-]','_',richiestaname)
                                with zipfile.ZipFile(fp2, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
                                    zf.writestr(zinfo_or_arcname=richiestaname, data=fp.getvalue())

                                # save everything
                                d = DataRequestAcceptance(
                                    data_request=self, institution=Institution.objects.filter(id__in=institution)[0])
                                d.dataset.save(richiestaname.replace('csv','zip'),File(fp2))
                                d.save()
                                """
                                Manda una mail a chi deve gestire il processo di validazione della richiesta dati
                                """
                                subject = 'BioSTREAM | Nuova richiesta dati'
                                message = """<strong>Messaggio automatico</strong><br>
<h3>Nuova richiesta dati da <strong>{}</strong> dal portale BioSTREAM</h3>
</br>
</br>
<h4>Dati di dettaglio della richiesta</h4>
</hr>
<strong>Richiedente</strong>: {} ({}), {}</br>
<strong>Motivazione</strong>:{}</br>
<strong>Richiesti solo dati in direttiva Habitat?</strong>:{}</br>
</br>
<strong>In allegato a questa mail il file che verrebbe reso disponibile al richiedente.</strong>
</br>
<strong>Revisione della richiesta</strong>: <a href="https://biostreamportal.net{}">https://biostreamportal.net{}</a> 
</br>
<a href="biostreamportal.net">Progetto BioSTREAM</a></br>
<image src="http://biostreamportal.net/media/loghi/banner_biostream_low.png" width=400></image></br>
<strong>Nota di riservatezza</strong></br>
Il presente messaggio contiene informazioni da considerarsi strettamente riservate, 
ed è destinato esclusivamente al destinatario sopra indicato, il quale è l'unico 
autorizzato ad usarlo, copiarlo e, sotto la propria responsabilità, diffonderlo.
Chiunque ricevesse questo messaggio per errore o lo leggesse senza esserne legittimato 
è avvertito che trattenerlo, copiarlo, divulgarlo, distribuirlo a persone diverse dal 
destinatario è severamente proibito, ed è pregato di rinviarlo immediatamente al 
mittente distruggendo l'originale.
                                    """.format(
                                    get_or_null(orig.created_by.name),
                                    get_or_null(orig.created_by.email),
                                    get_or_null(orig.created_by.profile.declared_institution),
                                    get_or_null(orig.description),
                                    get_or_null(orig.habitat_only),
                                    get_or_null(get_admin_url(d)),
                                    get_or_null(get_admin_url(d))
                                )
                                message_plain = """
Messaggio automatico
--------------------

Nuova richiesta dati da {} entro il portale BioSTREAM
=====================================================
Dati di dettaglio della richiesta
---------------------------------

Richiedente: {} ({}), {}
Motivazione:{}
Richiesti solo dati in direttiva Habitat?:{}

Revisione della richiesta: https://biostreamportal.net{} 
In allegato a questa mail il file che verrebbe reso disponibile al richiedente.

Progetto BioSTREAM
------------------
Nota di riservatezza
Il presente messaggio contiene informazioni da considerarsi strettamente riservate, 
ed è destinato esclusivamente al destinatario sopra indicato, il quale è l'unico 
autorizzato ad usarlo, copiarlo e, sotto la propria responsabilità, diffonderlo.
Chiunque ricevesse questo messaggio per errore o lo leggesse senza esserne legittimato 
è avvertito che trattenerlo, copiarlo, divulgarlo, distribuirlo a persone diverse dal 
destinatario è severamente proibito, ed è pregato di rinviarlo immediatamente al 
mittente distruggendo l'originale.
                                    """.format(
                                    get_or_null(orig.created_by.name),
                                    get_or_null(orig.created_by.email),
                                    get_or_null(orig.created_by.profile.declared_institution.institution_name),
                                    get_or_null(orig.description),
                                    get_or_null(orig.habitat_only),
                                    get_or_null(get_admin_url(d)),
                                )      
                                receivers = list(Institution.objects.filter(id=institution[0],profile__validation_referee=True).values_list('profile__user__email',flat=True))
                                receivers.append('biostreamportal@gmail.com')
                                msg = EmailMultiAlternatives(subject,message, bcc=receivers,reply_to=['biostreamportal@gmail.com'])
                                msg.content_subtype = 'html'
                                msg.attach_alternative(message_plain, "text/plain")
                                msg.attach_file(d.dataset.path)
                                msg.send()                    
        # ...altrimenti, se la richiesta non esiste, faccio tutto quanto sopra ma senza controllare se
        # il tutto è già stato accettato
        else:
            # if orig.is_accepted:
            #    self.accepted = True
            if self.comune:
                geom = self.comune.geom
                self.geom = self.comune.geom
            elif self.geofile:
                super(DataRequest, self).save(*args, **kwargs)
                ds = DataSource(self.geofile.path)
                self.geom = MultiPolygon(ds[0].get_geoms(geos=True)[0])
            else:
                geom = self.geom
            geom = self.geom
            super(DataRequest, self).save(*args, **kwargs)
            # ...ne recupero i dati relativi...
            orig = DataRequest.objects.get(pk=self.pk)
            obss = Observation.objects.filter(geom__intersects=geom)
            institutions = obss.values_list('dataorigin__institution__id').distinct()
            # se la richiesta non contiene dati, mi fermo
            if not institutions:
                self.is_blank = True
                super(DataRequest, self).save(*args, **kwargs)                    
            else:
                if self.habitat_only and not obss.filter(
                    Q(taxon__taxonprotection__habitat_ap_2=True) | 
                    Q(taxon__taxonprotection__habitat_ap_2_prior=True) |
                    Q(taxon__taxonprotection__habitat_ap_4=True) |
                    Q(taxon__taxonprotection__habitat_ap_5=True)
                ):
                    self.is_blank = True
                    super(DataRequest, self).save(*args, **kwargs)                    
                else:     
                    for institution in institutions:
                        obs = obss.filter(dataorigin__institution__id__in=institution).defer('geom')
                        if self.habitat_only:
                            obs = obs.filter(
                                Q(taxon__taxonprotection__habitat_ap_2=True) | 
                                Q(taxon__taxonprotection__habitat_ap_2_prior=True) |
                                Q(taxon__taxonprotection__habitat_ap_4=True) |
                                Q(taxon__taxonprotection__habitat_ap_5=True)
                            )
                        # se dopo l'eventuale filtro la richiesta ha ancora osservazioni, proseguo
                        if obs:
                            for ob in obs:
                                ob.lat = ob.latitude_wgs84
                                ob.lon = ob.longitude_wgs84                              
                            df = convert_to_dataframe(obs,fields=['id','uuid','dataorigin_row_id','dataorigin__origin_name','dataorigin__institution__institution_name','taxon','time','date_complete','date_year','date_day','date_month','count','count_modifier','count_max','count_min','count_method','baseoss','behaviour','death','sampling_method','lon','lat','coord_precision','sampling_precision','locality','it_municipality','it_province','recorder','details','meteo','habitat_code','protectedarea_code','bibliography','note_observation','note_event','taxon__taxonprotection__habitat_ap_2','taxon__taxonprotection__habitat_ap_2_prior','taxon__taxonprotection__habitat_ap_4','taxon__taxonprotection__habitat_ap_5','dataorigin__validated'],addcoords=True)
                            fp = StringIO()
                            fp.write(df.to_csv())

                            # compress to zipfile
                            fp2 = BytesIO()
                            richiestaname = 'richiestadati_{}_{}_{}.csv'.format(
                                now().strftime('%Y-%m-%d'),
                                get_current_user().name.replace(' ', '_'),
                                Institution.objects.filter(id__in=institution).values_list('institution_name',flat=True)[0]
                            )
                            with zipfile.ZipFile(fp2, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
                                zf.writestr(zinfo_or_arcname=richiestaname, data=fp.getvalue())

                            # save everything
                            d = DataRequestAcceptance(
                                data_request=self, institution=Institution.objects.filter(id__in=institution)[0])
                            d.dataset.save(richiestaname.replace('csv','zip'),File(fp2))
                            d.save()
                            """
                            Manda una mail a chi deve gestire il processo di validazione della richiesta dati
                            """
                            subject = 'BioSTREAM | Nuova richiesta dati'
                            message = """<strong>Messaggio automatico</strong><br>
<h3>Nuova richiesta dati da <strong>{}</strong> dal portale BioSTREAM</h3>
</br>
</br>
<h4>Dati di dettaglio della richiesta</h4>
</hr>
<strong>Richiedente</strong>: {} ({}), {}</br>
<strong>Motivazione</strong>:{}</br>
<strong>Richiesti solo dati in direttiva Habitat?</strong>:{}</br>
</br>
<strong>In allegato a questa mail il file che verrebbe reso disponibile al richiedente.</strong>
</br>
<strong>Revisione della richiesta</strong>: <a href="https://biostreamportal.net{}">https://biostreamportal.net{}</a> 
</br>
<a href="biostreamportal.net">Progetto BioSTREAM</a></br>
<image src="http://biostreamportal.net/media/loghi/banner_biostream_low.png" width=400></image></br>
<strong>Nota di riservatezza</strong></br>
Il presente messaggio contiene informazioni da considerarsi strettamente riservate, 
ed è destinato esclusivamente al destinatario sopra indicato, il quale è l'unico 
autorizzato ad usarlo, copiarlo e, sotto la propria responsabilità, diffonderlo.
Chiunque ricevesse questo messaggio per errore o lo leggesse senza esserne legittimato 
è avvertito che trattenerlo, copiarlo, divulgarlo, distribuirlo a persone diverse dal 
destinatario è severamente proibito, ed è pregato di rinviarlo immediatamente al 
mittente distruggendo l'originale.
                                """.format(
                                get_or_null(orig.created_by.name),
                                get_or_null(orig.created_by.name),
                                get_or_null(orig.created_by.email),
                                get_or_null(orig.created_by.profile.declared_institution),
                                get_or_null(orig.description),
                                get_or_null(orig.habitat_only),
                                get_admin_url(d),
                                get_admin_url(d),
                            )
                            message_plain = """
Messaggio automatico
--------------------

Nuova richiesta dati da {} entro il portale BioSTREAM
=====================================================
Dati di dettaglio della richiesta
---------------------------------

Richiedente: {} ({}) {}
Motivazione:{}
Richiesti solo dati in direttiva Habitat?:{}

Revisione della richiesta: https://biostreamportal.net{} 
In allegato a questa mail il file che verrebbe reso disponibile al richiedente.

Progetto BioSTREAM
------------------
Nota di riservatezza
Il presente messaggio contiene informazioni da considerarsi strettamente riservate, 
ed è destinato esclusivamente al destinatario sopra indicato, il quale è l'unico 
autorizzato ad usarlo, copiarlo e, sotto la propria responsabilità, diffonderlo.
Chiunque ricevesse questo messaggio per errore o lo leggesse senza esserne legittimato 
è avvertito che trattenerlo, copiarlo, divulgarlo, distribuirlo a persone diverse dal 
destinatario è severamente proibito, ed è pregato di rinviarlo immediatamente al 
mittente distruggendo l'originale.
                                """.format(
                                get_or_null(orig.created_by.name),
                                get_or_null(orig.created_by.name),
                                get_or_null(orig.created_by.email),
                                get_or_null(orig.created_by.profile.declared_institution),
                                get_or_null(orig.description),
                                get_or_null(orig.habitat_only),
                                get_admin_url(d),
                            )      
                            receivers = list(Institution.objects.filter(id=institution[0],profile__validation_referee=True).values_list('profile__user__email',flat=True))
                            receivers.append('biostreamportal@gmail.com')
                            msg = EmailMultiAlternatives(subject,message, bcc=receivers,reply_to=['biostreamportal@gmail.com'])
                            msg.content_subtype = 'html'
                            msg.attach_alternative(message_plain, "text/plain")
                            msg.attach_file(d.dataset.path)
                            msg.send()


class DataRequestAcceptanceManager(models.Manager):
    """
    Ritorno solo le richieste di autorizzazione dati afferenti 
    all'istituzione dell'utente di staff che ha fatto login
    """

    def get_queryset(self, request):
        if request.user.is_staff:
            return super(DataRequestAcceptanceManager, self).get_queryset().filter(
                institution=request.user.profile.declared_institution)
        elif request.user.is_admin:
            return super(DataRequestAcceptanceManager, self).get_queryset().all()


class DataRequestAcceptance(TimeStampedModel):
    id = models.AutoField(
        "ID", primary_key=True, db_column='data_requests_acceptances_id')
    data_request = models.ForeignKey(
        DataRequest, on_delete=models.CASCADE, verbose_name="Richiesta dati", blank=True, null=True)
    accepted = models.BooleanField("Richiesta accettata", default=False)
    motivation = models.TextField(
        "Motivazione per richiesta rifiutata", blank=True, null=True)
    institution = models.ForeignKey(Institution,
                                    verbose_name="Istituzione validatrice della richiesta dati", on_delete=models.CASCADE)
    dataset = models.FileField(
        "Dataset che il richiedente riceverebbe da parte dell'istituzione", upload_to="required_datasets/%Y-%m-%d/"
    )
    notified = models.BooleanField(
        "Notifica accettazione inviata", default=False)

    #objects = DataRequestAcceptanceManager()

    class Meta:
        managed = True
        verbose_name = 'Accettazione richiesta dati'
        verbose_name_plural = 'Accettazioni richieste dati'
        ordering = ['data_request', 'id']
        app_label = 'data_requests'

    def __str__(self):
        return "[Dati {}] Accettazione/rifiuto della richiesta dell'utente {} del {}".format(self.institution, self.data_request.created_by, self.created)


# Per semplicità, compilo il campo "accepted" su DataRequest per filtrare le richieste accettate:
# un reverse model manager pare non funzionare, e non si possono filtrare QuerySet per metodi del modello
@receiver(post_save, sender=DataRequestAcceptance)
def compile_accepted(sender, instance, *args, **kwargs):
    if instance.data_request.is_accepted:
        mdl = DataRequest.objects.filter(pk=instance.data_request.pk)[0]
        mdl.accepted = True
        mdl.save()
