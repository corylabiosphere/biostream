from django import forms
from .models import DataRequest
from leaflet.forms.widgets import LeafletWidget
from dal import autocomplete

class DataRequestForm(forms.ModelForm):
    """
    Form raggiungibile dal profilo dell'utente utile
    ad inviare richieste di dati precisi.

    .. image:: ../../_static/manual/biostream_25.png
        :width: 400
        :alt: Immagine del form di richiesta dati 
    """
    class Meta:
        model = DataRequest
        fields = ["name","geom","comune","description", "habitat_only"] # "geofile",
        widgets = {
            'comune': autocomplete.ModelSelect2(url='comuni-autocomplete'),
            'geom': LeafletWidget(
                attrs={'map_width': '100%', 'map_height': '300'  # ,'settings_overrides': {
                       # 'DEFAULT_CENTER': (6.0, 45.0),
                       # }
                       },
            )
        }
