from django import forms
from django.contrib.gis import admin
from django.urls import reverse
from leaflet.admin import LeafletGeoAdmin

from .models import (DataRequest, DataRequestAcceptance)

class DataRequestAcceptanceInlineAdmin(admin.StackedInline):
    """
    Accettazioni dei Fornitori per una specifica richiesta dati - inline
    """
    model = DataRequestAcceptance
    extra = 0
    readonly_fields = ['notified', 'dataset']
    fields = ['accepted', 'motivation', 'notified', 'dataset']

    def has_add_permission(self, request,obj=None):
        False

    #def has_delete_permission(self, request, obj=None):
    #    False

@admin.register(DataRequestAcceptance)
class DataRequestAcceptanceAdmin(admin.ModelAdmin):
    """
    Accettazioni dei Fornitori per una specifica richiesta dati
    """
    model = DataRequestAcceptance
    extra = 0
    list_display = ('requestor', 'created','institution', 'accepted',)
    readonly_fields = ['notified', 'dataset']
    fields = ['accepted', 'motivation', 'notified', 'dataset']

    @admin.display(description='RIchiedente', ordering='data_request__created_by')
    def requestor(self, obj):
        return obj.data_request.created_by
    


    def has_add_permission(self, request):
        False

    #def has_delete_permission(self, request, obj=None):
    #    False


@admin.register(DataRequest)
class DataRequestAdmin(LeafletGeoAdmin):
    """
    Gestore delle richieste di dati
    """
    list_display = ('id', 'created_by', 'created', 'accepted')
    readonly_fields = ('id', 'created_by',
                       'modified_by', 'created', 'modified', 'accepted', 'expired', )
    autocomplete_fields = ['comune']

    inlines = [DataRequestAcceptanceInlineAdmin, ]

    fieldsets = [
        (None, {
            'fields': [
                ('id',),
                ('created', 'created_by'),
                ('expired', 'accepted'),
            ]
        }),
        ('',{
            'fields': ['name'],
        }),
        ('Aree di richiesta',
            {'fields': [
                'geom',
                'comune',
                'geofile',
            ]}
         ),
        ('Dettagli', {
            'fields': [
                'description',
                ('habitat_only',),
            ]
        })
    ]
