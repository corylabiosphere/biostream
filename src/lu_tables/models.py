from django.contrib.gis.db import models


class LuTable(models.Model):
    code = models.CharField("Codice", max_length=1024, primary_key=True)
    description = models.CharField(
        "Descrizione", max_length=2048, blank=True, null=True)
    notes = models.TextField("Note", blank=True, null=True)

    class Meta:
        abstract = True
        managed = True
        ordering = ['code']

    def __str__(self):
        return "%s (%s)" % (self.code, self.description)

class LuTableCharOrder(models.Model):
    """
    Tabelle di lookup con chiave primaria numerica e ordinamento
    """
    code = models.CharField("Codice", max_length=8, primary_key=True)
    description = models.CharField(
        "Descrizione", max_length=1024, blank=True, null=True)
    order = models.IntegerField("Ordine", default=1,unique=True)
    notes = models.TextField("Note", blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ['order']

    def __str__(self):
        return "%s (%s)" % (self.code, self.description)

class LuBaseOss(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_baseoss'
        verbose_name = "Base dell'osservazione"
        verbose_name_plural = "Basi delle osservazioni"
        app_label = 'lu_tables'

class LuMeteo(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_meteo'
        verbose_name = "Tipologia meteorologica"
        verbose_name_plural = "Tipologie meteorologiche"
        app_label = 'lu_tables'


class LuBehaviour(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_behaviour'
        verbose_name = "Comportamento dell'esemplare"
        verbose_name_plural = "Comportamenti dell'esemplare"
        app_label = 'lu_tables'


class LuHabitat(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_habitat'
        verbose_name = "Habitat"
        verbose_name_plural = "Habitat"
        app_label = 'lu_tables'


class LuSamplingMethod(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_sampling_method'
        verbose_name = "Metodo di campionamento"
        verbose_name_plural = "Metodi di campionamento"
        app_label = 'lu_tables'


class LuCountMethod(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_count_method'
        verbose_name = "Metodo di conteggio"
        verbose_name_plural = "Metodi di conteggio"
        app_label = 'lu_tables'


class LuDeathCause(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_death_cause'
        verbose_name = "Causa di morte"
        verbose_name_plural = "Cause di morte"
        app_label = 'lu_tables'


class LuLifeStage(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_lifestage'
        verbose_name = "Stadio vitale"
        verbose_name_plural = "Stadi vitali"
        app_label = 'lu_tables'


class LuOccurrenceStatus(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_occurrence_status'
        verbose_name = "Tipo occorrenza"
        verbose_name_plural = "Tipi occorrenze"
        app_label = 'lu_tables'


class LuRank(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_rank'
        verbose_name = "TAXON - Rango"
        verbose_name_plural = "TAXON - Ranghi"
        app_label = 'lu_tables'


class LuSex(LuTable):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_sex'
        verbose_name = "Sesso"
        verbose_name_plural = "Sessi"
        app_label = 'lu_tables'


class LuIUCNCategory(LuTableCharOrder):
    class Meta(LuTable.Meta):
        db_table = 'lu_tables"."lu_status_iucn'
        verbose_name = "Stato di minaccia secondo IUCN"
        verbose_name_plural = "Stati di minaccia secondo IUCN"
        ordering = ['order']
        app_label = 'lu_tables'


class LuNameType(LuTable):
    class Meta(LuTable.Meta):
        managed = True
        db_table = "lu_nametype"
        verbose_name = "TAXON - Tipo nome"
        verbose_name_plural = "TAXON - Tipi nomi"
        app_label = 'lu_tables'

class LuTableDBCols(models.Model):
    """
    Tabelle per documentazione dei nomi delle colonne nelle varie tabelle
    """
    id = models.AutoField("ID",primary_key=True)
    colname = models.CharField("Nome colonna", max_length=1024)
    table = models.CharField("Table", max_length=1024)
    description_it = models.CharField("Descrizione colonna", max_length=1024, blank=True, null=True)
    notes = models.TextField("Note", blank=True, null=True)

    class Meta:
        managed = True
        ordering = ['table','colname']
        unique_together = ('colname','table',)
        app_label = 'lu_tables'

    def __str__(self):
        return "%s (%s)" % (self.colname, self.table)