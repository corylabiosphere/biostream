from django.apps import AppConfig

class LuTablesConfig(AppConfig):
    name = 'lu_tables'
    verbose_name = 'Tabelle di lookup'
