from django.apps import apps
from django.contrib import admin
from biostream.mixins import ListAdminWithSearch


models = apps.get_app_config('lu_tables').get_models()
for model in models:
    admin_class = type(
        'AdminClass', (ListAdminWithSearch, admin.ModelAdmin), {})
    try:
        admin.site.register(model, admin_class)
    except admin.sites.AlreadyRegistered:
        pass
