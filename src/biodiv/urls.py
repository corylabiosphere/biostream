from django.urls import path
from . import views
from rest_framework import routers
from .api import (
    SpeciesViewSet,
    SpeciesSupportViewSet,
    InstitutionViewSet,
    ObservationViewSet,
    ObservationChecklistViewSet,
    ObservationGridCountViewSet,
    DataOriginViewSet, ObservationGeoIDViewSet,
    ObservationPointSerializerViewSet, HabitatsViewSet, HabitatsChecklistViewSet,
    ObservationValidatedRequestViewSet,ObservationsGridHexViewSet#,ObservationPointSerializerSimpleViewSet
)

router = routers.DefaultRouter()
router.register(r'observation', ObservationViewSet,basename='observation')
router.register(r'observation-bbox-ids', ObservationGeoIDViewSet,basename='observationbboxids')
router.register(r'observationcheck', ObservationChecklistViewSet,basename='observationcheck')
router.register(r'observation-summary',ObservationGridCountViewSet, basename='observation-summary')
router.register(r'observationvalidatedrequest', ObservationValidatedRequestViewSet,basename='observationvalidatedrequest')
router.register(r'institutions', InstitutionViewSet,basename='institutions')
router.register(r'dataorigins', DataOriginViewSet,basename='dataorigins')
router.register(r'observation-point-taxon', ObservationPointSerializerViewSet,basename='observation-point-taxon')
#router.register(r'observation-point-taxon-simple', ObservationPointSerializerSimpleViewSet,
#                basename='observation-point-taxon-simple')
router.register(r'habitats', HabitatsViewSet,basename='habitats')
router.register(r'habitats-check', HabitatsChecklistViewSet,basename='habitats-check')
router.register(r'hexgrid-check', ObservationsGridHexViewSet,basename='hexgrid-check')