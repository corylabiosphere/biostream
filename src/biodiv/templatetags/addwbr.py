from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter()
def addwbr(value):
    every = 30
    value = list(value)
    value = [x for y in (value[i:i+every] + [' '] * (i < len(value) - 2) for
                         i in range(0, len(value), every)) for x in y]
    return "".join(value)
