from django.db.models.signals import pre_save, pre_delete, post_save, post_delete
from django.dispatch import receiver
from django.conf import settings
from biodiv.models import DataOrigin
import os.path

from biostream.settings import EMAIL_HOST_USER
from django.core.mail import send_mail

# add to settings.py if admin wants to stop insertion in model when file 'readonly' is present
# READ_ONLY_FILE = os.path.join(BASE_DIR, 'readonly')
# @receiver(pre_delete, sender=DataOrigin)
# @receiver(pre_save, sender=DataOrigin)
# def model_pre_change(sender, **kwargs):
#    if os.path.isfile(settings.READ_ONLY_FILE):
#        raise ReadOnlyException('Model in read only mode, cannot save')


@receiver(post_save, sender=DataOrigin)
class ReadOnlyException(Exception):
    pass
