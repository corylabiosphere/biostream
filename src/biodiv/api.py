import logging
from django.db.models import F, Count
from django.db.models import Count, F
from django.contrib.gis.db.models import Union
from django_filters import rest_framework as filters

from rest_framework import permissions, viewsets
from rest_framework_gis.filters import InBBoxFilter

from rest_framework.settings import api_settings
from rest_framework_csv import renderers as r

from .filters import (DynamicSearchFilter, ObservationFilter, TaxonFilter, TaxonInFilter, DataOriginCheckListFilter)

from .models import (DataOriginCheckList, Institution, Observation,
                     Observation, ObservationGridCount,
                     SpeciesSupport, Habitats, HabitatsChecklist, ObservationsGridHex)
from .serializers import (DataOriginCheckListSerializerBase,
                          DataOriginCheckListSerializerFull,
                          InstitutionSerializer,
                          ObservationGeoIDSerializer,
                          ObservationPointSerializerBase,
                          ObservationSerializerBase, ObservationSerializerFull,
                          ObservationCheckListSerializer,
                          ObservationGridCountSerializer,
                          SpeciesListSerializer, SpeciesSupportSerializer,
                          HabitatsSerializer, HabitatsChecklistSerializer, ObservationsGridHexSerializer)
from biostream.renderers import PaginatedZippedCSVRenderer,ZippedCSVRenderer
from map.api import filterbyarea

from taxon.functions import get_accepted_taxon

logger = logging.getLogger("project")


app_name = __package__

class ObservationGeoIDViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista utile a ritornare gli ID delle osservazioni rispetto ad un poligono dato in input
    """
    serializer_class = ObservationGeoIDSerializer
    filter_backends = (InBBoxFilter,)
    bbox_filter_field = 'geom'
    filter_class = TaxonInFilter
    filterset_fields = ['taxon']
    queryset = Observation.objects.filter(taxon__isnull=False)

    def get_queryset(self):
        """
        Modifica la queryset a seconda che l'utente faccia o meno parte dello staff
        del progetto: in quest'ultimo caso, ritorna tutte le osservazioni
        indipendentemente dal loro status di validazione e dalle autorizzazioni
        concesse
        """
        queryset = self.queryset
        if self.request.user.is_staff:
            return queryset.filter(taxon__isnull=False)
        return queryset.filter(taxon__isnull=False,dataorigin__unmasked=True,dataorigin__validated=True)


class ObservationChecklistViewSet(viewsets.ReadOnlyModelViewSet):
    """
    La vista ritorna una checklist delle varie specie presenti tra le osservazioni,
    comprendendo alcune iformazioni che normalmente vengono visualizzate tra i dettagli
    nei menu o nelle legende
    """
    serializer_class = ObservationCheckListSerializer
    queryset = Observation.objects.filter(taxon_rank='SPECIES',taxon__isnull=False)
    filter_class = TaxonFilter 
    filter_backends = (InBBoxFilter,)
    permission_classes = [permissions.AllowAny]


    def get_queryset(self):
        queryset = self.queryset
        queryset.values(rank=F('taxon__nameusage__family_fk__name__canonical_name'), taxonk=F('taxon__canonical_name'), 
            habitatk=F('taxon__taxonprotectionnormalized__habitat')).annotate(n=Count('taxon')).order_by('rank', 'taxonk')

        return queryset

class SpeciesSupportViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista che ritorna le informazioni di supporto relative ad una certa specie
    """
    queryset = SpeciesSupport.objects.all()
    serializer_class = SpeciesSupportSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['taxon', ]


class SpeciesViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Ritorna la lista delle specie presenti nel Portale
    """
    serializer_class = SpeciesListSerializer
    queryset = Observation.objects.filter(taxon__isnull=False).distinct('taxon__canonical_name').order_by('taxon__canonical_name')
    permission_classes = [permissions.AllowAny]


class InstitutionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Mostra i dettagli delle istituzioni
    """
    serializer_class = InstitutionSerializer
    querysCheckListet = Institution.objects.all()
    permission_classes = [permissions.AllowAny]


class DataOriginViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Mostra i dettagli dei dataset originali
    """
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.PaginatedCSVRenderer,ZippedCSVRenderer, )
    filter_class = DataOriginCheckListFilter

    def paginate_queryset(self, queryset, view=None):
        if 'no_page' in self.request.query_params:
            return None
        else:
            return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_queryset(self):
        if self.request.user.is_staff:
            return DataOriginCheckList.objects.all().annotate(institution_name=F('institution__institution_name'),institution_region=F('institution__region'),institution_province=F('institution__province'))
        return DataOriginCheckList.objects.filter(validated=True, unmasked=True).annotate(institution_name=F('institution__institution_name'),institution_region=F('institution__region'),institution_province=F('institution__province'))

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return DataOriginCheckListSerializerFull
        return DataOriginCheckListSerializerBase

    def get_renderer_context(self):
        """
        Ritorna i campi ordinati secondo l'ordine dei campi del serializzatore, oppure secondo l'ordine dei campi del modello.
        Utile per il renderer CSV, che di default ordina i campi alfabeticamente.
        Modificato leggermente per accomodare serializzatori dinamici
        """
        context = super().get_renderer_context()
        context['header'] = self.get_serializer_class().Meta.fields
        if context['header'] == '__all__':
            context['header'] = [f.name for f in self.serializer_class.Meta.model._meta.fields] 
        return context


# NEW DEF

class ObservationGridCountViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Conteggi riassuntivi delle osservazioni su griglia chilometrica, per specie ed osservazioni
    """
    queryset = ObservationGridCount.objects.all()
    serializer_class = ObservationGridCountSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['res', ]


class ObservationPointSerializerViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = ObservationPointSerializerBase
    queryset = Observation.objects.filter(taxon__isnull=False)
    permission_classes = [permissions.AllowAny]
    filter_class = ObservationFilter
    #renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.PaginatedCSVRenderer,ZippedCSVRenderer, )
    #filter_backends = (DynamicSearchFilter)
    #filterset_fields = ['id','taxon','rank']


    def get_queryset(self):
        queryset = self.queryset

        # recover accepted taxonomy
        taxon = self.request.query_params.get('taxon',None)
        if taxon:
            taxons = taxon.split(',')
            taxons = [get_accepted_taxon(tax,True)["id"] for tax in taxons] # passing characters in a function that was originally made to accept integers... //TODO 
            taxons = list(set(taxons))
            
            queryset = queryset.filter(taxon__in=taxons)

        if self.request.user.is_staff:
            if self.request.GET.get('code'):
                queryset = queryset.filter(habitat_code__isnull=False)
        else:
            queryset = queryset.filter(dataorigin__unmasked=True, dataorigin__validated=True)
            if self.request.GET.get('code'):
                queryset.filter(habitat_code__isnull=False)        

        return queryset

    #def filter_queryset(self, queryset):
        # super needs to be called to filter backends to be applied
    #    queryset = super().filter_queryset(queryset)
    #    if queryset.count() > 1000:
    #        return queryset.filter(habitatdir__isnull=False,iucn__isnull=False)
    #    return queryset

class ObservationViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista che attiva varie possibilità di filtro sulle osservazioni,
    sula restituzione dei dati in vari formati per lo scaricamento, e la paginazione
    per le tabelle di dettaglio
    """
    queryset = Observation.objects.filter(taxon__isnull=False)
    filter_backends = (
        InBBoxFilter, # /...?in_bbox=-90,29,-89,35 | min Lon, min Lat, max Lon, max Lat
        filters.DjangoFilterBackend,
        DynamicSearchFilter,
    )
    bbox_filter_field = 'geom'
    filter_class = ObservationFilter
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.PaginatedCSVRenderer,PaginatedZippedCSVRenderer, )
    #filterset_fields = ['id', 'taxon', 'code', 'observation_uuid']

    def paginate_queryset(self, queryset, view=None):
        if 'no_page' in self.request.query_params:
            return None
        else:
            return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_queryset(self):
        queryset = super(ObservationViewSet,self).get_queryset()

        taxon = self.request.query_params.get('taxon',None)
        if taxon:
            taxons = taxon.split(',')
            taxons = [get_accepted_taxon(tax,True)["id"] for tax in taxons] # passing characters in a function that was originally made to accept integers... //TODO 
            taxons = list(set(taxons))
            
            queryset = queryset.filter(taxon__in=taxons)

        # Se chiedo il codice dell'area protetta, rimuovo le osservazioni senza codice
        if self.request.GET.get('code'):
            queryset = queryset.filter(code__isnull=False)
        # Se l'utente non è di staff, rimuovo le osservazioni provenienti da dataset non validati e non mascherati
        if not self.request.user.is_staff:
            queryset = queryset.filter(
                dataorigin__unmasked=True, dataorigin__validated=True)
        return queryset

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return ObservationSerializerFull
        return ObservationSerializerBase

    def get_renderer_context(self):
        """
        Ritorna i campi ordinati secondo l'ordine dei campi del serializzatore, oppure secondo l'ordine dei campi del modello.
        Utile per il renderer CSV, che di default ordina i campi alfabeticamente.
        Modificato leggermente per accomodare serializzatori dinamici
        """
        context = super().get_renderer_context()
        context['header'] = self.get_serializer_class().Meta.fields
        if context['header'] == '__all__':
            context['header'] = [f.name for f in self.serializer_class.Meta.model._meta.fields] 
        return context


class ObservationValidatedRequestViewSet(viewsets.ReadOnlyModelViewSet):
    """
    La vista è comparabile ad `ObservationViewSet`, ma ritorna i dati precisi
    solo all'interno delle aree di richiesta dell'utente che vengono autorizzate 
    dai Fornitori di dati
    """
    serializer_class = ObservationPointSerializerBase
    filter_backends = (
        DynamicSearchFilter,
    )
    filter_class = ObservationFilter
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.PaginatedCSVRenderer, ZippedCSVRenderer,)
    filterset_fields = ['id', 'taxon', ]

    def paginate_queryset(self, queryset, view=None):
        if 'no_page' in self.request.query_params:
            return None
        else:
            return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_queryset(self):
        # Prendo tutte le osservazioni che hanno una tassonomia matchata
        queryset = Observation.objects.filter(taxon__isnull=False)
        # Se chiedo il codice dell'area protetta, rimuovo le osservazioni senza codice
        if self.request.GET.get('code'):
            queryset = queryset.filter(code__isnull=False)
        # Se l'utente non è di staff, rimuovo le osservazioni provenienti da dataset non validati e non mascherati
        if not self.request.user.is_staff:
            queryset = queryset.filter(
                dataorigin__unmasked=True, dataorigin__validated=True)
        return queryset

    def get_queryset(self):
        # Prendo tutte le osservazioni che hanno una tassonomia matchata
        request_geom = self.request.user.datarequest_added_related.filter(
            accepted=True).values('geom').aggregate(Union('geom'))['geom__union']
        if request_geom is not None:
            queryset = Observation.objects.filter(
                taxon__isnull=False, dataorigin__unmasked=True, dataorigin__validated=True, geom__within=request_geom)
            return queryset
        else:
            return Observation.objects.none()
        # Se chiedo il codice dell'area protetta, rimuovo le osservazioni senza codice


class HabitatsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Elendo degli habitat entro i confini del Progetto
    """
    queryset = Habitats.objects.all()#distinct('cod').order_by('cod')
    serializer_class = HabitatsSerializer
    permission_classes = [permissions.AllowAny]
    filter_backends = (InBBoxFilter,)
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.CSVRenderer, )

    def get_queryset(self):             
        queryset = super(HabitatsViewSet, self).get_queryset()
        queryset = filterbyarea(queryset,self)            
        return queryset.distinct('cod').order_by('cod')



class HabitatsChecklistViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Checklist proveniente da una vista materializzata che ritorna statistiche
    riassuntive relative ai vari habitat entro i confini del Progetto
    """
    queryset = HabitatsChecklist.objects.all()
    serializer_class = HabitatsChecklistSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['cod', 'maincod', ]

class ObservationsGridHexViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ObservationsGridHex.objects.all()
    serializer_class = ObservationsGridHexSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['taxon', 'res_m' ]
