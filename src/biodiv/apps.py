from django.apps import AppConfig


class BiodivConfig(AppConfig):
    name = 'biodiv'
    verbose_name = 'Dati di biodiversità'

    # def ready(self):
    #    import biodiv.signals
