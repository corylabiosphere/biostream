from django import forms
from django.contrib.gis import admin
from django.urls import reverse
from leaflet.admin import LeafletGeoAdmin, LeafletGeoAdminMixin
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedOnlyDropdownFilter, ChoiceDropdownFilter
from django.contrib.admin.filters import (
    RelatedOnlyFieldListFilter,
    SimpleListFilter
)
from django.db.models import JSONField
from prettyjson import PrettyJSONWidget

from .models import (
    DataOrigin, Institution, Observation, ObservationGrid,
    SpeciesSupport, Institution,
    SRIDModel
)

from biostream.admin import (
    ReadOnlyModelAdmin
)
from lu_tables.admin import *


@admin.register(SRIDModel)
class SRIDAdmin(LeafletGeoAdmin):
    """
    Visualizza gli identificativi dei sistemi di riferimento disponibili
    """
    search_fields = ('srid',)


@admin.register(SpeciesSupport)
class SpeciesSupportAdmin(LeafletGeoAdmin):
    """
    Informazioni di supporto per le specie, abilitato con mappa
    """
    list_display = ('__str__', 'id', 'name',)
    readonly_fields = ('id', 'created_by', 'modified_by',)

    # inlines = [SentieroImmagineInline,]

    fieldsets = [
        ('ID', {'fields': ['id', 'created_by', 'modified_by', ]}),
        ('Geometrie', {'fields': [
            'srid', 'geom',
        ]}),
        ('Informazioni di base', {'fields': [
            'name', 'description', 'institution', ]}),
        ('Dettagli', {'fields': [
            'taxon', 'support_visible_all', ]}),
        ('File associati', {'fields': [
            'origin_file', 'documentation_file'
        ]})
    ]


class RegionFilter(SimpleListFilter):
    """
    Filtro che ritorna i dataset originali 
    rispetto alla regione data in input
    """
    title = 'Regione'
    parameter_name = 'region'

    def lookups(self, request, model_admin):
        queryset = model_admin.queryset(request)
        return queryset.values_list('dataorigin__institution__pk', 'dataorigin__institution__region').order_by('dataorigin__institution__region').distinct()

    def queryset(self, request, queryset):
        # Ritorna la queryset filtrata
        if self.value():
            return queryset.filter(dataorigin__institution_region=self.value())


@admin.register(Observation)
class ObservationAdmin(LeafletGeoAdmin):
    """
    Modifica le singole osservazioni direttamente. Comprende un 
    visualizzatore per i campi extra in formato JSON
    """
    formfield_overrides = {
        JSONField: {'widget': PrettyJSONWidget}
    }
    list_display = ('id', 'get_taxon', 'dataorigin', 'date_complete')
    readonly_fields = ('id', 'count_max', 'count_min', 'count_modifier',)
    list_filter = (
        ('dataorigin', RelatedOnlyDropdownFilter),
        'dataorigin__institution',
        #        RegionFilter
    )
    search_fields = ('specie',)
    autocomplete_fields = (
        'srid',  # 'baseoss', 'death', 'sampling_method', 'meteo', 'behaviour',)
    )
    fieldsets = [
        ('ID', {'fields': ['id', 'dataorigin', ]}),
        ('Luogo', {'fields': [
            'srid', 'geom', 'locality'
        ]}),
        ('Informazioni di base', {'fields': [
            'specie', 'baseoss', 'count', 'count_method', 'count_modifier', 'count_min', 'count_max', ]}
         ),
        ('Tempo', {'fields': [
            'date_complete', 'date_day', 'date_month', 'date_year', 'time',
        ]}),
        ('Dettagli', {'fields': [
            'details',  'sampling_method', 'sampling_precision', 'behaviour', 'death', ]}),
        ('Varie', {'fields': [
            'meteo', 'note_observation', 'note_event', 'bibliography'
        ]}),
        ('Extra', {'fields': [
            'extra'
        ]})
    ]
    def get_taxon(self, obj):
        return obj.taxon.canonical_name
    get_taxon.short_description = 'Tassonomia'
    get_taxon.admin_order_field = 'taxon__canonical_name'    


@admin.register(Institution)
class InstitutionAdmin(admin.ModelAdmin):
    """
    Istituzioni che inseriscono dati nel Progetto
    """
    list_display = ('__str__', 'province', 'region',)
    readonly_fields = ('id', 'created_by', 'modified_by',)

    fieldsets = [
        ('ID', {'fields': ['id', 'created_by', 'modified_by', ]}),
        ('Informazioni di base',  {'fields': [
         'institution_name', 'province', 'region']}),
    ]


@admin.register(DataOrigin)
class DataOriginAdmin(admin.ModelAdmin):
    """
    Dataset originali, con opportuni filtri
    """
    list_display = ('id', 'origin_name','what', 'when', 'which',
                    'institution', 'unmasked', 'validated',)
    list_filter = (
        ('institution', RelatedOnlyDropdownFilter),
        ('when', DropdownFilter),
        'unmasked',
        'validated',
    )
    readonly_fields = ('id', 'modified_by', 'created_by',)
    autocomplete_fields = ('srid',)
    search_fields = ('what', 'institution__institution_name',
                     'institution__region','origin_name',)

    fieldsets = [
        ('ID', {'fields': ['id', 'created_by', 'modified_by']}),
        ('Informazioni di base',  {'fields': [
         'origin_name', 'institution']}
         ),
        ('Dettagli', {'fields': ['what', 'where', 'when', 'cartounit',
                                 'srid', 'origin_format', 'which',]}),
        ('Riferimenti', {'fields': ['reference', 'notes']}),
        ('File collegati', {'fields': [
         'origin_file', 'modified_file', 'documentation_file', 'script_file','script_file_other', ]}),
        ('Validazione e accessi', {'fields': [
            'validators', 'validated', 'accessors', 'unmasked', 'notes_import'
        ]})

    ]
