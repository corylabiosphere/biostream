from django_filters.rest_framework import CharFilter, FilterSet, NumberFilter
from rest_framework.filters import SearchFilter
from rest_framework_gis.filters import GeometryFilter
from rest_framework_gis.filterset import GeoFilterSet

from biostream.filters import CharInFilter,NumberInFilter
from biodiv.models import DataOriginCheckList,Observation
from taxon.models import Taxonomy, Name


class TaxonInFilter(FilterSet):
    taxon = CharFilter(field_name='taxon__canonical_name')

    class Meta:
        model = Observation
        fields = {'taxon': ['in', ]}


class IDInFilter(FilterSet):
    id = CharFilter(field_name='id')

    class Meta:
        model = Observation
        fields = {'id': ['in', ]}


class TaxonomyFilter(FilterSet):
    class Meta:
        model = Name
        fields = {'canonical_name': ['icontains']}

class ObservationFilter(FilterSet):
    geometry = GeometryFilter(
        field_name='geom',
        lookup_expr='intersects',
        help_text="""Filtro per le specie i cui punti di presenza intersecano la geometria data in input."""
    )
    #taxon = CharInFilter(field_name='taxon__canonical_name',lookup_expr='in')
    rank = CharFilter(field_name='taxon__rank')
    year_min = CharFilter(field_name='date_year', lookup_expr='gt')
    year_max = CharFilter(field_name='date_year', lookup_expr='lt')
    month_min = CharFilter(field_name='date_month', lookup_expr='gt')
    month_max = CharFilter(field_name='date_month', lookup_expr='lt')
    datasets = NumberInFilter(field_name='dataorigin', lookup_expr='in')

    class Meta:
        model = Observation
        fields = ['rank','year_min', 'year_max',
            'month_min', 'month_max', 'datasets', 'protectedarea_code'
        ]
        #fields = {'geometry': ['intersects'], 'year_min': ['gt'], 'year_max': ['lt'],
        #'month_min':['gt'], 'month_max':['lt'], 'datasets':['in'], 'protectedarea_code':['in']}

class ObservationBaseFilter(FilterSet):
    taxon = CharFilter('taxon__canonical_name', queryset=Taxonomy.objects.all(),lookup_expr='icontains')
    id = NumberInFilter('id')

    class Meta:
        model = Observation
        fields = {'id': ['in', ]}

class TaxonFilter(GeoFilterSet):
    geometry = GeometryFilter(
        field_name='geom',
        lookup_expr='intersects',
        help_text="""Filtro per le specie i cui punti di presenza intersecano la geometria data in input"""
    )

    class Meta:
        model = Observation
        fields = ['geometry']


# NEW
class DynamicSearchFilter(SearchFilter):
    def get_search_fields(self, view, request):
        return request.GET.getlist('search_fields', [])

class DataOriginCheckListFilter(FilterSet):
    id = NumberFilter(field_name='id')
    origin_name = CharFilter(field_name='origin_name')

    class Meta:
        model = DataOriginCheckList
        fields = ['id','origin_name']