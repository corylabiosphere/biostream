from lu_tables.models import (
    LuDeathCause
)
from taxon.models import (
    Name
)
from biostream.models import (
    GeoModel,
    TimeStampedModel,
    UserReferencedModel,
    ShortTextField
)
import re
import uuid
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import pre_save, pre_delete, post_save, post_delete

from django.urls import reverse

from django.contrib.gis.db import models
from django.db.models import JSONField

from django.core.exceptions import ValidationError
from biostream.validators import import_document_validator

from django.utils.translation import gettext_lazy as _
from django.db.models.fields import BooleanField
from django.forms import TextInput
from django_filters.utils import verbose_field_name

from localflavor.it.it_province import PROVINCE_CHOICES
from localflavor.it.it_region import REGION_CHOICES

from tinymce.models import HTMLField

from django.contrib.sites.models import Site
domain = Site.objects.get_current().domain

from django.core.mail import EmailMessage, EmailMultiAlternatives


def safestring(instring):
    """
    Pulisce una stringa da caratteri poco gradevoli per il database

    Args:
        instring (str): la stringa  che si vuole pulire

    Returns:
        la stringa pulita
    """
    return instring.strip().replace(" ", "_").replace(",", "")


class SRIDModel(models.Model):
    """
    Espone l'identificativo universale del sistema di riferimento
    rispetto alla tabella degli SRID di PostGIS
    """
    srid = models.AutoField("SRID", primary_key=True, editable=False)

    class Meta:
        managed = False
        db_table = 'public"."spatial_ref_sys'
        verbose_name = 'SRID'
        verbose_name_plural = 'SRIDs'
        ordering = ['srid']

    def __str__(self):
        return "%s" % (self.srid)


class Institution(TimeStampedModel, UserReferencedModel):
    """
    Istituzioni che partecipano al Progetto
    """
    id = models.AutoField("ID (autogenerato)",
                          primary_key=True, db_column="institution_id")
    institution_name = ShortTextField(
        "Nome dell'istituzione", blank=False, null=False)
    region = ShortTextField("Regione", choices=REGION_CHOICES, max_length=16)
    province = ShortTextField("Provincia", choices=PROVINCE_CHOICES, max_length=16)
    references = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name="Riferimenti",help_text="Selezionare tra gli utenti registrati i referenti per questa istituzione. Ognuno di loro riceverà conferma di nuovi dataset inseriti nel portale afferenti all'istituzione, più altre mail automatiche generate dal Portale")

    class Meta:
        managed = True
        db_table = 'data_biodiv"."institutions'
        verbose_name = 'Istituzione'
        verbose_name_plural = 'Istituzioni'

    def __str__(self):
        return "{} / {}".format(self.region, self.institution_name)


def get_dataorigin_file_upload_path(instance, filename):
    """
    Costruisce il percorso al file di dataset originale
    """
    return 'datasets/{0}/{1}/dataorigin_{2}/ORIGINALDATASET_{3}'.format(
        safestring(instance.institution.region), safestring(instance.institution.institution_name), safestring(instance.origin_name), filename)


def get_modified_file_upload_path(instance, filename):
    """
    Costruisce il percorso al file di dataset modificato
    e pronto per l'inserimento in BioSTREAM
    """
    return 'datasets/{0}/{1}/dataorigin_{2}/MODIFIEDDATASET_{3}'.format(
        safestring(instance.institution.region), safestring(instance.institution.institution_name), safestring(instance.origin_name), filename)


def get_dataorigin_documentation_upload_path(instance, filename):
    """
    Costruisce il percorso alla documentazione del dataset originale
    """
    return 'datasets/{0}/{1}/dataorigin_{2}/DOCUMENTATION_{3}'.format(
        safestring(instance.institution.region), safestring(instance.institution.institution_name), safestring(instance.origin_name), filename)


def get_dataorigin_script_upload_path(instance, filename):
    """
    Costruisce il percorso allo script che ha generato dal 
    dataset originale il dataset modificato pronto per l'inserimento
    in BioSTREAM
    """
    return 'datasets/{0}/{1}/dataorigin_{2}/SCRIPT_{3}'.format(
        safestring(instance.institution.region), safestring(instance.institution.institution_name), safestring(instance.origin_name), filename)


# used to map csv headers to location fields
HEADERS = {
    'shop_id': {'field':'id', 'required':True},
    'platinum_member': {'field':'platinum_member', 'required':False},
}


class DataOrigin(TimeStampedModel, UserReferencedModel):
    """
    Dataset che originano le osservazioni
    """
    id = models.AutoField("ID", primary_key=True, db_column="dataorigin_id")
    dataorigin_uuid = models.UUIDField("UUID", default=uuid.uuid4,
                            editable=False, db_column="dataorigin_uuid")
    origin_name = ShortTextField(
        "Nome del database originale", blank=False, null=False,help_text="Sarà quello visualizzato nelle tabelle dati, non indicare il nome del file ma quello del dataset senza eccessive abbreviazioni")
    institution = models.ForeignKey(
        Institution, verbose_name="Istituzione", db_column="institution_id", on_delete=models.CASCADE,
        blank=True, null=True,help_text="Ente che ha prodotto il dataset")
    where = ShortTextField(
        "Localizzazione nei server del fornitore dati", blank=True, null=True, db_column="origin_where",help_text="Percorso entro il server del fornitore dei dati in cui sono archiviati il file standardizzato e altri file ad esso associati (file originale, elaborazioni, foto...). Si consiglia di indicare un percorso stabile")
    cartounit = ShortTextField(
        "Unità di riferimento cartografico", blank=True, null=True, db_column="origin_cartounit")
    srid = models.ForeignKey('SRIDModel', models.PROTECT, verbose_name="SRID",
                             help_text="Identificativo (SRID) del sistema di riferimento", default=3035, db_column='srid_id')
    origin_format = ShortTextField(
        "Formato del dataset originale", blank=True, null=True, db_column="origin_format")
    when = models.IntegerField(
        "Anno di creazione del dataset originale", blank=True, null=True, db_column="origin_when")
    what = ShortTextField(
        "Descrizione del dataset", blank=True, null=True, db_column="origin_what",help_text="Sintesi dei dati contenuti, periodo di riferimento, aree interessate, metodi applicati, specie interessate, altri dettagli")
    which = ShortTextField(
        "Descrizione riassuntiva del contenuto del dataset", blank=True, null=True, db_column="origin_which",help_text="Aree interessate, specie interessate, altri dettagli utili")
    reference = ShortTextField(
        "Nominativo di riferimento", blank=True, null=True)
    notes = models.TextField("Note", blank=True, null=True)
    notes_import = models.TextField("Note al processo di importazione", blank=True, null=True)
    origin_file = models.FileField("File del dataset originale (ZIP nel caso di più file)",
                                   upload_to=get_dataorigin_file_upload_path, blank=True, null=True, max_length=1024)
    modified_file = models.FileField("File modificato, pronto per l'inserimento",
                                     upload_to=get_modified_file_upload_path, blank=True, null=True, max_length=1024,help_text="Caricare il file standardizzato per il WebGIS")
    documentation_file = models.FileField("Documentazione",
                                          upload_to=get_dataorigin_documentation_upload_path, blank=True, null=True, max_length=1024,help_text="Caricare eventuali articoli, relazioni o documenti relativi al dataset")
    script_file = models.FileField("Script usato per la sistemazione del dataset originale",
                                   upload_to=get_dataorigin_script_upload_path, blank=True, null=True, max_length=1024)
    script_file_other = models.FileField("Ulteriore script usato per successive sistemazioni",
                                   upload_to=get_dataorigin_script_upload_path, blank=True, null=True, max_length=1024,help_text="Caricare eventuali script utilizzati per la normalizzazione dei dati in un formato BioSTREAM - compatibile. Se più script, comprirere il tutto in un unico archivio")
    
    validators = ShortTextField("Validatori", blank=True, null=True,help_text="Persona che ha verificato che il file finale risulti conforme agli standard")
    accessors = models.ManyToManyField(
        Institution, verbose_name="Istutizioni con accesso pieno", related_name='accessors')
    validated = BooleanField(
        "Il dataset ha completato il processo di validazione?", default=False,help_text="Se il dataset non ha completato il processo di validazione non verrà accodato alla banca dati e quindi non potrà essere pubblicato. Per la standardizzazione dei file verifica il documento degli standard. Se hai bisogno di aiuto contatta il WebGIS manager")
    unmasked = models.BooleanField(
        "Dataset visibile sul WebGIS?", default=True,help_text="Il dataset può essere visibile se validato scientificamente o proveniente da enti 'certificati'. Indicare se il dataset è validato scientificamente o meno, se non lo è non potrà essere pubblicato")
    
    class Meta:
        managed = True
        db_table = 'data_biodiv"."dataorigins'
        verbose_name = 'Dataset originale'
        verbose_name_plural = 'Dataset originali'
        #permissions = (
        #    ("can_validate_dataset", "Può validare il dataset"),
        #)
    def get_uri(self):
        """
        Ritorna la URI del dataset originale
        """
        return 'https://{}/dataorigin-detail/{}'.format(domain, self.uuid)

    def __str__(self):
        return "{} ({})".format(self.origin_name, self.institution.institution_name)


@receiver(post_save, sender=DataOrigin)
def model_post_save(sender, instance, created, raw, **kwargs):
    """
    Manda una mail di conferma appena una nuovo dataset viene inserito
    nel Portale con qualche dettaglio del dataset inserito, a:
        - l'inseritrice/tore
        - i referenti per il Fornitore da cui proviene il dataset 
            (definito dall'inseritore nel campo "Institutions")
    """
    if created:
        def get_or_null(string):
            if string is None or string == '':
                string = 'Non definito'
            else:
                if type(string) == type(True):
                    if string:
                        string = 'Sì'
                    else:
                        string = 'No'
            return string

        subject = 'BioSTREAM | Nuovo inserimento dataset'
        message = """<strong>Messaggio automatico</strong><br>
<h3>Inserimento dataset <strong>{}</strong> in portale BioSTREAM completato</h3>
</br>
</br>
<h4>Dati di dettaglio del dataset</h4>
</hr>
<strong>Istituzione associata</strong>: {}</br>
<strong>Nome originale</strong>:{}</br>
<strong>SRID</strong>: {}</br>
<strong>Unità cartografica</strong>: {}</br>
<strong>Localizzazione nel server del Fornitore</strong>: {}</br>
<strong>Formato originale</strong>: {}</br>
<strong>Nominativo di riferimento</strong>: {}</br>
<strong>Anno creazione</strong>: {}</br>
<strong>Inseritore / destinatario</strong>: {} ({})</br>
<br>
<br>
I validatori del portale provvederanno ora ad effettuare i necessari controlli sintattici
sulla correttezza formale del dataset: completata la procedura, riceverete comununicazione
automatica di avvenuta validazione.            
<br>
<br>
<a href="biostreamportal.net">Progetto BioSTREAM</a></br>
<image src="http://biostreamportal.net/media/loghi/banner_biostream_low.png" width=400></image></br>
<strong>Nota di riservatezza</strong></br>
Il presente messaggio contiene informazioni da considerarsi strettamente riservate, 
ed è destinato esclusivamente al destinatario sopra indicato, il quale è l'unico 
autorizzato ad usarlo, copiarlo e, sotto la propria responsabilità, diffonderlo.
Chiunque ricevesse questo messaggio per errore o lo leggesse senza esserne legittimato 
è avvertito che trattenerlo, copiarlo, divulgarlo, distribuirlo a persone diverse dal 
destinatario è severamente proibito, ed è pregato di rinviarlo immediatamente al 
mittente distruggendo l'originale.
            """.format(
            get_or_null(instance.origin_name),
            get_or_null(instance.institution.institution_name),
            get_or_null(instance.what),
            get_or_null(instance.srid.srid),
            get_or_null(instance.cartounit),
            get_or_null(instance.where),
            get_or_null(instance.origin_format),
            get_or_null(instance.reference),
            get_or_null(instance.when),
            get_or_null(instance.created_by.name),
            get_or_null(instance.created_by.email)
        )
        message_plain = """
Messaggio automatico
--------------------


Inserimento dataset {} in portale BioSTREAM completato
======================================================

Dati di dettaglio del dataset
-----------------------------
Istituzione associata: {}
Nome originale:{}
SRID: {}
Unità cartografica: {}
Localizzazione nel server del Fornitore: {}
Formato originale: {}
Nominativo di riferimento: {}
Anno creazione: {}
Inseritore / destinatario: {} ({})


I validatori del portale provvederanno ora ad effettuare i necessari controlli sintattici
sulla correttezza formale del dataset: completata la procedura, riceverete comununicazione
automatica di avvenuta validazione.


Progetto BioSTREAM
------------------
Nota di riservatezza:
Il presente messaggio contiene informazioni da considerarsi strettamente riservate, 
ed è destinato esclusivamente al destinatario sopra indicato, il quale è l'unico 
autorizzato ad usarlo, copiarlo e, sotto la propria responsabilità, diffonderlo.
Chiunque ricevesse questo messaggio per errore o lo leggesse senza esserne legittimato 
è avvertito che trattenerlo, copiarlo, divulgarlo, distribuirlo a persone diverse dal 
destinatario è severamente proibito, ed è pregato di rinviarlo immediatamente al 
mittente distruggendo l'originale.
            """.format(
            get_or_null(instance.origin_name),
            get_or_null(instance.institution.institution_name),
            get_or_null(instance.what),
            get_or_null(instance.srid.srid),
            get_or_null(instance.cartounit),
            get_or_null(instance.where),
            get_or_null(instance.origin_format),
            get_or_null(instance.reference),
            get_or_null(instance.when),
            get_or_null(instance.created_by.name),
            get_or_null(instance.created_by.email)
        )        
        receiver = '{}'.format(instance.created_by.email)

        msg = EmailMultiAlternatives(subject,message, bcc=[receiver,'biostreamportal@gmail.com'],reply_to=['biostreamportal@gmail.com'])
        msg.content_subtype = 'html'
        msg.attach_alternative(message_plain, "text/plain")
        msg.send()
class DataOriginCheckList(TimeStampedModel, UserReferencedModel):
    """
    Una checklist con alcune statistiche descrittive dei dataset originali.
    Derivata da una vista materializzata in PostgreSQL
    """
    id = models.AutoField("ID", primary_key=True, db_column="dataorigin_id")
    dataorigin_uuid = models.UUIDField("UUID", default=uuid.uuid4,
                            editable=False, db_column="dataorigin_uuid")
    origin_name = ShortTextField(
        "Nome del database originale", blank=False, null=False)
    institution = models.ForeignKey(
        Institution, verbose_name="Istituzione", db_column="institution_id", on_delete=models.CASCADE,related_name='dataorigincheck')
    where = ShortTextField("Localizzazione nei server del fornitore dati", blank=True, null=True, db_column="origin_where")
    cartounit = ShortTextField("Unità di riferimento cartografico", blank=True, null=True, db_column="origin_cartounit")
    srid = models.ForeignKey('SRIDModel', models.PROTECT, verbose_name="SRID",help_text="Identificativo (SRID) del sistema di riferimento", default=3035, db_column='srid_id')
    origin_format = ShortTextField(
        "Formato del dataset originale", blank=True, null=True, db_column="origin_format")
    when = models.IntegerField(
        "Anno di creazione del dataset originale", blank=True, null=True, db_column="origin_when")
    what = ShortTextField(
        "Descrizione del dataset", blank=True, null=True, db_column="origin_what")
    which = ShortTextField(
        "Descrizione riassuntiva del contenuto del dataset", blank=True, null=True, db_column="origin_which")
    reference = ShortTextField("Nominativo di riferimento", blank=True, null=True)
    notes = models.TextField("Note", blank=True, null=True)
    origin_file = models.FileField("File del dataset originale (ZIP nel caso di più file)",
                                   upload_to=get_dataorigin_file_upload_path, blank=True, null=True, max_length=1024)
    modified_file = models.FileField("File modificato, pronto per l'inserimento",
                                     upload_to=get_modified_file_upload_path, blank=True, null=True, max_length=1024)
    documentation_file = models.FileField("Documentazione",
                                          upload_to=get_dataorigin_documentation_upload_path, blank=True, null=True, max_length=1024)
    validators = ShortTextField("Validatori", blank=True, null=True)
    accessors = models.ManyToManyField(
        Institution, verbose_name="Istutizioni con accesso pieno", related_name='dataoriginchecklist')
    validated = BooleanField(
        "Il dataset ha completato il processo di validazione?", blank=True, null=True)
    unmasked = models.BooleanField(
        "Dataset visibile sul WebGIS?", default=True)
    species_count = models.IntegerField(
        "Conteggio specie", blank=True, null=True)
    obs_count = models.IntegerField(
        "Conteggio osservazioni", blank=True, null=True)
    species_list = models.TextField("Lista specie", blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'data_biodiv"."dataorigin_checklist'
        verbose_name = 'Dataset originale - checklist'
        verbose_name_plural = 'Dataset originali - checklist'

    def __str__(self):
        return "{} ({})".format(self.origin_name, self.institution.institution_name)


def get_dataorigin_documentation_upload_path(instance, filename):
    return 'datasets/{0}/{1}/dataorigin_{2}/DOCUMENTATION_{3}'.format(
        safestring(instance.institution.region), safestring(instance.institution.institution_name), safestring(instance.origin_name), filename)


def get_speciesupport_file_upload_path(instance, filename):
    return 'datasets/{0}/{1}/speciesupport_{2}/ORIGINALFILE_{3}'.format(
        safestring(instance.institution.region), safestring(instance.institution.institution_name), safestring(instance.name), filename)


def get_speciesupport_documentation_upload_path(instance, filename):
    return 'datasets/{0}/{1}/speciesupport_{2}/DOCUMENTATION_{3}'.format(
        safestring(instance.institution.region), safestring(instance.institution.institution_name), safestring(instance.name), filename)


class SpeciesSupport(TimeStampedModel, GeoModel, UserReferencedModel):
    """
    Modello per dataset che si vogliono rendere disponibili per la consultazione
    specie - specifica
    """
    id = models.AutoField("ID", primary_key=True,
                          db_column="species_support_id")
    name = ShortTextField("Nome del dataset", default="Esempio")
    description = ShortTextField(
        "Descrizione", blank=True, null=True, db_column="support_description")
    institution = models.ForeignKey(
        Institution, verbose_name="Istituzione", db_column="institution_id", on_delete=models.CASCADE,
        blank=True, null=True)
    taxon = ShortTextField("Taxon", blank=True, null=True)  # TODO
    srid = models.ForeignKey(SRIDModel, models.PROTECT,
                             default=3035, db_column='srid_id')
    geom = models.GeometryField(
        "Geometria/e", blank=False, null=False, srid=3035)
    support_visible_all = models.BooleanField(
        "Visibile a qualsiasi utente del WebGIS?", default=True)
    origin_file = models.FileField("File del dataset originale (ZIP)",
                                   help_text="L'upload delle geometrie può essere effettuato e.g. tramite QGIS connettendosi al database BioSTREAM (credenziali abilitate necessarie)", upload_to=get_speciesupport_file_upload_path, blank=True, null=True)
    documentation_file = models.FileField("Documentazione",
                                          upload_to=get_speciesupport_documentation_upload_path, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'data_biodiv"."specie_support'
        verbose_name = 'Dataset di supporto per una specie'
        verbose_name_plural = 'Dataset di supporto per le specie'

    def __str__(self):
        return "{} ({})".format(self.taxon, self.institution)


###########
# VIEWS
###########


class ObservationGrid(GeoModel):
    """
    VISTA MATERIALIZZATA - mostra un summary delle osservazioni
    su griglie regolari per singole specie
    """
    id = models.IntegerField("ID", primary_key=True)
    specie = models.TextField("Specie", blank=True, null=True)
    count = models.IntegerField(
        "Conteggio", blank=True, null=True)
    res = models.IntegerField(
        "Risoluzione della cella", blank=True, null=True)
    geom = models.PolygonField("Geometria", blank=True, null=True, srid=3035)

    class Meta:
        managed = False
        db_table = 'data_biodiv"."observation_grid'
        verbose_name = "Quadrante griglia riassuntiva"
        verbose_name_plural = "Quadranti griglia riassuntiva"

    def __str__(self):
        return "{} - {}km".format(self.specie, self.res)


class ObservationGridCount(GeoModel):
    """
    VISTA MATERIALIZZATA - mostra un summary delle osservazioni
    su griglie regolari per tutte le specie
    """
    id = models.IntegerField("ID", primary_key=True)
    spec_list = models.TextField("Specie", blank=True, null=True)
    obs_count = models.IntegerField(
        "Conteggio osservazioni", blank=True, null=True)
    spec_count = models.IntegerField(
        "Conteggio specie", blank=True, null=True)
    res = models.IntegerField(
        "Risoluzione della cella", blank=True, null=True)
    geom = models.PolygonField("Geometria", blank=True, null=True, srid=3035)

    class Meta:
        managed = False
        db_table = 'data_biodiv"."observation_grid_count'
        verbose_name = "Quadrante griglia riassuntiva con conteggi a dettaglio standard"
        verbose_name_plural = "Quadranti griglia riassuntiva con conteggi a dettaglio standard"

    def __str__(self):
        return "{} - {}km".format(self.id, self.res)


class Observation(TimeStampedModel, GeoModel):
    id = models.AutoField("ID", primary_key=True, db_column="observation_id")
    uuid = models.UUIDField("UUID", default=uuid.uuid4,
                            editable=False, db_column="observation_uuid")
    protectedarea_code = ShortTextField(
        "Codice area protetta", blank=True, null=True)
    habitat_code = ShortTextField(
        "Codice habitat Natura2000", blank=True, null=True)
    it_municipality = ShortTextField("Comune", blank=True, null=True)
    it_province = ShortTextField("Provincia", blank=True, null=True)
    it_region = ShortTextField("Regione", blank=True, null=True)
    dataorigin_row_id = models.IntegerField(
        "ID riga nel dataset originale", blank=True, null=True)
    # THIS IS THE VERBATIM SPECIE
    taxon_original = ShortTextField("Tassonomia originale", blank=True, null=True)
    # THIS IS THE CLEANED SPECIE
    taxon_cleaned = ShortTextField("Tassonomia originale pulita per corrispondenza con backbone", blank=True, null=True)
    # THIS IS THE TAXONOMY SPECIE
    taxon = models.ForeignKey(
        Name, models.SET_NULL, blank=True, null=True, verbose_name="Tassonomia completa")
    taxon_rank = ShortTextField('Rango tassonomico',max_length=64,blank=True,null=True)
    baseoss = models.TextField(
        verbose_name="Base dell'osservazione", blank=True, null=True, db_column='baseoss_id')
    date_complete = models.DateField(
        "Data dell'osservazione", blank=True, null=True)
    date_day = models.IntegerField(
        "Giorno dell'osservazione", help_text="Autogenerato se è presente la data dell'osservazione", blank=True, null=True)
    date_month = models.IntegerField(
        "Mese dell'osservazione", help_text="Autogenerato se è presente la data dell'osservazione", blank=True, null=True)
    date_year = models.IntegerField(
        "Anno dell'osservazione", help_text="Autogenerato se è presente la data dell'osservazione", blank=True, null=True)
    time = models.TimeField("Ora dell'osservazione", blank=True, null=True)
    sampling_method = models.TextField(
        verbose_name="Metodo di campionamento", blank=True, null=True, db_column='sampling_method_id')
    sampling_precision = models.IntegerField(
        "Precisione spaziale del metodo di campionamento utilizzato", blank=True, null=True)
    coord_x = models.FloatField("Coordinata X", blank=True, null=True)
    coord_y = models.FloatField("Coordinata Y", blank=True, null=True)
    coord_precision = models.IntegerField(
        "Precisione nella determinazione delle coordinate", blank=True, null=True)
    behaviour = models.TextField(
        verbose_name="Comportamento", blank=True, null=True, db_column='behaviour_id')
    srid = models.ForeignKey(SRIDModel, models.PROTECT,
                             default=3035, db_column='srid_id')
    count = ShortTextField(
        "Numero individui", default='1', blank=True, null=True)
    count_modifier = ShortTextField(
        "Modificatore del numero di individui", default='x', blank=True, null=True,
        editable=False, help_text="Autogenerato")
    count_min = models.IntegerField("Conteggio minimo",
                                    blank=True, null=True, editable=False, help_text="Autogenerato")
    count_max = models.IntegerField("Conteggio massimo",
                                    blank=True, null=True, editable=False, help_text="Autogenerato")
    count_method = models.TextField(
        verbose_name="Metodo di conteggio degli esemplari", blank=True, null=True, db_column='count_method_id')
    locality = ShortTextField("Località", blank=True, null=True)
    recorder = ShortTextField("Rilevatore/i", blank=True, null=True)
    details = ShortTextField("Dettagli", blank=True, null=True)
    death = models.TextField(verbose_name="Eventuale causa di morte",
                             blank=True, null=True, db_column='death_id')
    meteo = models.TextField(verbose_name="Meteo",
                             blank=True, null=True, db_column='meteo_id')
    habitat = models.TextField(
        verbose_name="Habitat", blank=True, null=True, db_column='habitat_id')
    geom = models.PointField(
        "Geometria (coordinate)", blank=False, null=False, srid=3035)
    dataorigin = models.ForeignKey(
        DataOrigin, on_delete=models.DO_NOTHING, blank=True, null=True, verbose_name="Dataset origine", related_name='observations')
    note_observation = models.TextField(
        "Note relative all'esemplare osservato", blank=True, null=True)
    note_event = models.TextField(
        "Note relative all'evento di osservazione", blank=True, null=True)
    bibliography = models.TextField(
        "Eventuali fonti bibliografiche", blank=True, null=True)
    extra = JSONField("Colonne extra", blank=True, null=True)


    def clean(self):
        """
        Pulisce l'osservazione, tentando di renderla compatibile con
        lo Standard BioSTREAM
        """
        # Compile number of individuals modifier
        if self.count is not None:
            if not self.count.isdigit():
                if self.count.isalpha():
                    if self.count in ['--', '++', '-', '+']:
                        self.count_modifier = self.count
                        self.count = None
                    else:
                        raise ValidationError({
                            'count_modifier_nonnumeric_nonexists': ValidationError("Il modificatore della stima del numero di individui non rientra tra quelli consentiti ['++','+','-','--']")
                        })
                elif self.count.isdigit():
                    pass
                elif '-' in self.count:
                    self.count_min = int(self.count.split('-')[0])
                    self.count_min = int(self.count.split('-')[1])
                else:
                    count_modifier = ''.join(
                        i for i in self.count if not i.isdigit())
                    if count_modifier in ['>', '<', '~', 'x', 'X']:
                        self.count_modifier = count_modifier.lower()
                    else:
                        raise ValidationError({
                            'count_modifier_nonexists': ValidationError("Il modificatore del numero di individui non rientra tra quelli consentiti ['>','<','~','x','X']")
                        })
                    self.count = int(re.sub("[^\d]+", "", self.count))
        if self.date_complete is not None:
            self.date_year = self.date_complete.year
            self.date_month = self.date_complete.month

    def get_uri(self):
        """
        Ritorna la URI dell'osservazione
        """
        return 'https://{}/observation-detail/{}'.format(domain, self.uuid)

    class Meta:
        managed = False
        db_table = 'data_biodiv"."observations'
        verbose_name = 'Osservazione'
        verbose_name_plural = 'Osservazioni'


class HabitatsChecklist(models.Model):
    cod = ShortTextField("Codice", primary_key=True)
    maincod = models.IntegerField("Raggruppamento", blank=True, null=True)
    maindesc = models.TextField(
        "Descrizione raggruppamento", blank=True, null=True)
    desc = models.TextField("Descrizione codice", blank=True, null=True)
    website = ShortTextField("Sito web", blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'data_biodiv"."habitat_natura2000_it'
        verbose_name = "Habitat Natura2000"
        verbose_name_plural = "Elenco habitat Natura2000"
        ordering = ['cod']

    def __str__(self):
        return "%s - %s".format(self.cod, self.desc)


class Habitats(GeoModel):
    id = models.IntegerField("ID", primary_key=True,db_column="habitat_natura2000_id")
    cod = ShortTextField("Codice", max_length=-1, blank=True, null=True)
    desc = models.TextField("Descrizione", blank=True, null=True)
    geom = models.MultiPolygonField("Geometria", blank=True, null=True, srid=3035)

    class Meta:
        managed = False
        db_table = 'data_biodiv"."habitat_natura2000'
        verbose_name = "Habitat Natura2000"
        verbose_name_plural = "Habitat Natura2000"
        ordering = ['cod']


##########
## GRIGLIE
##########

class ObservationsGridHex(models.Model):
    id = models.BigIntegerField("ID",primary_key=True)
    taxon = models.TextField("Taxon",blank=True, null=True)
    #taxon_rank = models.TextField("Rango tassonomico",blank=True, null=True)  # This field type is a guess.
    res_m = models.IntegerField("Risoluzione griglia",blank=True,null=True)
    geom = models.GeometryField(srid=0, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'data_biodiv"."observations_grid_hex'
        verbose_name = "Griglia esagonale di osservazione"
        verbose_name_plural = "Griglie esagonali di osservazione"
        ordering = ['taxon']
