from django.contrib.gis.geos import Point
from .models import (
    SpeciesSupport,
    Institution,
    DataOrigin, DataOriginCheckList,
    Observation, ObservationGridCount,
    Habitats, HabitatsChecklist, ObservationsGridHex
)
from taxon.models import TaxonomyNormalized
from rest_framework import serializers, relations
from rest_framework.serializers import ModelSerializer
from rest_framework.fields import SkipField
from rest_pandas.serializers import PandasSerializer

from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField
from rest_framework_gis.fields import GeometryField

from collections import OrderedDict
from biostream.serializers import DynamicFieldsMixin


class NonNullModelSerializer(serializers.ModelSerializer):
    """
    Un serializzatore generico che ritorna solo i campi non nulli di un modello
    """

    def to_representation(self, instance):
        result = super(NonNullModelSerializer,
                       self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if result[key] is not None])


class InstitutionSerializer(ModelSerializer):
    """
    Serializzatore per le istituzioni
    """
    class Meta:
        model = Institution
        id_field = 'id'
        fields = ('id', 'institution_name', 'region', 'province',)


class DataOriginCheckListSerializerFull(ModelSerializer):
    """
    Serializzatore per i dataset con statistiche riassuntive,
    completo di tutte le info del modello
    """
    institution_name = serializers.CharField(source='institution.institution_name', read_only=True)
    institution_region = serializers.CharField(source='institution.region', read_only=True)
    institution_province = serializers.CharField(source='institution.province', read_only=True)

    class Meta:
        model = DataOriginCheckList
        fields = ('id', 'dataorigin_uuid','origin_name','institution_name','institution_region','institution_province','cartounit',
                  'when', 'what', 'which', 'reference', 'validated', 'unmasked', 'species_count', 'obs_count', 'species_list',)


class DataOriginCheckListSerializerBase(ModelSerializer):
    """
    Serializzatore per i dataset con statistiche riassuntive,
    con informazioni sensibili mascherate
    """
    institution_name = serializers.CharField(
        source='institution.institution_name', read_only=True)
    institution_region = serializers.CharField(
        source='institution.region', read_only=True)
    institution_province = serializers.CharField(
        source='institution.province', read_only=True)

    class Meta:
        model = DataOriginCheckList
        fields = ('id', 'dataorigin_uuid','origin_name', 'institution_name','institution_region','institution_province', 'cartounit','when','what','which', 'reference','species_count', 'obs_count', 'species_list', )


class ObservationSerializerBase(ModelSerializer):
    """
    Serializzatore per le osservazioni, con informazioni di base
    e coordinate ricampionate a minor precisione a tutela delle specie
    e dei dati originali
    """
    dataorigin_name = serializers.CharField(source='dataorigin.origin_name', read_only=True)
    dataorigin_institution_name = serializers.CharField(source='dataorigin.institution.institution_name', read_only=True)
    dataorigin_what = serializers.CharField(source='dataorigin.what', read_only=True)
    rank = serializers.CharField(source='taxon.rank', read_only=True)
    taxon = serializers.CharField(source='taxon.canonical_name', read_only=True)
    habitatprotection = serializers.SerializerMethodField()
    uri = serializers.SerializerMethodField()

    def get_habitatprotection(self, obj):
        query = obj.taxon.taxonprotectionnormalized_set
        if query.exists:
            if query.first() is not None:
                return query.first().habitat
            else:
                return None
        else:
            return None

    def get_uri(self, obj):
        # Ritorna la URI dell'osservazione
        return obj.get_uri()

    class Meta:
        model = Observation
        fields = ('id', 'protectedarea_code', 'taxon', 'rank', 'habitatprotection', 'date_complete', 'date_month', 'date_year', 'baseoss','count', 'recorder', 'details', 'dataorigin_name', 'dataorigin_what', 'dataorigin_institution_name', 'latitude_wgs84_imp', 'longitude_wgs84_imp', 'coord_resampled_precis_m', 'uri')


class ObservationSerializerFull(ModelSerializer):
    """
    Serializzatore per le osservazioni, con coordinate precise
    """
    dataorigin_name = serializers.CharField(source='dataorigin.origin_name', read_only=True)
    dataorigin_institution_name = serializers.CharField(source='dataorigin.institution.institution_name', read_only=True)
    dataorigin_what = serializers.CharField(source='dataorigin.what', read_only=True)
    rank = serializers.CharField(source='taxon.rank', read_only=True)
    taxon = serializers.CharField(source='taxon.canonical_name', read_only=True)
    habitatprotection = serializers.SerializerMethodField()
    uri = serializers.SerializerMethodField()

    def get_habitatprotection(self, obj):
        query = obj.taxon.taxonprotectionnormalized_set
        if query.exists:
            if query.first() is not None:
                return query.first().habitat
            else:
                return None
        else:
            return None

    def get_uri(self, obj):
        # Ritorna la URI dell'osservazione
        return obj.get_uri()

    class Meta:
        model = Observation
        fields = ('id', 'protectedarea_code', 'habitat_code', 'taxon', 'rank', 'habitatprotection', 'date_day', 'date_month', 'date_year', 'baseoss', 'time', 'sampling_method', 'sampling_precision', 'coord_precision', 'behaviour', 'count','count_modifier', 'count_method', 'locality', 'recorder', 'details', 'death', 'meteo', 'habitat', 'dataorigin_name', 'dataorigin_institution_name', 'dataorigin_what', 'latitude_wgs84', 'longitude_wgs84', 'uri')


class ObservationGeoIDSerializer(GeoFeatureModelSerializer):
    """
    Serializza solo alcune informazioni fondamentali delle osservazioni
    """
    geom = GeometryField(precision=0)
    #taxon = serializers.SerializerMethodField()

    #def get_taxon(self,obj):
    #    return obj.taxon.canonical_name if obj.taxon else None

    class Meta:
        geo_field = 'geom'
        id_field = 'id'
        model = Observation
        fields = ('id') #'taxon', 'geom',)


class SpeciesSupportSerializer(DynamicFieldsMixin, GeoFeatureModelSerializer):
    """
    Serializzatore per le geometrie di supporto delle specie
    """
    geom = GeometryField(precision=0)

    class Meta:
        model = SpeciesSupport
        geo_field = 'geom'
        id_field = 'id'
        fields = ('id', 'taxon', 'documentation_file')


class SpeciesListSerializer(ModelSerializer):
    """
    Serializza la lista delle specie rispetto alle osservazioni presenti 
    """
    class Meta:
        model = Observation
        id_field = 'id'
        fields = ('id', 'specie',)


class ObservationPointSerializerBase(GeoFeatureModelSerializer):
    """
    Serializzatore per le osservazioni che mostra solo
    alcune informazioni essenziali relative ad ID, tassonomia, geometria e appartenenza o meno alla direttiva habitat
    """
    geom = GeometryField(precision=0)
    ### CANNOT include them like that, as some objects has no protection! Have to edit the query
    #habitatdir = serializers.CharField(source='taxon.taxonprotectionnormalized.habitat',read_only=True)
    #taxmain = serializers.CharField(source='taxon.canonical_name',read_only=True) # taxon from main taxonomy
    #specie_iucn = serializers.CharField(read_only=True)
    #habitatdir = serializers.SerializerMethodField()

    def get_habitatdir(self, obj):
        query = obj.taxon.taxonprotectionnormalized_set
        if query.exists:
            if query.first() is not None:
                return query.first().habitat
            else:
                return None
        else:
            return None

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['properties'] = None

        return representation


    class Meta:
        model = Observation
        #id_field = 'id'
        geo_field = 'geom'
        fields = ('geom', )#'habitatdir','taxmain',)


class HabitatsSerializer(ModelSerializer):
    """
    Elenco degli habitat
    """
    class Meta:
        model = Habitats
        fields = ('cod',)


class HabitatsChecklistSerializer(ModelSerializer):
    """
    Elenco degli habitat con statistiche riassuntive
    """
    class Meta:
        model = HabitatsChecklist
        fields = ('cod', 'maincod', 'maindesc', 'desc', 'website',)


class ObservationCheckListSerializer(ModelSerializer):
    taxonk = serializers.CharField(read_only=True)
    rank = serializers.CharField(read_only=True)
    habitatk = serializers.CharField(read_only=True)
    habitatlegendk = serializers.CharField(read_only=True)
    n = serializers.IntegerField(read_only=True)

    class Meta:
        model = Observation
        fields = ('taxonk', 'rank', 'habitatk','habitatlegendk','n',)


class ObservationGridCountSerializer(DynamicFieldsMixin, GeoFeatureModelSerializer):
    """
    Serializzatore per la griglia riassuntiva delle osservazioni
    """
    geom = GeometryField(precision=0)

    class Meta:
        model = ObservationGridCount
        geo_field = 'geom'
        id_field = 'id'
        fields = ('id', 'spec_count', 'obs_count', 'res',) # spec_list

class ObservationsGridHexSerializer(DynamicFieldsMixin, GeoFeatureModelSerializer):
    """
    Serializzatore per la griglia riassuntiva esagonale delle osservazioni
    """
    geom = GeometryField(precision=0)

    class Meta:
        model = ObservationsGridHex
        geo_field = 'geom'
        id_field = 'id'
        fields = ('id', )