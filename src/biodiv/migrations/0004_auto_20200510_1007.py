# Generated by Django 2.0 on 2020-05-10 10:07

import biodiv.models
import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('biodiv', '0003_auto_20200510_0958'),
    ]

    operations = [
        migrations.CreateModel(
            name='ObservationGridCountFine',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False, verbose_name='ID')),
                ('spec_list', models.TextField(blank=True, null=True, verbose_name='Specie')),
                ('obs_count', models.IntegerField(blank=True, null=True, verbose_name='Conteggio osservazioni')),
                ('spec_count', models.IntegerField(blank=True, null=True, verbose_name='Conteggio specie')),
                ('res', models.IntegerField(blank=True, null=True, verbose_name='Risoluzione della cella')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=3035, verbose_name='Geometria')),
            ],
            options={
                'verbose_name': 'Quadrante griglia riassuntiva con conteggi a dettaglio molto fine',
                'verbose_name_plural': 'Quadranti griglia riassuntiva con conteggi a dettaglio molto fine',
                'db_table': 'biodiv"."observation_grid_count_fine',
                'managed': False,
            },
        ),
        migrations.DeleteModel(
            name='InatGrid',
        ),
        migrations.AlterModelOptions(
            name='observationgrid',
            options={'managed': False, 'verbose_name': 'Quadrante griglia riassuntiva', 'verbose_name_plural': 'Quadranti griglia riassuntiva'},
        ),
        migrations.AlterModelOptions(
            name='observationgridcount',
            options={'managed': False, 'verbose_name': 'Quadrante griglia riassuntiva con conteggi a dettaglio standard', 'verbose_name_plural': 'Quadranti griglia riassuntiva con conteggi a dettaglio standard'},
        ),
        migrations.AlterField(
            model_name='dataorigin',
            name='documentation_file',
            field=models.FileField(blank=True, max_length=1024, null=True, upload_to=biodiv.models.get_dataorigin_documentation_upload_path, verbose_name='Documentazione'),
        ),
        migrations.AlterModelTable(
            name='dataorigin',
            table='biodiv"."dataorigins',
        ),
        migrations.AlterModelTable(
            name='dataoriginchecklist',
            table='biodiv"."dataorigin_checklist',
        ),
        migrations.AlterModelTable(
            name='inat',
            table='biodiv"."inat',
        ),
        migrations.AlterModelTable(
            name='institution',
            table='biodiv"."institutions',
        ),
        migrations.AlterModelTable(
            name='observation',
            table='biodiv"."observations',
        ),
        migrations.AlterModelTable(
            name='observationfull',
            table='biodiv"."observations',
        ),
        migrations.AlterModelTable(
            name='observationgrid',
            table='biodiv"."observation_grid',
        ),
        migrations.AlterModelTable(
            name='observationgridcount',
            table='biodiv"."observation_grid_count',
        ),
        migrations.AlterModelTable(
            name='speciessupport',
            table='biodiv"."specie_support',
        ),
        migrations.AlterModelTable(
            name='sridmodel',
            table='public"."spatial_ref_sys',
        ),
    ]
