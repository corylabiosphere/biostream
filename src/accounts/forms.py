from __future__ import unicode_literals
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, HTML, Field
from authtools import forms as authtoolsforms
from django.contrib.auth import forms as authforms
from django.urls import reverse
from profiles.models import Profile

from django.forms.models import model_to_dict,fields_for_model

# Captcha
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox

from django.contrib.auth import get_user_model
User = get_user_model()


class LoginForm(AuthenticationForm):
    #remember_me = forms.BooleanField(
    #    label="Ricordami", required=False, initial=False)
    captcha = ReCaptchaField(
        widget=ReCaptchaV2Checkbox(
            api_params={'hl': 'it'}
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields["username"].widget.input_type = "email"  # ugly hack

        self.helper.layout = Layout(
            Field("username", placeholder="Inserisci email", autofocus=""),
            Field("password", placeholder="Inserisci Password"),
            Field("captcha"),
            HTML(
                '<a href="{}">Password dimenticata?</a>'.format(
                    reverse("accounts:password-reset")
                )
            ),
            #Field("remember_me"),
            Submit("sign_in", "Log in",
                   css_class="btn btn-lg btn-primary btn-block"),
        )


class SignupForm(authtoolsforms.UserCreationForm):
    captcha = ReCaptchaField(
        widget=ReCaptchaV2Checkbox(
            api_params={'hl': 'it'}
        )
    )
    motivation = forms.CharField(max_length=1000, help_text='Motivazione della tua iscrizione',required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for fieldname in ['password1', 'password2']:
            self.fields[fieldname].help_text = None

        self.helper = FormHelper()
        self.fields["email"].widget.input_type = "email"

        self.helper.layout = Layout(
            Field("email", placeholder="Inserisci email", autofocus=""),
            Field("name", placeholder="Inserisci nome completo"),
            Field("motivation",title="Motivazione iscrizione"),
            Field("password1", placeholder="Inserisci password"),
            Field("password2", placeholder="Reinserisci password"),
            Field("captcha"),
            Submit("sign_up", "Registrati", css_class="btn-warning"),
        )

    def save(self, commit=True):
        user = super(SignupForm, self).save(commit=True)
        user_profile = Profile(user=user,motivation=self.cleaned_data['motivation'])
        user_profile.save()
        return user

class PasswordChangeForm(authforms.PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.layout = Layout(
            Field("old_password",
                  placeholder="Inserisci vecchia password", autofocus=""),
            Field("new_password1", placeholder="Inserisci nuova password"),
            Field("new_password2", placeholder="Inserisci nuova password (di nuovo)"),
            Submit("pass_change", "Cambia password", css_class="btn-warning"),
        )


class PasswordResetForm(authtoolsforms.FriendlyPasswordResetForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.layout = Layout(
            Field("email", placeholder="Inserisci email", autofocus=""),
            Submit("pass_reset", "Resetta password", css_class="btn-warning"),
        )


class SetPasswordForm(authforms.SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.layout = Layout(
            Field("new_password1",
                  placeholder="Inserisci nuova password", autofocus=""),
            Field("new_password2", placeholder="Inserisci nuova password (di nuovo)"),
            Submit("pass_change", "Cambia password", css_class="btn-warning"),
        )
