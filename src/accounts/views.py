from __future__ import unicode_literals

from biostream.utils import send_email
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth import get_user_model
from django.contrib import auth
from django.contrib import messages
from braces import views as bracesviews
from django.conf import settings
from . import forms

from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordResetView,PasswordResetDoneView

User = get_user_model()


class LoginView(bracesviews.AnonymousRequiredMixin, LoginView):
    template_name = "accounts/login.html"
    form_class = forms.LoginForm

    def form_valid(self, form):
        redirect = super().form_valid(form)
        remember_me = form.cleaned_data.get("remember_me")
        if remember_me is True:
            ONE_MONTH = 30 * 24 * 60 * 60
            expiry = getattr(settings, "KEEP_LOGGED_DURATION", ONE_MONTH)
            self.request.session.set_expiry(expiry)
        return redirect


class LogoutView(LogoutView):
    url = reverse_lazy("home")


class SignUpView(
    bracesviews.AnonymousRequiredMixin,
    bracesviews.FormValidMessageMixin,
    generic.CreateView,
):
    form_class = forms.SignupForm
    model = User
    template_name = "accounts/signup.html"
    success_url = reverse_lazy("home")
    form_valid_message = "Iscrizione effettuata!"

    def form_valid(self, form):
        r = super().form_valid(form)
        username = form.cleaned_data["email"]
        password = form.cleaned_data["password1"]
        send_email('admin@biostreamportal.net',
                   username,
                   "Iscrizione al portale BioSTREAM completata!",
                   "Hai completato l'iscrizione al portale BioSTREAM, con nome utente uguale al destinatario di questa mail e password " + password +
                   ". Effettua il login su biostreamportal.net/login, ed accedi all'area di amministrazione alla URL biostreamportal.net/admin.",
                   "Hai completato l'iscrizione al portale BioSTREAM, con nome utente uguale al destinatario di questa mail e password <strong>" + password +
                   "</strong>. Effettua il login su <a href='http://biostreamportal.net/login'>biostreamportal.net/login</a>, ed accedi all'area di amministrazione alla URL <a href='http://biostreamportal.net/admin'>biostreamportal.net/admin</a>."
                   )
        user = auth.authenticate(email=username, password=password)
        auth.login(self.request, user)
        return r


class PasswordChangeView(PasswordChangeView):
    form_class = forms.PasswordChangeForm
    template_name = "accounts/password-change.html"
    success_url = reverse_lazy("accounts:logout")

    def form_valid(self, form):
        form.save()
        messages.success(
            self.request,
            "Password cambiata."
            "Si prega di rieffettuare il login.",
        )

        return super().form_valid(form)


class PasswordResetView(PasswordResetView):
    form_class = forms.PasswordResetForm
    template_name = "accounts/password-reset.html"
    success_url = reverse_lazy("accounts:password-reset-done")
    subject_template_name = "accounts/emails/password-reset-subject.txt"
    email_template_name = "accounts/emails/password-reset-email.html"


class PasswordResetDoneView(PasswordResetDoneView):
    template_name = "accounts/password-reset-done.html"


class PasswordResetConfirmView(PasswordResetDoneView):
    template_name = "accounts/password-reset-confirm.html"
    form_class = forms.SetPasswordForm
