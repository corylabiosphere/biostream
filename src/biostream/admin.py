from django.contrib.admin import ModelAdmin
class ReadOnlyModelAdmin(ModelAdmin):
    """
    Modello che rende un modello di amministrazione di sola lettura
    """

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False