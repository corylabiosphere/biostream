from io import BytesIO, StringIO
from celery import shared_task
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils import timezone
import zipfile
import csv

from biodiv.models import Observation
from map.models import Province, Regioni

# from biostream.utils import queryset_to_workbook
@shared_task
def export_data_to_excel(user_email: str, typ: str) -> None:
    """Send extracted model data and save in excel and send to email."""
    #excelfile = BytesIO()
    #workbook = Workbook()
    
    qs = Observation.objects.all().order_by('taxon')
    typ_geom = typ.split('-')[0]
    geom = None
    if typ_geom=='region':
        if typ=="region-veneto":
            geom = Regioni.objects.filter(den_reg='Veneto').values_list('geom',flat=True)[0]
        elif typ=="region-fvg":
            geom = Regioni.objects.filter(den_reg='Friuli Venezia Giulia').values_list('geom',flat=True)[0]
        elif typ=="region-taa":
            geom = Regioni.objects.filter(den_reg='Trentino-Alto Adige').values_list('geom',flat=True)[0]
    elif typ_geom=='province':
            typ_attr = typ.split('-')[1]
            geom = Province.objects.filter(prov_name=typ_attr.capitalize()).values_list('geom')[0]

    if geom:
        qs = qs.filter(geom__intersects=geom)

    
    fields = ('uuid', 'protectedarea_code', 'habitat_code', 'taxon__scientific_name', 'taxon_rank', 'date_day', 'date_month', 'date_year', 'baseoss', 'time', 'sampling_method', 'sampling_precision', 'coord_precision', 'behaviour', 'count','count_modifier', 'count_method', 'locality', 'recorder', 'details', 'death', 'meteo', 'habitat', 'dataorigin__origin_name', 'dataorigin__institution__institution_name', 'dataorigin__what', 'coord_x', 'coord_y','geom',)
    # workbook = queryset_to_workbook(qs, fields)

    qs = qs.values(*fields)
    for elem in qs:
        elem["uri"] = 'https://biostreamportal.net/observation-detail/' + str(elem["uuid"])
        elem["coord_y"] = elem["geom"].y
        elem["coord_x"] = elem["geom"].x
        del elem["geom"]

    fs = list(fields)
    fs.remove("geom")
    fields = tuple(fs)
    #data = [list(row.values()) for row in qs]

    #columns = ['UUID','Codice area protetta','Codice habitat','Taxon','Rango tassonomico','Protezione taxon (dir. Habitat)','Giorno','Mese','Anno','Base osservazione','Ora','Metodo di campionamento','Precisione campionamento','Precisione coordinate','Comportamento','Conteggio','Modificatore conteggio','Metodo conteggio','Località','Osservatore','Dettagli','Causa mprte','Meteo','Habitat, descrizione','Dataset originale, nome','Dataset originale, istituzione','Dataset originale, descrizione','Latitudine','Longitudine','SRID','URI']

    #data.insert(0,list(columns))
    #workbook.new_sheet('Ultimi dati BioSTREAM',data=data)
    # workbook.save(excelfile)
    now = timezone.now()

    message = EmailMessage(
        'Export BioSTREAM del {}'.format(now.date().isoformat()),
        '''
        Dump completo portale BioSTREAM generato il {}
        =============================================================

        Tipo dump: {}
        Numero record: {}

        Progetto BioSTREAM
        ------------------
        Nota di riservatezza
        Il presente messaggio contiene informazioni da considerarsi strettamente riservate, 
        ed è destinato esclusivamente al destinatario sopra indicato, il quale è l'unico 
        autorizzato ad usarlo, copiarlo e, sotto la propria responsabilità, diffonderlo.
        Chiunque ricevesse questo messaggio per errore o lo leggesse senza esserne legittimato 
        è avvertito che trattenerlo, copiarlo, divulgarlo, distribuirlo a persone diverse dal 
        destinatario è severamente proibito, ed è pregato di rinviarlo immediatamente al 
        mittente distruggendo l'originale.        
        '''.format(now.isoformat(),typ,qs.count()),
        settings.DEFAULT_FROM_EMAIL,
        [user_email],
    )

    f2 = BytesIO()
    with zipfile.ZipFile(f2, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
        #zf.writestr(zinfo_or_arcname="%s-biostream-ultimi-dati.xlsx" % (typ), data=excelfile.getvalue())
        #with open("%s-biostream-ultimi-dati.csv" % (typ), 'wb', ) as csvfile:
        #    write_csv(qs, csvfile)
        qs_data = StringIO()
        qs_writer = csv.writer(qs_data)
        qs_writer.writerow(fields)
        for obs in qs:
            qs_writer.writerow([obs[field] for field in fields])

        zf.writestr(zinfo_or_arcname="%s-biostream-ultimi-dati.csv" % (typ), data=qs_data.getvalue())

    message.attach("%s-biostream-ultimi-dati.zip" % (typ), f2.getvalue(), 'application/zip')
    message.send()
