class DBRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'auth':
            return 'default'
        elif model._meta.app_label == 'authtools':
            return 'default'
        elif model._meta.app_label == 'biodiv':
            return 'biodiv'
        elif model._meta.app_label == 'support':
            return 'support'
        elif model._meta.app_label == 'lu_tables':
            return 'lu_tables'
        elif model._meta.app_label == 'taxon':
            return 'taxon'
        else:
            return 'default'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'auth':
            return 'default'
        elif model._meta.app_label == 'authtools':
            return 'default'
        elif model._meta.app_label == 'biodiv':
            return 'biodiv'
        elif model._meta.app_label == 'support':
            return 'support'
        elif model._meta.app_label == 'lu_tables':
            return 'lu_tables'
        elif model._meta.app_label == 'taxon':
            return 'taxon'
        else:
            return 'default'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Relations between objects are allowed
        """
        db_list = ('default', 'biodiv', 'support', 'lu_tables', 'taxon')
        if obj1._state.db in db_list and obj2._state.db in db_list:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'auth':
            return db == 'default'
        if app_label == 'authtools':
            return db == 'default'
        if app_label == 'biodiv':
            return db == 'biodiv'
        elif app_label == 'support':
            return db == 'support'
        elif app_label == 'lu_tables':
            return db == 'lu_tables'
        elif app_label == 'taxon':
            return db == 'taxon'
        else:
            return db == 'default'
        return None
