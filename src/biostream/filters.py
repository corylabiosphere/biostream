from django_filters.rest_framework import BaseInFilter, CharFilter, FilterSet, NumberFilter
class CharInFilter(BaseInFilter, CharFilter):
    pass

class NumberInFilter(BaseInFilter, NumberFilter):
    pass