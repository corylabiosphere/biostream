from django.views import generic
from django.shortcuts import render
from biodiv.models import (
    Observation, DataOrigin, Institution
)
from map.models import Comuni
from django.views.generic.detail import DetailView
from dal import autocomplete

class HomePage(generic.TemplateView):
    """
    VIsta per la Home, con alcune info di contesto utili per i contatori
    """
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['n_obs'] = Observation.objects.all().count()
        context['n_dat'] = DataOrigin.objects.all().count()
        context['n_species'] = Observation.objects.distinct('taxon').count()
        context['n_part'] = Institution.objects.distinct().count()

        return context


class ObservationDetailPage(DetailView):
    """
    Dettaglio dell'osservazione raggiunto tramite URI della stessa
    """
    template_name = "observation_detail.html"
    context_object_name = 'observation'
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'
    model = Observation


class DataOriginDetailPage(DetailView):
    """
    Dettaglio del dataset originale raggiunto tramite la sua URI
    """
    template_name = "dataorigin_detail.html"
    context_object_name = 'dataorigin'
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'
    model = DataOrigin


class AboutPage(generic.TemplateView):
    template_name = "about.html"


class IntroductionPage(generic.TemplateView):
    template_name = "introduction.html"


class DocumentsPage(generic.TemplateView):
    template_name = "documents.html"


class CatalogPage(generic.TemplateView):
    template_name = "catalog.html"


class SpecieCatalogPage(generic.TemplateView):
    template_name = "specie_catalog.html"


class OrganizationPage(generic.TemplateView):
    template_name = "organization.html"


class StatusPage(generic.TemplateView):
    template_name = "status.html"

class TaxonNonMatchPage(generic.TemplateView):
    template_name = "taxon_nonmatch.html"


class DocumentationPage(generic.TemplateView):
    template_name = "documentation/documentation_base.html"


class DocumentationCatalogPage(generic.TemplateView):
    template_name = "documentation/documentation_catalog.html"


class DocumentationChecklistPage(generic.TemplateView):
    template_name = "documentation/documentation_checklist.html"


class DocumentationDataRequestPage(generic.TemplateView):
    template_name = "documentation/documentation_datarequest.html"


class DocumentationGisPage(generic.TemplateView):
    template_name = "documentation/documentation_gis.html"


class DocumentationProgrammersPage(generic.TemplateView):
    template_name = "documentation/documentation_programmers.html"

class DocumentationUserManagerPage(generic.TemplateView):
    template_name = "documentation/documentation_usermanager.html"


####
# ERRORS VIEWS
####

def custom_bad_request_view(request, exception=None):
    return render(request, "errors/400.html", {})

def custom_page_not_found_view(request, exception):
    return render(request, "errors/404.html", {})

def custom_error_view(request, exception=None):
    return render(request, "errors/500.html", {})

def custom_permission_denied_view(request, exception=None):
    return render(request, "errors/403.html", {})
