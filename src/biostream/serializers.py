
class DynamicFieldsMixin(object):
    """
    Un mixin per i serializer che permette ai parametri di accettare un
    campo "fields" addizionale che semplicemente controlla quali campi 
    devono essere restituiti.
    """

    def __init__(self, *args, **kwargs):
        super(DynamicFieldsMixin, self).__init__(*args, **kwargs)
        if not self.context:
            return
        fields = self.context['request'].query_params.get('fields', None)
        if fields:
            fields = fields.split(',')
            allowed = set(fields)
            existing = set(self.fields.keys())
            if "geom" in existing:
                allowed.update(["geom", "id"])
            for field_name in existing - allowed:
                self.fields.pop(field_name)
