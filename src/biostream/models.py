from django.contrib.gis.db import models
from django.conf import settings
from django.contrib.gis.gdal import CoordTransform, SpatialReference
from django.contrib.gis.geos.point import Point
from crum import get_current_user
from django.forms import TextInput

from django.utils import timezone

import math


class TimeStampedModel(models.Model):
    """
    Classe astratta che rende disponibili per i modelli che la importano
    i campi `created' e `modified'
    """
    created = models.DateTimeField(
        "Creato il", blank=True, null=True, editable=False)
    modified = models.DateTimeField(
        "Modificato il", blank=True, null=True, editable=False)

    def save(self, *args, **kwargs):
        """ Quando l'elemento viene salvato, aggiorna il timestamp """
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(TimeStampedModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class UserReferencedModel(models.Model):
    """
    Una classe di modelli astratta che permette al modello
    di registrare automaticamente utente inseritore e modificatore
    """
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Aggiunto da",
                                   null=True, blank=True, on_delete=models.SET_NULL, editable=False, related_name="%(class)s_added_related")
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Modificato da",
                                    null=True, blank=True, on_delete=models.SET_NULL, editable=False, related_name="%(class)s_modified_related")

    def save(self, *args, **kwargs):
        """ Al salvataggio dell'elemento, aggiorna i campi """
        if self.created_by is None:
            user = get_current_user()
            if user and not user.pk:
                user = None
            if not self.pk:
                self.created_by = user
            self.modified_by = user
        super(UserReferencedModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class GeoModel(models.Model):
    """
    Una classe di modelli astratta che rende disponibili
    dei metodi utili ad estrarre automaticamente dagli elementi
    delle coordinate. Se le geometrie non sono punti, vengono utilizzati i centroidi. ATTENZIONE: si assume che il campo che immagazzina
    le coordinate sia nominato `geom'
    """

    @property
    def latitude(self):
        """ Estrae la latitudine (Y) nel sistema di riferimento nativo dalla geometria """
        g = self.geom
        if g is not None:
            coord = None
            if isinstance(g, models.PointField) or isinstance(g, Point):
                coord = g.y
            else:
                coord = g.centroid.coords[1]
            return round(coord)

    @property
    def latitude_wgs84(self):
        """ Estrae la latitudine (Y) WGS84 dalla geometria """
        g = self.geom
        if g is not None:
            coord = None
            google_coord = SpatialReference(4326)
            my_coord = SpatialReference(g.srid)
            trans = CoordTransform(my_coord, google_coord)
            g.transform(trans)

            if isinstance(g, models.PointField) or isinstance(g, Point):
                coord = g.y
            else:
                coord = g.centroid.coords[1]

            return round(coord, 6)

    @property
    def latitude_wgs84_imp(self):
        """ Estrae la latitudine (Y) WGS8 troncata alla
        seconda cifra decimale dalla geometria """
        g = self.geom
        if g is not None:
            coord = None
            google_coord = SpatialReference(4326)
            my_coord = SpatialReference(g.srid)
            trans = CoordTransform(my_coord, google_coord)
            g.transform(trans)

            if isinstance(g, models.PointField) or isinstance(g, Point):
                coord = g.y
            else:
                coord = g.centroid.coords[1]

            return round(coord, 2)

    @property
    def longitude(self):
        """ Estrae la longitudine (X) nl sistema di riferimento nativo dalla geometria """
        g = self.geom
        if g is not None:
            coord = None
            if isinstance(g, models.PointField) or isinstance(g, Point):
                coord = g.x
            else:
                coord = g.centroid.coords[0]

            return round(coord)

    @property
    def longitude_wgs84(self):
        """ Estrae la longitudine (X) WGS84 dalla geometria """
        g = self.geom
        if g is not None:
            coord = None
            google_coord = SpatialReference(4326)
            my_coord = SpatialReference(g.srid)
            trans = CoordTransform(my_coord, google_coord)
            g.transform(trans)

            if isinstance(g, models.PointField) or isinstance(g, Point):
                coord = g.x
            else:
                coord = g.centroid.coords[0]
            return round(coord, 6)

    @property
    def longitude_wgs84_imp(self):
        """ Estrae la longitudine (X) troncata alla seconda cifra decimale dalla geometria """
        g = self.geom
        if g is not None:
            coord = None
            google_coord = SpatialReference(4326)
            my_coord = SpatialReference(g.srid)
            trans = CoordTransform(my_coord, google_coord)
            g.transform(trans)

            if isinstance(g, models.PointField) or isinstance(g, Point):
                coord = g.x
            else:
                coord = g.centroid.coords[0]
            return round(coord, 2)

    @property
    def coord_resampled_precis_m(self):
        """
        Applicazione della formula dell'emisenoverso per il calcolo della
        great circle distance tra due punti, in particolare per il calcolo dell'imprecisione
        risultante dal troncamento delle coordinate WGS84 alla seconda cifra decimale
        """
        if self.geom is not None:
            lat = self.latitude_wgs84
            lon = self.longitude_wgs84
            lat_imp = self.latitude_wgs84_imp
            lon_imp = self.longitude_wgs84_imp

            r = 6378.137
            dLat = lat * math.pi / 180 - lat_imp * math.pi / 180
            dLon = lon * math.pi / 180 - lon_imp * math.pi / 180
            a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat * math.pi / 180) * \
                math.cos(lat_imp * math.pi / 180) * \
                math.sin(dLon/2) * math.sin(dLon/2)
            c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))

            return int(r * c * 1000)

        else:
            return None

    class Meta:
        abstract = True


class ShortTextField(models.TextField):
    """
    Campo di testo con preimpostato un widget di lunghezza
    più moderata rispetto a quella predefinita di `models.TextField'
    """

    def formfield(self, **kwargs):
        kwargs.update(
            {"widget": TextInput(attrs={'size': 40})}
        )
        return super(ShortTextField, self).formfield(**kwargs)


class SRID(models.Model):
    """
    Da un database spazialmente abilitato, la tabella
    mostra i codici deisistemi di riferimento geografici
    disponibili
    """
    srid = models.AutoField("SRID", primary_key=True)

    class Meta:
        managed = False
        db_table = '"public"."spatial_ref_sys"'
        verbose_name = 'SRID'
        verbose_name_plural = 'SRIDs'
        ordering = ['srid']
        app_label = 'biostream'

    def __str__(self):
        return "%s" % (self.srid)

