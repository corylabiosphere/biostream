from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

import debug_toolbar

from django.contrib import admin
from django.conf import settings
from django.shortcuts import render
from django.conf.urls.static import static
from django.urls import include, path, re_path
from django.conf.urls import (handler400, handler403, handler500, handler404)

import profiles.urls
import accounts.urls
import tabular_details.urls
import data_requests.urls

from biodiv.urls import router as databio_router
from map.urls import router as map_router
from map.urls import urlpatterns as map_urls
from taxon.urls import router as taxon_router
from taxon.urls import urlpatterns as taxon_patterns


from map import views as map_views
from taxon import views as taxon_views
from biodiv import views as biodiv_views
from . import views

from filebrowser.sites import site
from rest_framework import routers
from django.utils.safestring import mark_safe


# Docs
schema_view = get_schema_view(
    openapi.Info(
        title="BioSTREAM API",
        default_version='v1',
        description="",
        # terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="biostreamportal@gmail.com"),
        license=openapi.License(name="CC-BY-4.0"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

class HomeAPIBioSTREAMView(routers.APIRootView):
    """
    Da qua si può accedere alle varie risorse per i programmatori che il sito mette a disposizione
    """

    def get_view_name(self):
        return "Application Programming Interface del BioSTREAM"

    def get_view_description(self, html=False):
        text = "Da qua si può accedere alle varie risorse per i programmatori che il sito mette a disposizione"
        if html:
            return mark_safe("<p>{}</p>".format(text))
        else:
            return text


class ContainerRouter(routers.DefaultRouter):
    '''
    Contenitore che permette l'aggiunta progressiva di più routers 
    '''
    APIRootView = HomeAPIBioSTREAMView

    def register_router(self, router):
        self.registry.extend(router.registry)


router = ContainerRouter()
# Import e registrazione dei router dalle varie parti del progetto
router.register_router(databio_router)
router.register_router(map_router)
router.register_router(taxon_router)

# router.register_router(tabular_details_router)

# Personalized admin site settings like title and header
admin.site.site_title = 'Amministrazione sito BioSTREAM'
admin.site.site_header = 'Amministrazione sito'

handler404 = 'biostream.views.custom_page_not_found_view'
handler500 = 'biostream.views.custom_error_view'
handler403 = 'biostream.views.custom_permission_denied_view'
handler400 = 'biostream.views.custom_bad_request_view'


urlpatterns = [
    path("", views.HomePage.as_view(), name="home"),
    path("", include(accounts.urls)),
    path("", include(tabular_details.urls)),
    path('admin/filebrowser/', site.urls),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path("admin/", admin.site.urls),
    path('gis/', map_views.GisPage.as_view(), name='gis'),
    path('catalog/', views.CatalogPage.as_view(), name='catalogo'),
    path('specie-catalog/', views.SpecieCatalogPage.as_view(), name='specie-catalog'),
    path('checklist/', map_views.CheckListPage.as_view(), name='checklist'),
    path("about/", views.AboutPage.as_view(), name="about"),
    path("intro/", views.IntroductionPage.as_view(), name="intro"),
    path("organization/", views.OrganizationPage.as_view(), name="organization"),
    path("docs/", views.DocumentsPage.as_view(), name="docs"),
    path("docs-dev/", include('docs.urls')),
    path("taxon-nonmatch/", views.TaxonNonMatchPage.as_view(), name="taxon-nonmatch"),    
    path("observation-detail/<uuid:uuid>",views.ObservationDetailPage.as_view(), name="observation-detail"),
    path("documentation/", views.DocumentationPage.as_view(),name="documentation_base"),
    path("documentation-gis/", views.DocumentationGisPage.as_view(),name="documentation_gis"),
    path("documentation-catalog/", views.DocumentationCatalogPage.as_view(),name="documentation_catalog"),
    path("documentation-checklist/", views.DocumentationChecklistPage.as_view(),name="documentation_checklist"),
    path("documentation-datarequest/", views.DocumentationDataRequestPage.as_view(),name="documentation_datarequest"),
    path("documentation-programmers/", views.DocumentationProgrammersPage.as_view(),name="documentation_programmers"),
    path("documentation-usermanager/", views.DocumentationUserManagerPage.as_view(),name="documentation_usermanager"),
    path("data-requests/", include(data_requests.urls)),
    path('status/', views.StatusPage.as_view(), name='statuspage'),
    path("users/", include(profiles.urls)),
    path('nested_admin/', include('nested_admin.urls')),
    path('tinymce/', include('tinymce.urls')),
    path('watchman/', include('watchman.urls'), name='status'),
] + map_urls + taxon_patterns + [
    # URL della API
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # Un solo include per tutte le URL, così da tenere /api come radice
    path('api/', include(router.urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Include django debug toolbar if DEBUG is on
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]

# Errors


def page_not_found_view(request, exception, template_name='errors/404.html'):
    response = render(request, template_name)
    response.status_code = 404
    return response


handler404 = page_not_found_view


def error_view(request, template_name='errors/500.html'):
    response = render(request, template_name)
    response.status_code = 500
    return response


handler500 = error_view


def permission_denied_view(request, exception, template_name='errors/403.html'):
    response = render(request, template_name)
    response.status_code = 403
    return response


handler403 = permission_denied_view


def bad_request_view(request, exception, template_name='errors/400.html'):
    response = render(request, template_name)
    response.status_code = 400
    return response


handler400 = bad_request_view
