"""
Django settings for biostream project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from sentry_sdk.integrations.django import DjangoIntegration
import sentry_sdk
from django.contrib import messages
import environ
from django.urls import reverse_lazy
from pathlib import Path
import os

SITE_ID = 1

# Build paths inside the project like this: BASE_DIR / "directory"
BASE_DIR = Path(__file__).resolve().parent.parent.parent
STATICFILES_DIRS = [str(BASE_DIR / "static")]

STATIC_URL = "/static/"
MEDIA_URL = "/media/"

STATIC_ROOT = str(BASE_DIR.parent / "static_cdn")
MEDIA_ROOT = str(BASE_DIR.parent / "media_cdn")

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            str(BASE_DIR / "templates"),
            # insert more TEMPLATE_DIRS here
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                'django.template.context_processors.request',
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

# Use 12factor inspired environment variables or from a file

env = environ.Env()

# Create a local.env file in the settings directory
# But ideally this env file should be outside the git repo
env_file = Path(__file__).resolve().parent / "local.env"
if env_file.exists():
    environ.Env.read_env(str(env_file))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ
SECRET_KEY = env("SECRET_KEY")

ALLOWED_HOSTS = ["46.101.184.103",
                 ".biostreamportal.net", "localhost", "127.0.0.1"]

# Application definition

INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'dal',
    'dal_select2',  # https://django-autocomplete-light.readthedocs.io/en/master/install.html
    'filebrowser',
    'admin_interface',
    'colorfield',
    'django.contrib.gis',
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.sites',
    "django.contrib.postgres",
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admindocs',
    "django.contrib.staticfiles",
    'disable_cache_headers.apps.DisableCacheHeadersConfig',

    'sorl.thumbnail',
    'import_export',

    "mptt",
    'captcha',

    "authtools",
    "crispy_forms",
    "easy_thumbnails",
    'django_extensions_shell',
    'corsheaders',
    'bootstrap_italia_template',
    'sass_processor',
    'nested_admin',
    # https://sunscrapers.com/blog/10-django-packages-you-should-know/
    # https://vsupalov.com/favorite-django-packages-2019/
    'tinymce',
    'localflavor',
    'django_filters',
    'django_admin_listfilter_dropdown',
    'prettyjson',
    # https://django-simple-history.readthedocs.io/en/latest/quick_start.html#install
    # 'simple_history',
    # https://django-model-utils.readthedocs.io/en/latest/setup.html
    'rest_framework',
    'drf_yasg',
    'rest_pandas',
    'rest_framework_gis',
    'rest_framework_xml',
    'rest_framework_datatables',
    #'rest_framework_filters',
    #'rest_framework_word_filter',
    #'rest_framework_swagger',
    'leaflet',
    'djgeojson',
    #'data_wizard',
    #'data_wizard.sources',
    'watchman',

    "biostream",
    "biodiv.apps.BiodivConfig",
    "data_requests.apps.DataRequestsConfig",
    "profiles.apps.ProfileConfig",
    "accounts",

    "lu_tables.apps.LuTablesConfig",
    "map",
    "taxon.apps.TaxonConfig",
    "tabular_details",
    "docs",
    # utils
    "django_admin_generator", # https://django-admin-generator.readthedocs.io/en/latest/usage.html#install
    # https://github.com/un1t/django-cleanup
    'django_cleanup.apps.CleanupConfig', # to be placed last
)
DOCS_ROOT = str(BASE_DIR.parent / 'docs/_build/html')
DOCS_ACCESS = 'staff'

# This can be omitted to use the defaults
DATA_WIZARD = {
    'BACKEND': 'data_wizard.backends.threading',
    'LOADER': 'data_wizard.loaders.FileLoader',
    'PERMISSION': 'rest_framework.permissions.IsAdminUser',
}

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    'django_currentuser.middleware.ThreadLocalUserMiddleware',
    'crum.CurrentRequestUserMiddleware',
]
CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = "biostream.urls"

WSGI_APPLICATION = "biostream.wsgi.application"

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

# DATABASES = {
# Raises ImproperlyConfigured exception if DATABASE_URL not in
# os.environ
#    "default": env.db()
# }

DATABASES = {
    # If you syncdb against default databases, all tables for the Managed = True models will get created in django schema
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': env("DATABASE_NAME"),
        'USER': env("DATABASE_USER"),
        'PASSWORD': env("DATABASE_PW"),
        'HOST': 'localhost',
        'PORT': env("DATABASE_PORT"),
        'OPTIONS': {
                'options': '-c search_path=django,lu_tables,taxon,data_biodiv,data_adm,data_cult,data_geomorph,data_landcover,data_raster,tools,public'
        },
        'ATOMIC_REQUESTS': True
    },
}
#DATABASE_ROUTERS = ['biostream.routers.DBRouter', ]

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/
LANGUAGE_CODE = 'it-IT'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Crispy Form Theme - Bootstrap 3
CRISPY_TEMPLATE_PACK = "bootstrap3"

# For Bootstrap 3, change error alert to 'danger'

MESSAGE_TAGS = {messages.ERROR: "danger"}

# Authentication Settings
AUTH_USER_MODEL = "authtools.User"
LOGIN_REDIRECT_URL = reverse_lazy("profiles:show_self")
LOGIN_URL = reverse_lazy("accounts:login")

THUMBNAIL_EXTENSION = "png"  # Or any extn for your thumbnails

DATA_WIZARD = {
    'BACKEND': 'data_wizard.backends.threading',
    'LOADER': 'data_wizard.loaders.FileLoader',
    'PERMISSION': 'rest_framework.permissions.IsAdminUser',
}
PANDAS_RENDERERS = {
    'rest_pandas.renderers.PandasCSVRenderer',
}

REST_FRAMEWORK = {
    # https://www.django-rest-framework.org/api-guide/parsers/
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework_xml.parsers.XMLParser',
        'rest_framework.parsers.JSONParser',
        #'rest_framework.parsers.FormParser',
        #'rest_framework.parsers.MultiPartParser',
        #'rest_framework.parsers.FileUploadParser',        
    ),    
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework_datatables.renderers.DatatablesRenderer',
        'rest_framework_xml.renderers.XMLRenderer',
        #'rest_framework_csv.renderers.CSVRenderer',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        #'rest_framework_filters.backends.RestFrameworkFilterBackend',
        'rest_framework_datatables.filters.DatatablesFilterBackend',
        'django_filters.rest_framework.DjangoFilterBackend',

    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework_datatables.pagination.DatatablesLimitOffsetPagination',
    'PAGE_SIZE': 10,
}
SHELL_PLUS_PRINT_SQL = False

TINYMCE_DEFAULT_CONFIG = {
    'height': 360,
    'width': 1120,
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 20,
    'selector': 'textarea',
    'theme': 'modern',
    'plugins': '''
            textcolor save link image media preview codesample contextmenu
            table code lists fullscreen  insertdatetime  nonbreaking
            contextmenu directionality searchreplace wordcount visualblocks
            visualchars code fullscreen autolink lists  charmap print  hr
            anchor pagebreak
            ''',
    'toolbar1': '''
            fullscreen preview bold italic underline | fontselect,
            fontsizeselect  | forecolor backcolor | alignleft alignright |
            aligncenter alignjustify | indent outdent | bullist numlist table |
            | link image media | codesample |
            ''',
    'toolbar2': '''
            visualblocks visualchars |
            charmap hr pagebreak nonbreaking anchor |  code |
            ''',
    'contextmenu': 'formats | link image',
    'menubar': True,
    'statusbar': True,
}

FILEBROWSER_DIRECTORY = ''
DIRECTORY = ''
FILEBROWSER_MAX_UPLOAD_SIZE = 50000000
FILEBROWSER_EXTENSIONS = {
    'Image': ['.jpg', '.jpeg', '.gif', '.png', '.tif', '.tiff'],
    'Document': ['.pdf', '.doc', '.rtf', '.txt', '.xls', '.xlxs', '.csv'],
    'Archivio': ['.zip', '.rar'],
    # 'Audio': ['.mp3','.mp4','.wav','.aiff','.midi','.m4p']
}

DEFAULT_FROM_EMAIL = 'BioSTREAM Admin <biostreamportal@gmail.com>'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
#EMAIL_PORT = 25
#EMAIL_USE_TLS = False
EMAIL_USE_TLS = True
SERVER_EMAIL = env("EMAIL_HOST_USER")
EMAIL_HOST_USER = env("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = env("EMAIL_HOST_PASSWORD")
DEFAULT_FROM_EMAIL = 'BioSTREAM Admin <biostreamportal@gmail.com>'

def before_send(event, hint):
    """Don't log django.DisallowedHost errors in Sentry."""
    if 'log_record' in hint:
        if hint['log_record'].name == 'django.security.DisallowedHost':
            return None

    return event
sentry_sdk.init(
    dsn="https://180f1f8c81ed4a37948d5e3772600304@sentry.io/1896956",
    integrations=[DjangoIntegration()],

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,  
    before_send=before_send
)


RECAPTCHA_PUBLIC_KEY = env("RECAPTCHA_PUBLIC_KEY")
RECAPTCHA_PRIVATE_KEY = env("RECAPTCHA_PRIVATE_KEY")
LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (46.5377684, 12.1342377),
    'DEFAULT_ZOOM': 7,
    'MIN_ZOOM': 6,
    'MAX_ZOOM': 12,
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Celery settings
CELERY_BROKER_URL = 'amqp://localhost'
#CELERY_RESULT_BACKEND = "redis://localhost:6379"
