"""
WSGI config for biostream project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/
"""
from django.conf import settings
from django.core.wsgi import get_wsgi_application
import os

os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE", "biostream.settings.development"
)


application = get_wsgi_application()
