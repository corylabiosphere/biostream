# Generated by Django 2.2 on 2020-05-11 14:52

import biostream.utils
import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BioSTREAMArea',
            fields=[
                ('id', models.AutoField(primary_key=True,
                                        serialize=False, verbose_name='ID')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(
                    blank=True, null=True, srid=3035, verbose_name='Geometria')),
            ],
            options={
                'verbose_name': 'Area Progetto BioSTREAM',
                'verbose_name_plural': 'Aree Progetto BioSTREAM',
                'db_table': '"data_adm"."alps_biostream_area"',
                'ordering': ['id'],
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HabitatNatura2000',
            fields=[
                ('id', models.IntegerField(primary_key=True,
                                           serialize=False, verbose_name='ID')),
                ('cod', biostream.models.ShortTextField(
                    blank=True, null=True, verbose_name='Codice')),
                ('den', models.TextField(blank=True,
                                         null=True, verbose_name='Denominazione')),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(
                    blank=True, null=True, srid=3035, verbose_name='Geometria')),
            ],
            options={
                'verbose_name': 'SIC / ZPS',
                'verbose_name_plural': 'SIC / ZPS',
                'db_table': 'data_biodiv"."habitat_natura2000',
                'ordering': ['cod'],
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AreeProtette',
            fields=[
                ('code', biostream.models.ShortTextField(
                    primary_key=True, serialize=False, verbose_name='Codice')),
                ('site_type', biostream.models.ShortTextField(
                    blank=True, null=True, verbose_name='Tipo sito')),
                ('name', biostream.models.ShortTextField(
                    blank=True, null=True, verbose_name='Denominazione')),
                ('biogeographic_region', biostream.models.ShortTextField(
                    blank=True, null=True, verbose_name='Regione biogeografica')),
                ('administrative_region', biostream.models.ShortTextField(
                    blank=True, null=True, verbose_name='Regione')),
                ('sic_zsc', biostream.models.ShortTextField(
                    blank=True, null=True, verbose_name='SIC o ZSC?')),
                ('zps', biostream.models.ShortTextField(
                    blank=True, null=True, verbose_name='ZPS?')),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(
                    blank=True, null=True, srid=3035, verbose_name='Geometria')),
                ('specie', models.TextField(blank=True,
                                            null=True, verbose_name='Lista delle specie')),
                ('habitat', models.TextField(blank=True,
                                             null=True, verbose_name='Lista degli habitat')),
                ('specie_num', models.TextField(blank=True,
                                                null=True, verbose_name='Numero delle specie')),
                ('habitat_num', models.TextField(blank=True,
                                                 null=True, verbose_name='Numero di habitat')),
            ],
            options={
                'verbose_name': 'SIC / ZPS',
                'verbose_name_plural': 'SIC / ZPS',
                'db_table': 'data_biodiv"."it_sic_zps_checklist',
                'ordering': ['code'],
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='SRID',
            fields=[
                ('srid', models.AutoField(primary_key=True,
                                          serialize=False, verbose_name='SRID')),
            ],
            options={
                'verbose_name': 'SRID',
                'verbose_name_plural': 'SRIDs',
                'db_table': '"public"."spatial_ref_sys"',
                'ordering': ['srid'],
                'managed': False,
            },
        ),
    ]
