from __future__ import unicode_literals
from django.contrib import admin
from authtools.admin import NamedUserAdmin
from .models import Profile
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.html import format_html

User = get_user_model()


class UserProfileInline(admin.StackedInline):
    """
    Dettagli profilo utente
    """
    model = Profile
    fields = ('picture', 'bio', 'declared_institution',)

    def get_fields(self, request, obj=None):
        fields = super(UserProfileInline, self).get_fields(request, obj)
        if request.user.is_superuser:
            fields += ('email_verified', 'institution_verified','validation_referee',)

        return fields


class NewUserAdmin(NamedUserAdmin):
    """
    Creatore di un nuovo utente
    """
    inlines = [UserProfileInline]
    list_display = (
        "is_active",
        "email",
        "name",
        "permalink",
        "is_superuser",
        "is_staff",
    )

    def permalink(self, obj):
        url = reverse("profiles:show", kwargs={"slug": obj.profile.slug})
        # Unicode hex b6 is the Pilcrow sign
        return format_html('<a href="{}">{}</a>'.format(url, "\xb6"))


admin.site.unregister(User)
admin.site.register(User, NewUserAdmin)
