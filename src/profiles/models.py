from __future__ import unicode_literals
import uuid
from django.db import models
from django.conf import settings

from biodiv.models import Institution
from django.core.exceptions import ValidationError


class BaseProfile(models.Model):
    """
    Profilo base
    """
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True
    )
    slug = models.UUIDField(default=uuid.uuid4, blank=True, editable=False)

    picture = models.ImageField(
        "Immagine del profilo", upload_to="profile_pics/%Y-%m-%d/", null=True, blank=True
    )
    motivation = models.CharField("Motivo d'iscrizione al Portale", max_length=1000, blank=True,default='')
    bio = models.CharField("Breve bio", max_length=200, blank=True, null=True)
    declared_institution = models.ForeignKey(
        Institution, models.CASCADE, blank=True, null=True, verbose_name='Istituzione di appartenenza dichiarata')
    declared_institution_nonpresent = models.CharField('Istituzione di appartenenza se non presente nel menu',blank=True, null=True,max_length=300)
    email_verified = models.BooleanField("Email verificata", default=False)
    validation_referee = models.BooleanField("Referente per la validazione dei dati", default=False)
    institution_verified = models.BooleanField("Istituzione verificata", default=False)

    class Meta:
        abstract = True

    def clean(self):
        super(BaseProfile, self).clean()

        if self.declared_institution and self.declared_institution_nonpresent:
            raise ValidationError('Specificare la propria istituzione una sola volta!')



# @python_2_unicode_compatible
class Profile(BaseProfile):
    """
    Profilo completo
    """

    def __str__(self):
        return "Profilo di {}".format(self.user)
