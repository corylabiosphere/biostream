from __future__ import unicode_literals
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions
from django.contrib.auth import get_user_model
from . import models

from django.urls import reverse

User = get_user_model()


class UserForm(forms.ModelForm):
    """
    Form per i dettagli dell'utente base
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(Field("name"))

    class Meta:
        model = User
        fields = ["name"]


class ProfileForm(forms.ModelForm):
    """
    Form per i dettagli del profilo

    .. image:: ../../_static/manual/biostream_24.png
        :width: 400
        :alt: Immagine del form di dettaglio profilo

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Field("picture"),
            Field("motivation"),
            Field("declared_institution"),
            Field("bio"),
            Div(Submit("update", "Aggiorna", css_class="btn-success"),
                HTML(
                '<a class="btn btn-default" href="{}">Cambia password</a>'.format(
                    reverse("accounts:password-change")
                )
            ), css_class="form-row")
        )

    class Meta:
        model = models.Profile
        fields = ["picture", "motivation","bio", "declared_institution"]
