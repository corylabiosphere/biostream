// Variabili per il tooltip di misura area e disegno
var sketch;
var helpTooltipElement;
var helpTooltip;
var measureTooltipElement;
var measureTooltip;
var continuePolygonMsg = 'Clicca per continuare a disegnare il poligono';
var draw; 
var drawSource = new ol.source.Vector({wrapX: false});
var drawVector = new ol.layer.Vector({
    source: drawSource,
    displayInLayerSwitcher: false,			
    style: new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(233, 71, 24, 0.2)'
        }),
        stroke: new ol.style.Stroke({
            color: '#E94718',
            width: 2
        }),
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: '#E94718'
            })
        })
    })			
});