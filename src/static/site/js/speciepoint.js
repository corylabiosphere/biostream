var habclasses =  ["II*,IV","II,IV","II*","II","IV","V","Non habitat"]
var habclassescolors = ["#E74C3C","#ffb619","#8E44AD","#2980B9","#16A085","#F1C40F","#95A5A6"]
var createPointMultipleSpeciesVectorLayer = function(title,url) {
    var legend = "<br>Punti specie:";
    for(var i=0;i<habclasses.length;i++) {
        legend = legend + "<i style='color:"+habclassescolors[i]+"' class='fas fa-circle'> </i>"+habclasses[i]; 
    }
    var vectorSource = loadGeoJSONVectorSource(url,legend);
    return new ol.layer.Vector({
        title: title,
        name: title,
        url: url,
        geotype: 'point',
        layerkind: 'static-specie-point',
        source: vectorSource,
        attributionsCollapsible: true,
        style: function(feature, resolution){
            var styleHABIIPIV = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: habclassescolors[0]
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                    })
                })
            });
            var styleHABIIIV = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: habclassescolors[1]
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                    })
                })
            });						
            var styleHABIIP = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: habclassescolors[2]
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                    })
                })
            });				
            var styleHABII = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: habclassescolors[3]
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                    })
                })
            });
            var styleHABIV = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: habclassescolors[4]
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                    })
                })
            });
            var styleHABV = new ol.style.Style({
                image: new ol.style.Circle( {
                    radius: 5,
                    fill: new ol.style.Fill({
                        color: habclassescolors[5]
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                    })
                })
            });
            var styleCatchAll = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 4,
                    fill: new ol.style.Fill({
                        color: habclassescolors[6]
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'white',
                        width: 1.5
                    })
                })
            });

            if ( feature.get('habitatdir') == "II*,IV") {
                return [styleHABIIPIV];
            } else if( feature.get('habitatdir') == "II*" ) {
                return [styleHABIIP];
            } else if( feature.get('habitatdir') == "II,IV" ) {
                return [styleHABIIIV];
ù					} else if( feature.get('habitatdir') == "II" ) {
                return [styleHABII];
            } else if( feature.get('habitatdir') == "IV" ) {
                return [styleHABIV];
            } else if( feature.get('habitatdir') == "V" ) {
                return [styleHABV];
            } else {
                return [styleCatchAll];
            }
        }		
    });
};