function elem_id(id) {
    return document.getElementById(id);
};
function ajax(options) {
    return new Promise(function (resolve, reject) {
        $.ajax(options).done(resolve).fail(reject);
    });
};
function roundMultiArray(array, precision) {
    return array.map(function iter(a) {
        if (Array.isArray(a)) {
            return a.map(iter);
        }
        if(precision) {
            return Math.round(a * 10**precision) / 10**precision
        }
        else Math.round(a)
    });
};
function getJson(url) {
    return JSON.parse($.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        global: false,
        async: false,
        success: function (data) {
            return data;	
        }
    }).responseText);
};

var basePolygonStyle = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(233, 71, 24, 0.2)'
    }),
    stroke: new ol.style.Stroke({
        color: '#E94718',
        width: 2
    }),
    image: new ol.style.Circle({
        radius: 7,
        fill: new ol.style.Fill({
            color: '#E94718'
        })
    })
});

var loadGeoJSONVectorSource = function(url,attrib) {
    var source = new ol.source.Vector({
        attributions: [attrib],
        loader: function () {
            this.set('loadstart', Math.random());	
            $.ajax({
                type: 'GET',
                url: url,
                context: this
            }).done(function (response) {
                response = response.results;
                var format = new ol.format.GeoJSON();
                source.set('loadend', Math.random());
                this.addFeatures(format.readFeatures(response,
                {
                    dataProjection: mainProjection,
                    featureProjection: mainProjection
                }))
            });
        },
        crossOrigin: 'anonymous'
    });

    source.set('loadstart', '');
    source.set('loadend', '');

    source.on('change:loadstart', function(evt){
        loadToggle(true);
    });

    source.on('change:loadend', function(evt){
        loadToggle(false);
    });
    return source;
}

function getFilterValues(fields) {
    /**
    * Returns the values from the filter's selects.
    * #param {string[]} fields to explore 
    * @returns {array[]} of values.
    */			
    // fetch value of all non-all filter fields
    // year | altitude | region | province | code | baseoss | dataorigin_name  
    var values = [[],[]];
    // Build array of values which comprise the filter
    for(var i=0;i<fields.length;i++) {
        if(fields[i]=="year") {
            var value = $("#filter_" + fields[i]).data("value");
        } else {
            var value = $("#filter_" + fields[i]).val();
        };
        if(value != 'all' && value!= ''){
            values[0].push(fields[i]);
            values[1].push(value);
        } 
    }
    return values;
}

function getFilterURL(values) {
/**
* Builds a filtering property for URL based on input values.
* 
* @param {string[fields][values]}
* @returns {string} of URL part.
*/			
    var URL;
    for(var i=0;i<values[0].length;i++) {
        if(i==0) {
            URL = "&" + values[0][i] + "=" + values[1][i]
        } else {
            URL = URL + "&" + values[0][i] + "=" + values[1][i]
        }
    }
    return URL;
}

function extractUrlValue(key, url)
{
    if (typeof(url) === 'undefined')
        url = window.location.href;
    var match = url.match('[?&]' + key + '=([^&]+)');
    return match ? match[1] : null;
}

function getRandomColor(opacity) {
    return 'rgba('+Math.floor(Math.random()*256)+','+Math.floor(Math.random()*256)+','+Math.floor(Math.random()*256)+','+opacity+')';
};

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    csvFile = new Blob([csv], {type: "text/csv"});
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");	
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        csv.push(row.join(","));		
    }
    download_csv(csv.join("\n"), filename);
}


var loadTempLayer = function(url,id) {
    loadToggle(true);
    $.ajax({
        type: 'GET',
        url: url,
        context: this
    }).done(function (resp) {
        loadToggle(false);
        if(resp!=undefined) {
            var feats = resp.results;
            
            if(resp.results.features.length==1) {
                var bbo = feats.features[0].bbox;
                var geo = feats.features[0].geometry;	
            } else {
                var geo = feats;	
            }
            if(!(typeof vect==='undefined')) {
                vect.setSource(
                    new ol.source.Vector({
                        features: (new ol.format.GeoJSON({})).readFeatures(geo)
                    })								
                )
            } else {
                vect = new ol.layer.Vector({
                    displayInLayerSwitcher: false,
                    allwaysOnTop: true,noSwitcherDelete: true,visible: true,queryable: false,
                    geotype: 'polygon',layerkind: 'temp',			
                    source:	new ol.source.Vector({
                        features: (new ol.format.GeoJSON({})).readFeatures(geo)
                    }),
                    style: basePolygonStyle						
                });	
                map.addLayer(vect);
            }
            if(resp.results.features.length==1) {
                map.getView().fit(bbo,map.getSize());
            }
        };					
    });			
};

var styleGridFunction = function(f,res){
    var max = 100;
    var opacity = Math.min(1,f.get('features').length/max);
    return [ new ol.style.Style({ fill: new ol.style.Fill({ color: [0,0,255,opacity] }) }) ];
};

var createBoxSpeciesVectorLayer = function(taxonName,taxonRank,url,col1,col2) {
    var vectorSource = loadGeoJSONVectorSource(url,null);
    return new ol.layer.Vector({
        title: taxonName,
        name: taxonName,
        rank: taxonRank,
        url: url,
        filters: '',
        colors: [ col1, col2 ],
        geotype: 'grid',
        layerkind: 'specie-grid',
        source: vectorSource,
        style:	new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: col1,
                width: 3
            }),
            fill: new ol.style.Fill({
                color: col2
            })
        })
    });
};

var createPointSpeciesVectorLayer = function(taxonName,taxonRank,url,col1,col2) {
    var vectorSource = loadGeoJSONVectorSource(url,null);
    return new ol.layer.Vector({
        title: taxonName,
        name: taxonName,
        rank: taxonRank,
        url: url,
        filters: '',
        colors: [ col1, col2 ],
        geotype: 'point',
        layerkind: 'specie-point',
        source: vectorSource,
        style:	new ol.style.Style({
            image: new ol.style.Circle({
                radius: 4,
                fill: new ol.style.Fill({
                    color: col2
                }),
                stroke: new ol.style.Stroke({
                    color: col1,
                    width: 1.5
                })
            })
          })		
    });
};

var formatArea = function(polygon) {
    var area = ol.sphere.getArea(polygon);
    var output;
    if (area > 10000) {
        output = (Math.round(area / 1000000 * 100) / 100) +
            ' ' + 'km<sup>2</sup>';
    } else {
        output = (Math.round(area * 100) / 100) +
            ' ' + 'm<sup>2</sup>';
    }
    return output;
};

var getSizeFromZoom = function(z) {
    var sizes = [30000,20000,10000,5000,1000];
    var zooms = [8,9,10,11,12];
    if(z<=zooms[0]) {
        return sizes[0];
    } else if(z>=zooms[zooms.length-1]) {
        return sizes[sizes.length-1];
    } else {
        return sizes[zooms.indexOf(z)];
    }
};


var createUltraTable = function(url) {
    loadToggle(true);
    tableDestroy('ultratable');
    $('#ultratable-container').css('visibility','');
    $('#ultratable-container').append("<div class='row speciedownloadtablerow'><div class='col-sm-12 col-md-12 text-center'><button type='button' class='btn btn-primary speciedownloadtable'><i class='fas fa-download'></i></button></div></div>");

    $('#ultratable').DataTable({
        "pageLength": 10,"ordering": false,
        "serverSide": true,"orderCellsTop": true,
        "fixedHeader": true,"scrollX": true,"searching": false,
        "ajax": {
            "url": url,
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
        },
        "columnDefs": [
            {
                targets: 4,
                render: function (data, type, row, meta)
                {
                    data = "<button class=' btn btn-primary dataorigin-info'>" + data + "</button>";
                    return data;
                }
            },
            {
                targets: -1,
                render: function (data, type, row, meta)
                {
                    data = "<button class=' btn btn-primary preciseinfo'><a style='color:white !important;' href='" + data + "'</a>"+ data +"</button>";
                    return data;
                }
            },					
            {
                targets: [0,1],
                render: function (data, type, row, meta)
                {
                    data = "<em>" + data + "</em>";
                    return data;
                }									
            }					
        ]						
    });	
    loadToggle(false);
    document.querySelector('#ultratable-container').scrollIntoView({ behavior: 'smooth' });
};

var clearSelection = function() {
    vectorDragSource.clear();
    selectInteractionForGrids.getFeatures().clear();
};
