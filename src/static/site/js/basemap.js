proj4.defs('EPSG:3035',
'+proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +units=m +no_defs ');
ol.proj.proj4.register(proj4);
const mainProjection = ol.proj.get('EPSG:3035');

var baseLayers = new ol.layer.Group({
    title: 'Sfondi',
    noSwitcherDelete: true,
    layers: [
        new ol.layer.Tile({
            title: "Satellite",
            baseLayer: true,
            visible: true,
            allwaysOnTop: true,
            noSwitcherDelete: true,
            source: new ol.source.XYZ({
                attributions: ['Sfondo satellitare by Esri'],
                attributionsCollapsible: true,
                url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                maxZoom: 23,
                crossOrigin: 'anonymous'
            })
        }),
        new ol.layer.Tile({
            title: "OSM",
            baseLayer: true,
            visible: false,
            allwaysOnTop: true,
            noSwitcherDelete: true,
            source: new ol.source.OSM({crossOrigin: 'anonymous',maxZoom:17}),
        }),			
        new ol.layer.Tile({
            title: "Etichette",
            allwaysOnTop: true,
            noSwitcherDelete: true,
            source: new ol.source.Stamen({ layer: 'terrain-labels', crossOrigin: 'anonymous' })
        }),
        new ol.layer.Tile({
            title: "Regioni italiane",
            allwaysOnTop: false,
            noSwitcherDelete: true,
            visible: false,
            queryable: true,
            layerkind: 'tile-wms',
            source: new ol.source.TileWMS({
                url: 'https://biostreamportal.net/geoserver/biostream_main/wms',
                params: {
                    'FORMAT': "image/png", 
                    'VERSION': '1.1.1',
                    tiled: true,
                    "LAYERS": 'biostream_main:it_regioni',
                    "exceptions": 'application/vnd.ogc.se_inimage',
                    crossOrigin: 'anonymous'
                  }
            })
          }),
        new ol.layer.Tile({
            title: "Province italiane",
            allwaysOnTop: false,
            noSwitcherDelete: true,
            visible: false,
            queryable: true,
            layerkind: 'tile-wms',
            source: new ol.source.TileWMS({
                url: 'https://biostreamportal.net/geoserver/biostream_main/wms',
                params: {
                    'FORMAT': "image/png", 
                    'VERSION': '1.1.1',
                    tiled: true,
                    "LAYERS": 'biostream_main:it_province',
                    "exceptions": 'application/vnd.ogc.se_inimage',
                    crossOrigin: 'anonymous'
                  }
            })
          }),				  
        new ol.layer.Tile({
            title: "Comuni",
            allwaysOnTop: true,
            noSwitcherDelete: true,
            visible: false,
            queryable: true,
            layerkind: 'tile-wms',
            source: new ol.source.TileWMS({
                url: 'https://biostreamportal.net/geoserver/biostream_main/wms',
                params: {
                    'FORMAT': "image/png", 
                    'VERSION': '1.1.1',
                    tiled: true,
                    "LAYERS": 'biostream_main:biostream_area_comuni',
                    "exceptions": 'application/vnd.ogc.se_inimage',
                    crossOrigin: 'anonymous'
                  }
            })
          }),
        new ol.layer.Tile({
            title: "Habitat Natura 2000",
            allwaysOnTop: true,
            noSwitcherDelete: true,
            visible: false,
            queryable: true,
            layerkind: 'tile-wms',
            source: new ol.source.TileWMS({
                url: 'https://biostreamportal.net/geoserver/biostream_main/wms',
                params: {
                    'FORMAT': "image/png", 
                    'VERSION': '1.1.1',
                    tiled: true,
                    "LAYERS": 'biostream_main:habitat_natura2000',
                    "exceptions": 'application/vnd.ogc.se_inimage',
                    crossOrigin: 'anonymous'
                  }
            })
          }),
        new ol.layer.Tile({
            title: "Siti Natura 2000",
            allwaysOnTop: true,
            noSwitcherDelete: true,
            visible: false,
            queryable: true,
            layerkind: 'tile-wms',
            source: new ol.source.TileWMS({
                url: 'https://biostreamportal.net/geoserver/biostream_main/wms',
                params: {
                    'FORMAT': "image/png", 
                    'VERSION': '1.1.1',
                    tiled: true,
                    "LAYERS": 'biostream_main:sites_n2k',
                    "exceptions": 'application/vnd.ogc.se_inimage',
                    crossOrigin: 'anonymous'
                  }
            })
          }),					  			  
        new ol.layer.Tile({
            title: "Aree tampone Dolomiti UNESCO",
            allwaysOnTop: true,
            noSwitcherDelete: true,
            visible: false,
            layerkind: 'tile-wms',
            queryable: false,
            source: new ol.source.TileWMS({
                url: 'https://biostreamportal.net/geoserver/biostream_main/wms',
                params: {
                    'FORMAT': "image/png", 
                    'VERSION': '1.1.1',
                    tiled: true,
                    "LAYERS": 'biostream_main:alps_dolomites_unesco_buffer',
                    "exceptions": 'application/vnd.ogc.se_inimage',
                    tilesOrigin: 4384887.99230349 + "," + 2552980.55028106,
                    crossOrigin: 'anonymous'
                  }
            })
          }),				  				
        new ol.layer.Tile({
            title: "Dolomiti UNESCO",
            allwaysOnTop: true,
            noSwitcherDelete: true,
            visible: false,
            queryable: false,
            layerkind: 'tile-wms',
            source: new ol.source.TileWMS({
                url: 'https://biostreamportal.net/geoserver/biostream_main/wms',
                params: {
                    'FORMAT': "image/png", 
                    'VERSION': '1.1.1',
                    tiled: true,
                    "LAYERS": 'biostream_main:alps_dolomites_area',
                    "exceptions": 'application/vnd.ogc.se_inimage',
                    tilesOrigin: 4384887.99230349 + "," + 2552980.55028106,
                    crossOrigin: 'anonymous'
                  }
            })
          })			
    ]
});

// The Map
const unescoLonLat = [12.1342377, 46.5377684];
var map = new ol.Map({
    target: 'map',
    view: new ol.View({
        zoom: 10,
        center: ol.proj.transform(unescoLonLat, 'EPSG:4326', 'EPSG:3035'),
        projection: mainProjection
    }),
    layers: baseLayers
});


// Main control bar
var mainbar = new ol.control.Bar();
map.addControl(mainbar);

/* Standard Controls */
// Mask control
var maskJson = getJson("https://biostreamportal.net/static/json/mask.json");
var coords = maskJson.features[0].geometry.coordinates;
var maskLayer = new ol.Feature(
new ol.geom.MultiPolygon(
        coords
));		
map.getView().fit(maskLayer.getGeometry().getExtent(), map.getSize());
var mask = new ol.filter.Mask({ feature: maskLayer, inner:false, fill: new ol.style.Fill({ color:[255,255,255,0.5] }) });
mainbar.addControl (new ol.control.ZoomToExtent({
    tipLabel:"Zooma alla massima estensione",
    extent: maskLayer.getGeometry().getExtent()
}));
mainbar.addControl(new ol.control.FullScreen({tipLabel: "Attiva/Disattiva la modalità Schermo Intero"}));
mainbar.addControl(new ol.control.Rotate({tipLabel: "Resetta rotazione"}));
baseLayers.getLayers().forEach(function(lyr){
    lyr.addFilter(mask);
});
var toggleMaskControl = new ol.control.Button ({
    html: '<i class="fas fa-mask"></i>',
    className: "toggleMask",
    title: 'Attiva/disattiva la maschera',
    handleClick: function()
    {
        (mask.get('active') ? mask.set('active',false) : mask.set('active',true)); 
    }
});         
mainbar.addControl(toggleMaskControl);
/* Other controls */

var printControl = new ol.control.Print();
printControl.on('print', function(e) {
    $('body').css('opacity', .1);
    console.log('printing')
});
printControl.on(['print', 'error'], function(e) {
$('body').css('opacity', 1);
// Print success
    e.canvas.toBlob(function(blob) {
        saveAs(blob, 'map.png');
    }, 'image/png');
});

// Scale bar
var scaleLineControl = new ol.control.ScaleLine();
map.addControl(scaleLineControl);

var searchLayer = new ol.layer.Vector({
    displayInLayerSwitcher: false,
    source: new ol.source.Vector(),
    style: new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            stroke: new ol.style.Stroke ({
                color: 'rgb(255,165,0)',
                width: 3
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255,165,0,.3)'
            })
        }),
        stroke: new ol.style.Stroke ({
            color: 'rgb(255,165,0)',
            width: 3
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255,165,0,.3)'
        })
    })
});
map.addLayer(searchLayer);
// Set the search control 
var searchControl = new ol.control.SearchNominatim (
    {	//target: $(".options").get(0),
        polygon: $("#polygon").prop("checked"),
        label: 'Cerca',
        placeholder: 'Cerca...',
        reverse: true,
        position: true	// Search, with priority to geo position
    });
// Select feature when click on the reference index
searchControl.on('select', function(e) {	// console.log(e);
    searchLayer.getSource().clear();
    // Check if we get a geojson to describe the search
    if (e.search.geojson) {
        var format = new ol.format.GeoJSON();
        var f = format.readFeature(e.search.geojson, { dataProjection: "EPSG:4326", featureProjection: map.getView().getProjection() });
        searchLayer.getSource().addFeature(f);
        var view = map.getView();
        var resolution = view.getResolutionForExtent(f.getGeometry().getExtent(), map.getSize());
        var zoom = view.getZoomForResolution(resolution);
        var center = ol.extent.getCenter(f.getGeometry().getExtent());
        // redraw before zoom
        setTimeout(function(){
                view.animate({
                center: center,
                zoom: Math.min (zoom, 16)
            });
        }, 100);
    }
    else {
        map.getView().animate({
            center:e.coordinate,
            zoom: Math.max (map.getView().getZoom(),16)
        });
    }
});
// Layer Switcher 
var layerSwitcherControl = new ol.control.LayerSwitcher({
    show_progress: true,
    extent: true,
    trash: true,
    oninfo: function(l){
        $('.options').html(l.get('title')+'<br/>');
    }
});
// The search input
var search = $('<input>').attr('placeholder','Filtra...');
function filterLayers(rex, layers) {
    var found = false;
    layers.forEach(function(l){
        // Layer Group
        if (l.getLayers) {
            if (filterLayers(rex, l.getLayers().getArray())) {
                l.set('noLayer', false);
                found = true;
            } else {
                l.set('noLayer', true);
            }
        } else {
            if (rex.test(l.get('title'))) {
                l.setVisible(true);
                found = true;
            } else {
                l.setVisible(false);
            }
        }
    });
    return found;
}
layerSwitcherControl.setHeader(search.get(0));
map.addControl(layerSwitcherControl);

// Main control bar
var mainbar = new ol.control.Bar();
mainbar.setPosition('bottom');
map.addControl(mainbar);

var secondbar = new ol.control.Bar();
secondbar.setPosition('top');
map.addControl(secondbar);
secondbar.addControl(searchControl);



// FeatureInfo for WMS
map.on('singleclick', function(evt) {
    var viewProjection = map.getView().getProjection();
    // var viewResolution = view.getResolution();			
    document.getElementById('info').innerHTML = '';
    var viewResolution = /** @type {number} */ (map.getView().getResolution());
    baseLayers.forEach(function(lyr){
        if(lyr.get('layerkind') == 'tile-wms' & lyr.getVisible() & lyr.get('queryable')) {
            var url = lyr.getSource().getGetFeatureInfoUrl(
                evt.coordinate, viewResolution, viewProjection,
                {'INFO_FORMAT': 'text/html'});
            if (url) {
                fetch(url)
                .then(function (response) { return response.text(); })
                .then(function (html) {
                    document.getElementById('info').innerHTML += html;
                    $('#info style').remove();
                    $('#info title').remove();
                    $('table.featureInfo').addClass('table');
                    $('caption.featureInfo').appendTo($('table.featureInfo'));
                    $('table.featureInfo').find('td').first().remove();
                    $('table.featureInfo').find('td').first().remove();
                    $('table.featureInfo').find('th').first().remove();
                    $('table.featureInfo').find('th').first().remove();
                });
            }					
        }
    });
});