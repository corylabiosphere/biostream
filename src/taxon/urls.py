from django.urls import path
from . import views
from rest_framework import routers
from .api import (
    TaxonomyViewSet,TaxonStatsViewSet,
    TaxonomyNormalizedViewSet,TaxonProtectionNormalizedViewSet,TaxonomyNonmatchesViewSet
)

router = routers.DefaultRouter()
router.register(r'taxonomy', TaxonomyViewSet,basename='taxonomy')
router.register(r'taxonomy-normalized', TaxonomyNormalizedViewSet,basename='taxonomynormalized')
router.register(r'species', TaxonProtectionNormalizedViewSet,basename='species')
router.register(r'taxonstats', TaxonStatsViewSet,basename='taxonstats')
router.register(r'taxon-nonmatch', TaxonomyNonmatchesViewSet,basename='taxon-nonmatch')

urlpatterns = [
     path('taxon-matcher/', views.correct_taxon_from_observation,name='taxon_matcher'),
    path('taxon/', views.get_taxon, name='taxon'),
    path('taxon-gbif/', views.GBIFTaxonAutocomplete.as_view(), name='taxon-gbif'),
    path('get-accepted-taxon/<int:taxon>/',views.get_accepted_taxon,name="get-accepted-taxon"),    
    path('taxonstats/<taxon>/<rank>', views.taxonstats, name='taxonstats'),

]