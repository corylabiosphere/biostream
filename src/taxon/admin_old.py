

#@admin.register(Name)
class NameAdmin(ReadOnlyModelAdmin):
    list_display = ('scientific_name', 'rank', 'type')  # 'rank', 'type',)
    list_filter = ('rank', 'type')
    search_fields = ('scientific_name', 'rank')
    list_per_page = 15
    fieldsets = (
        ('', {
            'fields': (
                ('scientific_name', 'canonical_name'),
                ('rank', 'type'),
            )
        }),
        ('Dettagli', {
            'fields': (
                'genus_or_above',
                'infra_generic',
                ('specific_epithet', 'infra_specific_epithet', 'cultivar_epithet'),
            )
        }),
        ('Status', {
            'fields': (
                'notho_type',
                'nom_status',
                'remarks',
            ),
            'classes': ('collapse',)
        })
    )