# Generated by Django 3.2.15 on 2022-09-07 12:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taxon', '0011_auto_20220902_1059'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='taxonomynormalized',
            table='taxon"."observation_all_taxonomies',
        ),
        migrations.AlterModelTable(
            name='taxonstats',
            table='taxon"."observation_taxon_stats',
        ),
    ]
