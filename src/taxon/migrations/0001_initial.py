# Generated by Django 2.2.6 on 2019-10-16 08:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Citation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('citation', models.TextField(blank=True, null=True)),
                ('link', models.TextField(blank=True, null=True)),
                ('identifier', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Citazione',
                'verbose_name_plural': 'Citazioni',
                'db_table': 'citation',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Dataset',
            fields=[
                ('key', models.UUIDField(primary_key=True, serialize=False)),
                ('title', models.TextField(blank=True, null=True)),
                ('parent', models.UUIDField(blank=True, null=True)),
                ('publisher', models.UUIDField(blank=True, null=True)),
                ('nub_prio', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Dataset',
                'verbose_name_plural': 'Datasets',
                'db_table': 'dataset',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DatasetMetrics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count_usages', models.IntegerField(blank=True, null=True)),
                ('count_synonyms', models.IntegerField(blank=True, null=True)),
                ('count_names', models.IntegerField(blank=True, null=True)),
                ('count_col', models.IntegerField(blank=True, null=True)),
                ('count_nub', models.IntegerField(blank=True, null=True)),
                ('count_by_rank', models.TextField(blank=True, null=True)),
                ('count_by_kingdom', models.TextField(blank=True, null=True)),
                ('count_by_origin', models.TextField(blank=True, null=True)),
                ('count_vernacular_by_lang', models.TextField(blank=True, null=True)),
                ('count_extensions', models.TextField(blank=True, null=True)),
                ('count_other', models.TextField(blank=True, null=True)),
                ('downloaded', models.DateTimeField(blank=True, null=True)),
                ('created', models.DateTimeField(blank=True, null=True)),
                ('latest', models.BooleanField(blank=True, null=True)),
                ('count_by_issue', models.TextField(blank=True, null=True)),
                ('count_by_constituent', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Metrica  per un dataset',
                'verbose_name_plural': 'Metriche per i dataset',
                'db_table': 'dataset_metrics',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Description',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(blank=True, null=True)),
                ('type', models.TextField(blank=True, null=True)),
                ('language', models.CharField(blank=True, max_length=2, null=True)),
                ('creator', models.TextField(blank=True, null=True)),
                ('contributor', models.TextField(blank=True, null=True)),
                ('license', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Descrizione di un utilizzo di un nome',
                'verbose_name_plural': 'Descrizione degli utilizzi dei nomi',
                'db_table': 'description',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Distribution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location_id', models.TextField(blank=True, null=True)),
                ('locality', models.TextField(blank=True, null=True)),
                ('country', models.CharField(blank=True, max_length=2, null=True)),
                ('occurrence_status', models.TextField(blank=True, null=True)),
                ('threat_status', models.TextField(blank=True, null=True)),
                ('establishment_means', models.TextField(blank=True, null=True)),
                ('appendix_cites', models.TextField(blank=True, null=True)),
                ('start_day_of_year', models.IntegerField(blank=True, null=True)),
                ('end_day_of_year', models.IntegerField(blank=True, null=True)),
                ('start_year', models.IntegerField(blank=True, null=True)),
                ('end_year', models.IntegerField(blank=True, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('temporal', models.TextField(blank=True, null=True)),
                ('life_stage', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Distribuzione per una specie',
                'verbose_name_plural': 'DIstribuzione per le specie',
                'db_table': 'distribution',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Identifier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.TextField()),
                ('identifier', models.TextField(blank=True, null=True, unique=True)),
                ('title', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Identificatore di un nome',
                'verbose_name_plural': 'Identificatori dei nomi',
                'db_table': 'identifier',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Literature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.TextField(blank=True, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'RIsorsa bibliografica',
                'verbose_name_plural': 'Risorse bibliografiche',
                'db_table': 'literature',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.TextField(blank=True, null=True)),
                ('format', models.TextField(blank=True, null=True)),
                ('identifier', models.TextField(blank=True, null=True)),
                ('references', models.TextField(blank=True, null=True)),
                ('title', models.TextField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('audience', models.TextField(blank=True, null=True)),
                ('created', models.DateTimeField(blank=True, null=True)),
                ('creator', models.TextField(blank=True, null=True)),
                ('contributor', models.TextField(blank=True, null=True)),
                ('publisher', models.TextField(blank=True, null=True)),
                ('license', models.TextField(blank=True, null=True)),
                ('rights_holder', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Media',
                'verbose_name_plural': 'Media',
                'db_table': 'media',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Name',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('scientific_name', models.TextField(blank=True, null=True)),
                ('canonical_name', models.TextField(blank=True, null=True)),
                ('type', models.TextField()),
                ('genus_or_above', models.TextField(blank=True, null=True)),
                ('infra_generic', models.TextField(blank=True, null=True)),
                ('specific_epithet', models.TextField(blank=True, null=True)),
                ('infra_specific_epithet', models.TextField(blank=True, null=True)),
                ('cultivar_epithet', models.TextField(blank=True, null=True)),
                ('notho_type', models.TextField(blank=True, null=True)),
                ('parsed_partially', models.BooleanField()),
                ('authorship', models.TextField(blank=True, null=True)),
                ('year', models.TextField(blank=True, null=True)),
                ('year_int', models.IntegerField(blank=True, null=True)),
                ('bracket_authorship', models.TextField(blank=True, null=True)),
                ('bracket_year', models.TextField(blank=True, null=True)),
                ('nom_status', models.TextField(blank=True, null=True)),
                ('sensu', models.TextField(blank=True, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('modified', models.DateTimeField(blank=True, null=True)),
                ('rank', models.TextField(blank=True, null=True)),
                ('parsed', models.BooleanField(blank=True, null=True)),
                ('strain', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Nome',
                'verbose_name_plural': 'Nomi',
                'db_table': 'name',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='NameUsage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('constituent_key', models.UUIDField(blank=True, null=True)),
                ('rank', models.TextField(blank=True, null=True, verbose_name='Rango')),
                ('is_synonym', models.BooleanField()),
                ('pp_synonym_fk', models.IntegerField(blank=True, null=True)),
                ('status', models.TextField(blank=True, null=True)),
                ('nom_status', models.TextField(blank=True, null=True)),
                ('origin', models.TextField(blank=True, null=True)),
                ('remarks', models.TextField(blank=True, null=True)),
                ('modified', models.DateTimeField(blank=True, null=True)),
                ('references', models.TextField(blank=True, null=True)),
                ('taxon_id', models.TextField(blank=True, null=True)),
                ('num_descendants', models.IntegerField(blank=True, null=True)),
                ('last_interpreted', models.DateTimeField(blank=True, null=True)),
                ('issues', models.TextField(blank=True, null=True)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('source_taxon_key', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Utilizzo di un nome',
                'verbose_name_plural': 'Utilizzi dei nomi',
                'db_table': 'name_usage',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='SpeciesInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('marine', models.BooleanField(blank=True, null=True)),
                ('terrestrial', models.BooleanField(blank=True, null=True)),
                ('extinct', models.BooleanField(blank=True, null=True)),
                ('hybrid', models.BooleanField(blank=True, null=True)),
                ('living_period', models.TextField(blank=True, null=True)),
                ('age_in_days', models.IntegerField(blank=True, null=True)),
                ('size_in_millimeter', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
                ('mass_in_gram', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
                ('life_form', models.TextField(blank=True, null=True)),
                ('habitat', models.TextField(blank=True, null=True)),
                ('freshwater', models.BooleanField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Informazione di una specie',
                'verbose_name_plural': 'Informazioni delle specie',
                'db_table': 'species_info',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='VernacularName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(blank=True, null=True)),
                ('language', models.CharField(blank=True, max_length=2, null=True)),
                ('preferred', models.BooleanField(blank=True, null=True)),
                ('sex', models.TextField(blank=True, null=True)),
                ('life_stage', models.TextField(blank=True, null=True)),
                ('area', models.TextField(blank=True, null=True)),
                ('country', models.CharField(blank=True, max_length=2, null=True)),
                ('plural', models.BooleanField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Nome vernacolare',
                'verbose_name_plural': 'Nomi vernacolari',
                'db_table': 'vernacular_name',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='NubRel',
            fields=[
                ('usage_fk', models.OneToOneField(db_column='usage_fk', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='name_usage_primary', serialize=False, to='taxon.NameUsage')),
            ],
            options={
                'verbose_name': 'Relazione tra nomi',
                'verbose_name_plural': 'Relazioni tra i nomi',
                'db_table': 'nub_rel',
                'managed': False,
            },
        ),
    ]
