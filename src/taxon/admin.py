from django.apps import apps
from django.contrib import admin
from biostream.mixins import ListAdminMixin

from django import forms
from django.contrib.gis import admin
from django.urls import reverse
from leaflet.admin import LeafletGeoAdmin, LeafletGeoAdminMixin

from .models import (
    Name, Taxonomy, TaxonStats, TaxonProtection
)
from biostream.admin import (
    ReadOnlyModelAdmin
)



@admin.register(TaxonProtection)
class TaxonProtectionAdmin(admin.ModelAdmin):
    list_display = ('name','habitat_ap_2','habitat_ap_2_prior','habitat_ap_4','habitat_ap_5',)
    list_filter = ('habitat_ap_2','habitat_ap_2_prior','habitat_ap_4','habitat_ap_5','iucn',)
    #search_fields = ['name']
    #autocomplete_fields = ['name']

    list_per_page = 10
    fieldsets = (
        ('Specie',{
            'fields': (
                #('name'),
                'endemism',
            ),
        }),
        ('Protezione italiana',{
            'fields': (
                ('l_157_92_art_2','l_157_92'),
            )
        }),
        ('Direttiva Habitat',{
            'fields': (
                ('habitat_ap_2','habitat_ap_2_prior'),
                ('habitat_ap_4',),
                'habitat_ap_5',
            )
        }),
        ('Direttiva Uccelli',{
            'fields': (
                ('cee_79_409_ap_1',),
                ('cee_79_409_ap_2_i','cee_79_409_ap_2_ii',),
                ('cee_79_409_ap_3_i','cee_79_409_ap_3_ii',),
            )
        }),
        ('Convenzione di Berna',{
            'fields': (
                ('berna_all_1','berna_all_2','berna_all_3'),
            )
        }),
        ('CITES',{
            'fields': (
                ('cites_all_a','cites_all_b','cites_all_d'),
            )
        }),
        ('Convenzione di Bonn',{
            'fields': (
                ('bonn_ap_1','bonn_ap_2'),
            )
        }),
        ('Convenzione di Barcellona',{
            'fields': (
                'barcellona_all_2',
            )
        }),
        ('IUCN',{
            'fields': (
                'iucn',
                'iucn_detail'
            )
        }),
    )

@admin.register(Taxonomy)
class TaxonomyAdmin(ReadOnlyModelAdmin):
    list_display = ('taxonomy_specie', 'taxonomy_family','taxonomy_kingdom', )  # 'rank', 'type',)
    list_filter = ('taxonomy_family', 'taxonomy_ordo', 'taxonomy_class','taxonomy_phylum', 'taxonomy_kingdom',)
    search_fields = ('taxonomy_specie',)
    list_per_page = 15
    fieldsets = (
        ('', {
            'fields': (
                ('taxonomy_specie',),
            )
        }),
        ('Dettagli', {
            'fields': (
                #('taxonomy_subspecie',),
                ('taxonomy_genus','taxonomy_family', 'taxonomy_ordo', 'taxonomy_class',),
                ('taxonomy_phylum', 'taxonomy_kingdom',),
            )
        }),
        ('', {
            'fields': (
                ('is_synonym_by_taxonomy', 'status_by_taxonomy'),
            ),
            'classes': ('collapse',)
        })
    )
@admin.register(TaxonStats)
class TaxonStatsAdmin(ReadOnlyModelAdmin):
    list_display = ('taxon', 'get_rank', 'observations_count',)
    search_fields = ('taxon',)
    list_filter = ('taxon__rank',)
    fieldsets = (
        ('', {
            'fields': (
                ('taxon', 'taxon__rank'),
            )
        }),
        ('Conteggi', {
            'fields': (
                'observations_count',
                'origin_n',
                'habitat_natura2000_n',
                'comuni_n',
                'province_n',
                'regioni_n',
                'sic_zps_n',
                ('month_1_n', 'month_2_n', 'month_3_n',),
                ('month_4_n', 'month_5_n', 'month_6_n',),
                ('month_7_n', 'month_8_n', 'month_9_n',),
                ('month_10_n', 'month_11_n', 'month_12_n',),
            )
        }),
        ('Dettagli', {
            'fields': (
                'origin',
                'habitat_natura2000',
                'comuni',
                'province',
                'regioni',
                'sic_zps',
            ),
            'classes': ('collapse',),
        })
    )

    def get_rank(self, obj):
        return obj.taxon.rank
    get_rank.short_description = 'Rango tassonomico'
    get_rank.admin_order_field = 'taxon__rank'