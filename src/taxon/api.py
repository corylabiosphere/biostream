import django_filters
from django_filters import rest_framework as filters
from rest_flex_fields import filter_backends
from rest_framework import permissions, viewsets
from rest_framework.settings import api_settings
from rest_framework_csv import renderers as r
from rest_framework_datatables.filters import DatatablesFilterBackend


from rest_framework_gis.filters import GeometryFilter, InBBoxFilter

from .models import TaxonProtectionNormalized, Taxonomy, TaxonomyNormalized, TaxonStats, ObservationsTaxonomyNonmatches
from .serializers import (TaxonProtectionNormalizedSerializer,
                          TaxonomyNormalizedSerializer, TaxonomySerializer,
                          TaxonStatsSerializerBase, TaxonStatsSerializerFull,TaxonomyNonmatchesSerializer
                          )

from biostream.renderers import ZippedCSVRenderer

class TaxonomyViewSet(viewsets.ModelViewSet):
    """
    La tassonomia ragionata utilizzata dal Progetto BioSTREAM, sintesi di varie fonti
    """
    queryset = Taxonomy.objects.all()
    serializer_class = TaxonomySerializer
    permission_classes = [
        permissions.AllowAny
    ]
    filterset_fields = ['taxonomy_specie', 'taxonomy']


class TaxonomyNormalizedViewSet(viewsets.ModelViewSet):
    """
    Indice di tutti i nomi tassonomici contenuti nel Progetto BioSTREAM. Utile
    ad esempio per la costruzione di menu
    """
    queryset = TaxonomyNormalized.objects.all()
    serializer_class = TaxonomyNormalizedSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    filterset_fields = ['canonical_name', 'rank']


class TaxonProtectionNormalizedViewSet(viewsets.ModelViewSet):
    """
    Indice di tutti i nomi tassonomici contenuti nel Progetto BioSTREAM, completi di eventuale presenza negli allegati della direttiva habitat. Utile
    ad esempio per la costruzione di menu
    """
    queryset = TaxonProtectionNormalized.objects.filter(
        name_id__isnull=False)
    serializer_class = TaxonProtectionNormalizedSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    filter_backends = (DatatablesFilterBackend,)#RestFrameworkFilterBackend,)
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.PaginatedCSVRenderer, ZippedCSVRenderer, )

    def get_renderer_context(self):
        """
        Ritorna i campi ordinati secondo l'ordine dei campi del serializzatore, oppure secondo l'ordine dei campi del modello.
        Utile per il renderer CSV, che di default ordina i campi alfabeticamente
        """
        context = super().get_renderer_context()
        context['header'] = self.serializer_class.Meta.fields
        if context['header'] == '__all__':
            context['header'] = [f.name for f in self.serializer_class.Meta.model._meta.fields] 
        return context

    def paginate_queryset(self, queryset, view=None):
        if 'no_page' in self.request.query_params:
            return None
        else:
            return self.paginator.paginate_queryset(queryset, self.request, view=self)    


class TaxonStatsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TaxonStats.objects.all()

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return TaxonStatsSerializerFull
        return TaxonStatsSerializerBase


class TaxonomyNonmatchesViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ObservationsTaxonomyNonmatches.objects.all()
    serializer_class = TaxonomyNonmatchesSerializer
    permission_classes = [
        permissions.AllowAny
    ]
    filter_backends = (DatatablesFilterBackend,)#RestFrameworkFilterBackend,)
    renderer_classes = tuple(api_settings.DEFAULT_RENDERER_CLASSES) + (r.PaginatedCSVRenderer, ZippedCSVRenderer, )