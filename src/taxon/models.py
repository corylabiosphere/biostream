from django.db import models
from lu_tables.models import *
from biostream.db_functions import BaseFunctionTableModel, TableFunctionArg, TableFunctionManager
from collections import OrderedDict
from biostream.models import (
    ShortTextField
)
from lu_tables.models import LuIUCNCategory
from django.contrib.postgres.search import SearchVectorField
from django.contrib.postgres.indexes import GinIndex


class Name(models.Model):
    """
    Nomi dei vari taxa
    """
    scientific_name = ShortTextField(
        "Nome scientifico", blank=True, null=True)
    canonical_name = ShortTextField("Nome canonico", blank=True, null=True)
    rank = ShortTextField("Rango", blank=True,
                          null=True, db_column="rank")
    type = ShortTextField("Tipo", blank=True, null=True)
    genus_or_above = ShortTextField(
        "Genere (o taxon superiore)", blank=True, null=True)
    infra_generic = ShortTextField(blank=True, null=True)
    specific_epithet = ShortTextField(
        "Epiteto specifico", blank=True, null=True)
    infra_specific_epithet = ShortTextField(
        "Epiteto infrascpecifico", blank=True, null=True)
    cultivar_epithet = ShortTextField(
        "Epiteto della cultivar", blank=True, null=True)
    notho_type = ShortTextField(blank=True, null=True)
    nom_status = ShortTextField(
        "Status nomenclaturale", blank=True, null=True)
    remarks = ShortTextField("Note", blank=True, null=True)
    scientific_name_vector = SearchVectorField(null=True) 
    canonical_name_vector = SearchVectorField(null=True)


    class Meta:
        managed = False
        db_table = 'taxon"."name'
        unique_together = (('scientific_name', 'rank'),)
        verbose_name = 'Nome'
        verbose_name_plural = 'Nomi'
        indexes = (GinIndex(fields=["scientific_name_vector"]),GinIndex(fields=["canonical_name_vector"]),) 

    def __str__(self):
        return '{} ({})'.format(self.scientific_name, self.rank)

TAXON_RANK_CHOICES = ('DOMAIN', 'SUPERKINGDOM', 'KINGDOM', 'SUBKINGDOM', 'INFRAKINGDOM', 'SUPERPHYLUM', 'PHYLUM', 'SUBPHYLUM', 'INFRAPHYLUM', 'SUPERCLASS', 'CLASS', 'SUBCLASS', 'INFRACLASS', 'PARVCLASS', 'SUPERLEGION', 'LEGION', 'SUBLEGION', 'INFRALEGION', 'SUPERCOHORT', 'COHORT', 'SUBCOHORT', 'INFRACOHORT', 'MAGNORDER', 'SUPERORDER', 'GRANDORDER', 'ORDER', 'SUBORDER', 'INFRAORDER', 'PARVORDER', 'SUPERFAMILY', 'FAMILY', 'SUBFAMILY', 'INFRAFAMILY', 'SUPERTRIBE', 'TRIBE', 'SUBTRIBE', 'INFRATRIBE', 'SUPRAGENERIC_NAME', 'GENUS', 'SUBGENUS', 'INFRAGENUS', 'SECTION', 'SUBSECTION', 'SERIES', 'SUBSERIES', 'INFRAGENERIC_NAME', 'SPECIES_AGGREGATE', 'SPECIES', 'INFRASPECIFIC_NAME', 'GREX', 'SUBSPECIES', 'CULTIVAR_GROUP', 'CONVARIETY', 'INFRASUBSPECIFIC_NAME', 'PROLES', 'RACE', 'NATIO', 'ABERRATION', 'MORPH', 'VARIETY', 'SUBVARIETY', 'FORM', 'SUBFORM', 'PATHOVAR', 'BIOVAR', 'CHEMOVAR', 'MORPHOVAR', 'PHAGOVAR', 'SEROVAR', 'CHEMOFORM', 'FORMA_SPECIALIS', 'CULTIVAR', 'STRAIN', 'OTHER', 'UNRANKED')

class NameUsage(models.Model):
    """
    Utilizzo e relazioni di un certo nome
    """
    name = models.ForeignKey(Name, models.DO_NOTHING, db_column='name_fk')
    constituent_key = models.UUIDField(blank=True, null=True)
    rank = models.ForeignKey(LuRank, models.SET_NULL,
                             blank=True, null=True, db_column="rank")
    parent_fk = models.ForeignKey('self', models.SET_NULL, blank=True, null=True, db_column="parent_fk")
    is_synonym = models.BooleanField("Sinonimo?",blank=True,null=True)
    #pp_synonym_fk = models.IntegerField(blank=True, null=True)
    #status = ShortTextField(blank=True, null=True)
    #nom_status = ShortTextField(blank=True, null=True)
    #basionym_fk = models.ForeignKey('self', models.SET_NULL, blank=True,null=True, verbose_name="Basionimo", related_name='basionym', db_column="basionym_fk")
    kingdom_fk = models.ForeignKey('self', models.SET_NULL, blank=True, null=True, verbose_name="Regno", related_name='kingdom', db_column="kingdom_autofk")
    phylum_fk = models.ForeignKey('self', models.SET_NULL, blank=True, null=True, verbose_name="Phylum", related_name='phylum', db_column="phylum_autofk")
    class_fk = models.ForeignKey('self', models.SET_NULL, blank=True,
                                 null=True, verbose_name="Classe", related_name='classe', db_column="class_autofk")
    order_fk = models.ForeignKey('self', models.SET_NULL, blank=True,
                                 null=True, verbose_name="Ordine", related_name='order', db_column="order_autofk")
    family_fk = models.ForeignKey('self', models.SET_NULL, blank=True,
                                  null=True, verbose_name="Famiglia", related_name='family', db_column="family_autofk")
    genus_fk = models.ForeignKey('self', models.SET_NULL, blank=True,
                                 null=True, verbose_name="Genere", related_name='genus', db_column="genus_autofk")
    #subgenus_fk = models.ForeignKey('self', models.SET_NULL, blank=True,
    #                                null=True, verbose_name="Subgenere", related_name='subgenus', db_column="subgenus_autofk")
    species_fk = models.ForeignKey('self', models.SET_NULL, blank=True, null=True, verbose_name="Specie", related_name='species', db_column="species_autofk")
    origin = ShortTextField(blank=True, null=True)
    #remarks = ShortTextField(blank=True, null=True)
    #modified = models.DateTimeField(blank=True, null=True)
    #references = ShortTextField(blank=True, null=True)
    #taxon_id = ShortTextField(blank=True, null=True)
    #num_descendants = models.IntegerField(blank=True, null=True)
    #last_interpreted = models.DateTimeField(blank=True, null=True)
    #issues = ShortTextField(blank=True, null=True)
    #deleted = models.DateTimeField(blank=True, null=True)
    #source_taxon_key = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'taxon"."name_usage'
        verbose_name = 'Utilizzo di un nome'
        verbose_name_plural = 'Utilizzi dei nomi'

    def __str__(self):
        if not self.class_fk:
            return '{}'.format(self.name.scientific_name)
        elif self.class_fk and self.name:
            return '{} ({})'.format(self.name.scientific_name, self.class_fk.name.scientific_name)
        else:
            return ''

TAXON_STATUS_CHOICES = (
    ('ACCEPTED','ACCEPTED'),
    ('DOUBTFUL','DOUBTFUL'),
    ('SYNONYM','SYNONYM'),
    ('HETEROTYPIC_SYNONYM','HETEROTYPIC_SYNONYM'),
    ('HOMOTYPIC_SYNONYM','HOMOTYPIC_SYNONYM'),
    ('PROPARTE_SYNONYM','PROPARTE_SYNONYM'),
    ('MISAPPLIED','MISAPPLIED'),
    ('INTERMEDIATE_RANK_SYNONYM','INTERMEDIATE_RANK_SYNONYM'),
    ('DETERMINATION_SYNONYM','DETERMINATION_SYNONYM'),
)
class Taxonomy(models.Model):
    """
    Tassonomia semplice relativa alle osservazioni
    """
    taxonomy_specie_id = models.IntegerField("ID Specie",primary_key=True)
    taxonomy_specie = ShortTextField("Specie",blank=True,null=True)
    is_synonym_by_taxonomy = models.BooleanField("Sinonimo?",blank=True,null=True)
    status_by_taxonomy = ShortTextField("Status",choices=TAXON_STATUS_CHOICES,blank=True,null=True)
    taxonomy_genus_id = models.IntegerField("ID Genere",blank=True,null=True)
    taxonomy_genus = ShortTextField("Genere",blank=True,null=True)
    taxonomy_family_id = models.IntegerField("ID famiglia",blank=True,null=True)
    taxonomy_family = ShortTextField("Famiglia",blank=True,null=True)
    taxonomy_ordo_id = models.IntegerField("ID ordine",blank=True,null=True)
    taxonomy_ordo = ShortTextField("Ordine",blank=True,null=True)
    taxonomy_class_id = models.IntegerField("ID classe",blank=True,null=True)
    taxonomy_class = ShortTextField("Classe",blank=True,null=True)
    taxonomy_phylum_id = models.IntegerField("ID phylum",blank=True,null=True)
    taxonomy_phylum = ShortTextField("Phylum",blank=True,null=True)
    taxonomy_kingdom_id = models.IntegerField("ID regno",blank=True,null=True)    
    taxonomy_kingdom = ShortTextField("Regno",blank=True,null=True)

    class Meta:
        managed = False
        db_table = 'taxon"."observations_backbone'
        verbose_name = 'Tassonomia di base'
        verbose_name_plural = 'Tassonomie di base'
        ordering = ['taxonomy_specie']

    def __str__(self):
        if self.taxonomy_specie:
            return "{} ({})".format(self.taxonomy_specie,'SPECIES')
        elif self.taxonomy_genus:
            return "{} ({})".format(self.taxonomy_genus,'GENUS')
        elif self.taxonomy_family:
            return "{} ({})".format(self.taxonomy_family,'FAMILY')
        elif self.taxonomy_ordo:
            return "{} ({})".format(self.taxonomy_ordo,'ORDER')
        elif self.taxonomy_class:
            return "{} ({})".format(self.taxonomy_class,'CLASS')
        elif self.taxonomy_phylum:
            return "{} ({})".format(self.taxonomy_phylum,'PHYLUM')
        else:
            return "{} ({})".format(self.taxonomy_kingdom,'KINGDOM')

class TaxonomyNormalized(models.Model):
    """
    Tassonomia semplice relativa alle osservazioni normalizzata con solo nomi e ranghi
    """
    id = models.IntegerField("ID", primary_key=True)
    canonical_name = ShortTextField("Nome scientifico semplice")
    scientific_name = ShortTextField("Nome scientifico")    
    rank = ShortTextField("Rango")
    parent = models.ForeignKey(Name,on_delete=models.DO_NOTHING,verbose_name="Parente",null=True,blank=True,db_column='parent_fk',related_name='taxonomynormalized_parent')
    name = models.ForeignKey(Name,on_delete=models.DO_NOTHING,verbose_name="Parente",null=True,blank=True,db_column='name_id',related_name='taxonomynormalized_name')
    usage = models.ForeignKey(NameUsage,on_delete=models.DO_NOTHING,verbose_name="Utilizzo del nome",db_column='usage_id')

    class Meta:
        managed = False
        db_table = 'taxon"."observation_all_taxonomies'
        verbose_name = 'Tassonomia semplice di una osservazione'
        verbose_name_plural = 'Tassonomie semplici delle osservazioni'
        unique_together = ('canonical_name', 'rank',)
        ordering = ['rank','canonical_name']

    def __str__(self):
        return "%s (%s)".format(self.canonical_name, self.rank)
    


class TaxonProtectionManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(name__observation__isnull=False).distinct()

class TaxonProtection(models.Model):
    taxon_protection = models.AutoField("ID",primary_key=True,db_column="taxon_protection_id")
    name = models.OneToOneField(Name,on_delete=models.CASCADE,verbose_name="Nome tassonomico",db_column="name_id")
    taxonomy_specie = models.TextField(verbose_name="Specie")
    #taxonomy_specie_vernacular = models.TextField("Nome vernacolare",blank=True, null=True)
    l_157_92_art_2 = models.BooleanField("157/92 art. 2",blank=True, null=True)
    l_157_92 = models.BooleanField("157/92",blank=True, null=True)
    cee_79_409_ap_1 = models.BooleanField("CEE 79/409 1",blank=True, null=True)
    cee_79_409_ap_2_i = models.BooleanField("CEE 79/409 2i",blank=True, null=True)
    cee_79_409_ap_2_ii = models.BooleanField("CEE 79/409 2ii",blank=True, null=True)
    cee_79_409_ap_3_i = models.BooleanField("CEE 79/409 3i",blank=True, null=True)
    cee_79_409_ap_3_ii = models.BooleanField("CEE 79/409 3ii",blank=True, null=True)
    berna_all_1 = models.BooleanField("Conv. BERNA, all.1",blank=True, null=True)
    berna_all_2 = models.BooleanField("Conv. BERNA, all.2",blank=True, null=True)
    berna_all_3 = models.BooleanField("Conv. BERNA, all.3",blank=True, null=True)
    cites_all_a = models.BooleanField("CITES, all. a",blank=True, null=True)
    cites_all_b = models.BooleanField("CITES, all. b",blank=True, null=True)
    cites_all_d = models.BooleanField("CITES, all. d",blank=True, null=True)
    bonn_ap_1 = models.BooleanField("Conv. BONN, all. 1",blank=True, null=True)
    bonn_ap_2 = models.BooleanField("Conv. BONN, all. 2",blank=True, null=True)
    habitat_ap_2 = models.BooleanField("Habitat, all. 2",blank=True, null=True)
    habitat_ap_4 = models.BooleanField("Habitat, all. 4",blank=True, null=True)
    habitat_ap_5 = models.BooleanField("Habitat, all. 5",blank=True, null=True)
    habitat_ap_2_prior = models.BooleanField("Habitat, all. 2*",blank=True, null=True)
    barcellona_all_2 = models.BooleanField("Conv. BARCELLONA, all.2",blank=True, null=True)
    endemism = models.BooleanField("Endemismo?",blank=True, null=True)
    iucn = models.ForeignKey(LuIUCNCategory,models.SET_NULL,verbose_name="IUCN",blank=True, null=True,db_column='iucn')
    iucn_detail = models.TextField("IUCN, criteri",blank=True, null=True)
    specie_sensitivity = models.IntegerField("Sensibilità (km della griglia",blank=True,null=True)

    objects = TaxonProtectionManager()

    class Meta:
        managed = False
        db_table = 'taxon_protection'
        verbose_name = 'Ambiti di protezione della specie (MITE-PAT)'
        verbose_name_plural = 'Ambiti di protezione delle specie (MITE-PAT)'
        ordering = ['name']
        
    def __str__(self):
            return "{}".format(self.name)


class TaxonProtectionNormalized(models.Model):
    """
    Dettagli riguardo lo status di protezione di una certa specie
    """
    name = models.ForeignKey(Name,models.DO_NOTHING,null=True,blank=True,verbose_name="Tassonomia")
    taxonomy_specie = models.TextField(verbose_name="Specie", primary_key=True, db_column="taxonomy_specie")
    taxonomy_specie_vernacular = models.TextField("Nome vernacolare",blank=True, null=True)
    uccelli = models.TextField("Direttiva Uccelli",null=True)
    berna = models.TextField("Convenzione di Berna",null=True)
    cites = models.TextField("CITES",null=True)
    bonn = models.TextField("Bonn",null=True)
    habitat = models.TextField("Dir. Habitat",null=True)
    barcellona_all_2 = models.BooleanField("Barcellona",blank=True, null=True)
    endemism = models.BooleanField("Endemismo?",blank=True, null=True)
    iucn = models.TextField("IUCN",blank=True, null=True)
    iucn_detail = models.TextField("IUCN, dettaglio",blank=True, null=True)
    #habitat_legend = models.TextField(blank=True, null=True)
    #specie_iucn = models.TextField(blank=True, null=True)

    class Meta:
        managed = False # Created from a view. Don't remove
        db_table = 'taxon_protection_normalized'
        verbose_name = 'Ambiti di protezione di una specie'
        verbose_name_plural = 'Ambiti di protezione delle specie'

    def __str__(self):
            return "{}".format(self.name)  


class TaxonStats(models.Model):
    """
    Statistiche riassuntive relative ad una certa tassonomia
    """
    taxon = models.OneToOneField(
        Name, models.DO_NOTHING, verbose_name="Tassonomia", primary_key=True, db_column="name_id")
    #rank = ShortTextField("Rango tassonomico")
    canonical_name = ShortTextField("Nome canonico")
    observations_count = models.IntegerField("Conteggio osservazioni")
    origin = models.TextField("Nomi dei dataset che contengono il taxon")
    origin_n = models.IntegerField("Numero di dataset che contengono il taxon")
    habitat_natura2000 = models.TextField("Codici Habitat Natura2000 che contengono il taxon")
    habitat_natura2000_n = models.IntegerField("Numero di habitat Natura 2000 che contengono il taxon")
    comuni = models.TextField("Comuni che contengono il taxon")
    comuni_n = models.IntegerField("Numero di comuni che contengono il taxon")
    province = models.TextField("Province che contengono il taxon")
    province_n = models.IntegerField("Numero di province che contengono il taxon")
    regioni = models.TextField("Regioni che contengono il taxon")
    regioni_n = models.IntegerField("Numero di regioni che contengono il taxon")
    aree_protette = models.TextField("Aree protette che contengono il taxon")
    aree_protette_n = models.IntegerField("Numero di aree protette che contengono il taxon")
    month_1_n = models.IntegerField("N. oss. - Gennaio")
    month_2_n = models.IntegerField("N. oss. - Febbraio")
    month_3_n = models.IntegerField("N. oss. - Marzo")
    month_4_n = models.IntegerField("N. oss. - Aprile")
    month_5_n = models.IntegerField("N. oss. - Maggio")
    month_6_n = models.IntegerField("N. oss. - Giugno")
    month_7_n = models.IntegerField("N. oss. - Luglio")
    month_8_n = models.IntegerField("N. oss. - Agosto")
    month_9_n = models.IntegerField("N. oss. - Settembre")
    month_10_n = models.IntegerField("N. oss. - Ottobre")
    month_11_n = models.IntegerField("N. oss. - Novembre")
    month_12_n = models.IntegerField("N. oss. - Dicembre")

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'taxon"."observation_taxon_stats'
        verbose_name = "Statistiche per un taxon"
        verbose_name_plural = "Statistiche per i taxa"
        ordering = ['taxon']

    def __str__(self):
        return "%s (%s, n oss.: %s)".format(self.canonical_name, self.taxon.rank, self.observations_count)

class TaxonLeaves(BaseFunctionTableModel):
    function_args = OrderedDict((
        ('canonical_nam', TableFunctionArg(required=True)),
        ('ran', TableFunctionArg(required=True)),
    ))

    name = models.ForeignKey(Name, on_delete=models.DO_NOTHING,verbose_name='ID nome',related_name='l_name')
    usage = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,verbose_name='ID utilizzo',related_name='l_usafe')
    parent_fk = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,db_column='parent_fk',verbose_name='Genitore',related_name='l_parent')
    rank = models.TextField(verbose_name='Rango')
    canonical_name = models.TextField(verbose_name='Nome canonico')

    # example: TaxonLeaves.objects.all().table_function(canonical_nam='Aves',ran='CLASS')
    class Meta:
        db_table = 'get_taxonomy_leaves'
        managed = False

    def __str__(self):
        return '{} ({})'.format(self.canonical_name,self.rank)

class TaxonLeavesAll(BaseFunctionTableModel):
    """
    Versione senza bisogno di indicare il rango
    della tassonomia
    """
    function_args = OrderedDict((
        ('canonical_nam', TableFunctionArg(required=True)),
    ))

    name = models.ForeignKey(Name, on_delete=models.DO_NOTHING,verbose_name='ID nome',related_name='l_a_name')
    usage = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,verbose_name='ID utilizzo',related_name='l_a_usage')
    parent_fk = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,db_column='parent_fk',verbose_name='Genitore',related_name='l_a_parent')
    rank = models.TextField(verbose_name='Rango')
    canonical_name = models.TextField(verbose_name='Nome canonico')

    # example: TaxonLeavesAll.objects.all().table_function(canonical_nam='Aves')
    class Meta:
        db_table = 'get_all_taxonomy_leaves'
        managed = False

    def __str__(self):
        return '{} ({})'.format(self.canonical_name,self.rank)


class TaxonRoots(BaseFunctionTableModel):
    function_args = OrderedDict((
        ('canonical_nam', TableFunctionArg(required=True)),
        ('ran', TableFunctionArg(required=True)),
    ))

    name = models.ForeignKey(Name, on_delete=models.DO_NOTHING,verbose_name='ID nome',related_name='p_name')
    usage = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,verbose_name='ID utilizzo',related_name='p_usafe')
    parent_fk = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,db_column='parent_fk',verbose_name='Genitore',related_name='p_parent')
    rank = models.TextField(verbose_name='Rango')
    canonical_name = models.TextField(verbose_name='Nome canonico')

    # example: TaxonRoots.objects.all().table_function(canonical_nam='Aves',ran='CLASS')
    class Meta:
        db_table = 'get_taxonomy_roots'
        managed = False

    def __str__(self):
        return '{} ({})'.format(self.canonical_name,self.rank)

class TaxonRootsAll(BaseFunctionTableModel):
    function_args = OrderedDict((
        ('canonical_nam', TableFunctionArg(required=True)),
    ))

    name = models.ForeignKey(Name, on_delete=models.DO_NOTHING,verbose_name='ID nome',related_name='p_all_name')
    usage = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,verbose_name='ID utilizzo',related_name='p_all_usage')
    parent_fk = models.ForeignKey(NameUsage, on_delete=models.DO_NOTHING,db_column='parent_fk',verbose_name='Genitore',related_name='p_all_parent')
    rank = models.TextField(verbose_name='Rango')
    canonical_name = models.TextField(verbose_name='Nome canonico')

    # example: TaxonRootsAll.objects.all().table_function(canonical_nam=['Aves','Salamandra salamandra'])
    class Meta:
        db_table = 'get_all_taxonomy_roots'
        managed = False

    def __str__(self):
        return '{} ({})'.format(self.canonical_name,self.rank)

class ObservationsTaxonomyNonmatches(models.Model):
    id= models.AutoField("ID",primary_key=True)
    taxon_cleaned = models.TextField("Tassonomia originale o pulita",blank=True, null=True)
    canonical_name = models.TextField(blank=True, null=True)
    scientific_name = models.TextField(blank=True, null=True)
    canonical_id = models.IntegerField(blank=True, null=True)
    scientific_id = models.IntegerField(blank=True, null=True)
    n = models.BigIntegerField("Numero di osservazioni problematiche",blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'observations_taxonomy_nonmatches'