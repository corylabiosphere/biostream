from django.apps import AppConfig


class TaxonConfig(AppConfig):
    name = 'taxon'
    verbose_name = 'Gestore della tassonomia'
