from django.shortcuts import get_object_or_404, render
from taxon.models import Name,TaxonStats, TaxonomyNormalized
from dal import autocomplete
from django.db.models import F,Q
from django.http import JsonResponse
import json
# from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.http import HttpRequest, JsonResponse
from biodiv.models import Observation
from .functions import get_accepted_taxon
app_name = __package__


def taxonstats(request, taxon, rank):
    """
    Ritorna le statistiche riguardo una certa tassonomia, create 
    attraverso una vista materializzata su PostgreSQL
    """
    taxon = Name.objects.filter(canonical_name=taxon,rank=rank).values_list('id')[0]
    tax = get_object_or_404(TaxonStats, taxon=taxon)
    return render(request, 'taxon_stats.html', {'taxon': tax, 'taxonname': taxon})


class TaxonObservationAutocomplete(autocomplete.Select2QuerySetView):
    """
    OLD, DYNAMIC VERSION
    class TaxonObservationAutocomplete(autocomplete.Select2QuerySetView):
        
        def get_queryset(self):
            tax = Observation.objects.values_list('taxon__canonical_name',flat=True)
            #rr = self.forwarded.get('rr', None)

            if 'rr' in self.request.GET:
                tax = tax.filter(rr=self.request.GET.get('rr'))
            if 'areaprotetta' in self.request.GET:
                tax = tax.filter(areaprotetta=self.request.GET.get('areaprotetta'))
            if 'parco' in self.request.GET:
                tax = tax.filter(parco=self.request.GET.get('parco'))
            if 'comune' in self.request.GET:
                tax = tax.filter(comune=self.request.GET.get('comune'))

            qs = TaxonRootsAll.objects.all().table_function(canonical_nam=list(tax.distinct()))
            if self.q:
                qs = qs.filter(canonical_name__icontains=self.q)
            return qs.order_by('rank','canonical_name').annotate(sensitivity=F('name_id__taxonprotection__specie_sensitivity'))#.values('pk','canonical_name','rank','sensitivity')

        def get_results(self, context):
            return [
                {
                    'id': self.get_result_value(result),
                    'text': self.get_result_label(result),
                    'specie': str(result.canonical_name),
                    'rank': str(result.rank),
                    'sensitivity': str(result.sensitivity),
                    'selected_text': self.get_selected_result_label(result),
                } for result in context['object_list']
            ]    
    """    
    def get_queryset(self):
        qs = TaxonomyNormalized.objects.all()

        if self.q:
            qs = qs.filter(canonical_name__icontains=self.q)
        return qs.order_by('rank','canonical_name').annotate(sensitivity=F('name_id__taxonprotection__specie_sensitivity'))
    def get_results(self, context):
        return [
            {
                'id': self.get_result_value(result),
                'text': self.get_result_label(result),
                'specie': str(result.canonical_name),
                'rank': str(result.rank),
                'sensitivity': str(result.sensitivity),
                'selected_text': self.get_selected_result_label(result),
            } for result in context['object_list']
        ]

def get_accepted_taxon(request,taxon):    
    accepted_taxon = get_accepted_taxon(taxon)
    return JsonResponse({"taxon": accepted_taxon["id"],"taxon_scientific": accepted_taxon["scientific_name"],"taxon_vernacular": accepted_taxon["vernacular"]}, safe=False)     


def get_taxon(request):
    """
    Funzione per la ricerca di tassonomia in base ad una stringa "q" di input.
    Ritorna una stringa JSON compatibile con il tool JS Select2 
    """
    menu = {"results": []}
    habitat = request.GET.get('habitat')

    if request.GET.get('q'):
        q = request.GET['q']
        results = TaxonomyNormalized.objects.filter(canonical_name__icontains=q).order_by('canonical_name').values(
            'id', text=F('canonical_name'))
    else:
        results = TaxonomyNormalized.objects.all().order_by('canonical_name').values(
            'id', text=F('canonical_name'))

    if habitat:
        if habitat=='true':
            results = results.filter(Q(name__taxonprotectionnormalized__habitat__isnull=False))

    ranks = list(results.values_list('rank').distinct().order_by('-rank'))
    for rank in ranks:
        rankres = results.filter(rank=list(rank)[0])
        if not rankres.exists():
            continue

        menu["results"].append({
            "text": list(rank)[0],
            "children": list(
                rankres.order_by('canonical_name').values('id', 'text', rank=F('rank'))
            )
        })

    return JsonResponse(menu, safe=False)


class GBIFTaxonAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Name.objects.all()
        if self.q:
            #qs = qs.filter(scientific_name__search=self.q)
            qs = qs.filter(scientific_name__icontains=self.q)
        return qs

    def get_results(self, context):
        return [
            {
                'id': self.get_result_value(result),
                'text': str(result.scientific_name),
                'selected_text': str(result.scientific_name),
            } for result in context['object_list']
        ]

def correct_taxon_from_observation(request: HttpRequest) -> JsonResponse:
    if request.method == 'POST':
        request_data = json.loads(request.body)
        taxon_tocorrect = request_data['unMatchedTaxon']
        taxon_corrected = request_data['matchedTaxon']

        Observation.objects.filter(taxon_original__exact=taxon_tocorrect).update(taxon=taxon_corrected)

        return JsonResponse({'message': "Tassonomia corretta!"}, status=200)
    return JsonResponse({'message': "Errore nella correzione! Contatta l'amministratore di sistema"}, status=500)