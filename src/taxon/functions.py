from .models import Name

def get_accepted_taxon(taxon,char=False):
    accepted_taxon = taxon
    if char:
        taxonomy = Name.objects.filter(canonical_name=accepted_taxon)
    else:
        taxonomy = Name.objects.filter(id=accepted_taxon)
    synonym = taxonomy.values_list('nameusage__is_synonym',flat=True)[0]
    if synonym:
        rank = taxonomy.values_list('rank',flat=True)[0].lower()
        if rank=='subspecies':
            rank = 'species'
        col = 'nameusage__' + rank + '_fk__name__id'
        accepted_taxon = Name.objects.filter(id=taxonomy.values_list(col,flat=True)[0]).values('id','scientific_name','canonical_name')[0]
    else:
        accepted_taxon = taxonomy.values('id','scientific_name','canonical_name')[0]
    return accepted_taxon