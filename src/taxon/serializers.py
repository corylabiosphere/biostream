from .models import (
    Taxonomy, TaxonomyNormalized, TaxonProtectionNormalized, TaxonStats, ObservationsTaxonomyNonmatches
)
from rest_framework.serializers import ModelSerializer
from biostream.serializers import DynamicFieldsMixin

class TaxonomyNonmatchesSerializer(ModelSerializer):
    class Meta:
        model = ObservationsTaxonomyNonmatches
        fields = '__all__'

class TaxonomySerializer(ModelSerializer):
    class Meta:
        model = Taxonomy
        fields = '__all__'
        # fields = ('observation_specie', 'taxonomy_specie', 'taxonomy_family', 'taxonomy_ordo', 'taxonomy_class',
        #          'taxonomy_phylum', 'taxonomy_kingdom', 'taxonomy', 'is_synonym_by_taxonomy', 'status_by_taxonomy')

class TaxonomyNormalizedSerializer(ModelSerializer):
    class Meta:
        model = TaxonomyNormalized
        fields = '__all__'


class TaxonProtectionNormalizedSerializer(ModelSerializer):
    #taxonomy_specie = serializers.CharField(read_only=True, source="taxonomy_specie.taxonomy_specie")
    class Meta:
        model = TaxonProtectionNormalized
    #    fields = ['taxonomy_specie','taxonomy_specie_vernacular','taxonomy_family','taxonomy_ordo',
    #    'taxonomy_phylum','taxonomy_kingdom','uccelli','berna','cites','bonn','habitat','barcellona_all_2','endemism','iucn','iucn_detail']
        fields = '__all__'

class TaxonStatsSerializerFull(DynamicFieldsMixin, ModelSerializer):
    class Meta:
        model = TaxonStats
        id_field = 'taxon'
        fields = ('taxon', 'canonical_name','observations_count', 'origin', 'origin_n', 'habitat_natura2000', 'habitat_natura2000_n', 'comuni', 'comuni_n', 'province', 'province_n', 'regioni', 'regioni_n', 'aree_protette',
                  'aree_protette_n', 'month_1_n', 'month_2_n', 'month_3_n', 'month_4_n', 'month_5_n', 'month_6_n', 'month_7_n', 'month_8_n', 'month_9_n', 'month_10_n', 'month_11_n', 'month_12_n')


class TaxonStatsSerializerBase(DynamicFieldsMixin, ModelSerializer):
    class Meta:
        model = TaxonStats
        id_field = 'taxon'
        fields = ('taxon', 'observations_count',)