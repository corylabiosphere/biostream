

# BioSTREAM

<img src="/media_cdn/loghi/banner_biostream.png" width=50% alt="Barra loghi BioSTREAM>

L'app BioSTREAM è prevalentemente un _WebGIS_ con funzione di consultazione, caricamento e filtraggio utile all'esplorazione della biodiversità dell'areale del Progetto BioSTREAM. È costruita con [Python][0] per il tramite del framework web [Django][1] e con varie altre applicazioni open source elencate in calce.

Il progetto è costituito dalle seguenti applicazioni di base:

* biodiv (per i dati di biodiversità)
* data_requests (per le richieste di dati)
* map (per la visualizzazione)
* profiles (per la gestione dei profili utenti)

## Installazione

Per settare un ambiente di sviluppo in grado di replicare localmente l'applicazione, + 
prima di tutto necessario installare Python. Quindi, utilizzando `virtualenv`, è necessario 
creare un ambiente virtuale entro il quale installare i pacchetti necessari:

    1. python3 -m venv biostream
    2. . biostream/bin/activate

Installare quindi le dipendenze:

    pip install -r requirements.txt

È quindi necessario avere a disposizione un database `PostgreSQL` + `PostGIS` (spazialmente abilitato),
preferibilmente anche con il _wrapper_ `plr` installato. Un setup di base utile a settarne uno rapidamente
è (_Ubuntu 18.04_):

    1. sudo apt install postgresql-12-postgis postgresql-12-plr
    2. sudo su postgres
    3. psql
    4. CREATE USER bioadmin WITH LOGIN SUPERUSER PASSWORD 'miapassworddb';
    5. CREATE DATABASE biostream WITH OWNER bioadmin;$
    6. \connect biostream
    7. CREATE EXTENSION postgis; CREATE EXTENSION plr;

Di conseguenza è necessario creare nel proprio ambiente Python un file `local.env` (cartella _biostream/settings_)
entro il quale settare le variabili utili ala connessione al database:

    DATABASE_NAME="biostream"
    DATABASE_USER="biostreamadmin"
    DATABASE_PW="miapassworddb"
    DATABASE_PORT=5432 # cambiare alla bisogna
    DATABASE_HOST="127.0.0.1" # oppure, localhost

Fatto questo, per ottenere la struttura del database (funzionante ma priva di dati a causa dell'esigenza di protezione di dati personali e della sensibilità di molte specie le cui localizzazioni sarebbero altrimenti esplicite), si deve ripristinare il backup di base distribuito con questa applicazione:

    pg_restore -p 5432 -U bioadmin -d biostream STRUTTURA_DB_BIOSTREAM.dump

E quindi, sincronizzare la app e creare un utente admin (utile ad accedere all'interfaccia di amministrazione dell'app):

    ./manage.py makemigrations --fake
    ./manage.py collectstatic
    ./manage.py createsuperuser

... E far partire il server di sviluppo:

    ./manage.py runserver



