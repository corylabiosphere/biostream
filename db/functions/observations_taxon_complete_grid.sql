-- View: data_biodiv.observation_grid_taxon_complete


CREATE MATERIALIZED VIEW data_biodiv.observation_grid_taxon_complete
TABLESPACE pg_default
AS
 WITH main AS (
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'SPECIE'::text AS taxon_rank,
            b.taxonomy_specie AS taxon,
            count(o.specie) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.specie, alpsgrid.res, alpsgrid.geom, b.taxonomy_specie
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'GENUS'::text AS taxon_rank,
            b.taxonomy_genus AS taxon,
            count(o.specie) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.specie, alpsgrid.res, alpsgrid.geom, b.taxonomy_genus
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'FAMILY'::text AS taxon_rank,
            b.taxonomy_family AS taxon,
            count(o.specie) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.specie, alpsgrid.res, alpsgrid.geom, b.taxonomy_family
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'ORDO'::text AS taxon_rank,
            b.taxonomy_ordo AS taxon,
            count(o.specie) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.specie, alpsgrid.res, alpsgrid.geom, b.taxonomy_ordo
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'CLASS'::text AS taxon_rank,
            b.taxonomy_class AS taxon,
            count(o.specie) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.specie, alpsgrid.res, alpsgrid.geom, b.taxonomy_class
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'PHYLUM'::text AS taxon_rank,
            b.taxonomy_phylum AS taxon,
            count(o.specie) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.specie, alpsgrid.res, alpsgrid.geom, b.taxonomy_phylum
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'KINGDOM'::text AS taxon_rank,
            b.taxonomy_kingdom AS taxon,
            count(o.specie) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.specie, alpsgrid.res, alpsgrid.geom, b.taxonomy_kingdom
        )
 SELECT row_number() OVER () AS id,
    main.grid_id,
    main.res,
    main.geom,
    main.taxon_rank,
    main.taxon,
    main.obs_count
   FROM main
  WHERE main.taxon IS NOT NULL
WITH DATA;

ALTER TABLE data_biodiv.observation_grid_taxon_complete
    OWNER TO biostreamadmin;


CREATE INDEX observations_grid_taxon_complete_geom_idx
    ON data_biodiv.observation_grid_taxon_complete USING gist
    (geom)
    TABLESPACE pg_default;