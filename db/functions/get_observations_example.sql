SELECT observations.observation_id,observations.specie,observations.recorder,observations.geom,st_x(st_transform(observations.geom,4326)) AS x,st_y(st_transform(observations.geom,4326)) AS y,observations.date_complete,dataorigins.origin_name FROM data_biodiv.observations
LEFT JOIN dataorigins ON (dataorigins.dataorigin_id=observations.dataorigin_id)
LEFT JOIN taxon.taxonomy_complete ON (observations.specie=taxonomy_complete.observation_specie)
LEFT JOIN data_adm.it_regioni ON st_contains(it_regioni.geom,observations.geom)
WHERE taxonomy_class='Amphibia'
AND it_regioni.den_reg='Friuli Venezia Giulia';


psql $BIOSTREAM -c "\copy (SELECT observations.observation_id,observations.specie,observations.recorder,st_x(st_transform(observations.geom,4326)) AS x,st_y(st_transform(observations.geom,4326)) AS y,observations.date_complete,dataorigins.origin_name FROM data_biodiv.observations LEFT JOIN dataorigins ON (dataorigins.dataorigin_id=observations.dataorigin_id) LEFT JOIN taxon.taxonomy_complete ON (observations.specie=taxonomy_complete.observation_specie) LEFT JOIN data_adm.it_regioni ON st_contains(it_regioni.geom,observations.geom) WHERE taxonomy_class='Amphibia' AND it_regioni.den_reg='Friuli Venezia Giulia') TO '~/anfibi_friuli.csv' csv header;"