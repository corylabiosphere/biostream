CREATE MATERIALIZED VIEW data_biodiv.inat_grid AS
SELECT DISTINCT it_grid.id AS id,it_grid.res AS res,it_grid.geom AS geom,COUNT(inat.geom) AS count,inat.specie AS specie
FROM data_adm.it_grid LEFT JOIN data_biodiv.inat
ON ST_INTERSECTS(it_grid.geom,inat.geom)
WHERE ST_INTERSECTS(it_grid.geom,inat.geom) AND res = ANY(ARRAY[10,100])
GROUP BY it_grid.id,inat.specie,it_grid.res,it_grid.geom ;

CREATE INDEX inat_grid_geom_index
  ON data_biodiv.inat_grid
  USING GIST (geom);
 VACUUM ANALYZE data_biodiv.inat_grid;
