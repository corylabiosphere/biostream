CREATE MATERIALIZED VIEW IF NOT EXISTS data_biodiv.observation_grid_fine
TABLESPACE pg_default
AS
 SELECT DISTINCT alps_convention_extended_grid.id,
    alps_convention_extended_grid.res,
    alps_convention_extended_grid.geom,
    observations.taxon_original AS specie,
    sum(observations.count_min) AS count_min,
    sum(observations.count_max) AS count_max,
    array_to_string(array_agg(DISTINCT observations.recorder), ', '::text) AS recorder,
    array_to_string(array_agg(DISTINCT observations.baseoss_id), ', '::text) AS baseoss,
    array_to_string(array_agg(DISTINCT dataorigins.origin_which), ', '::text) AS dataorigins,
    array_to_string(array_agg(DISTINCT institutions.institution_name), ', '::text) AS institutions,
    array_to_string(array_agg(DISTINCT observations.sampling_method_id), ', '::text) AS sampling_methods
   FROM alps_convention_extended_grid
     LEFT JOIN observations ON st_intersects(alps_convention_extended_grid.geom, observations.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = observations.dataorigin_id
     LEFT JOIN institutions ON dataorigins.institution_id = institutions.institution_id
  WHERE st_intersects(alps_convention_extended_grid.geom, observations.geom) AND (alps_convention_extended_grid.res = ANY (ARRAY[1::double precision, 5::double precision, 10::double precision, 25::double precision, 50::double precision, 100::double precision]))
  GROUP BY alps_convention_extended_grid.id, observations.taxon_original, alps_convention_extended_grid.res, alps_convention_extended_grid.geom
WITH DATA;

ALTER TABLE IF EXISTS data_biodiv.observation_grid_fine
    OWNER TO biostreamadmin;


CREATE INDEX observations_grid_fine_geom_index
    ON data_biodiv.observation_grid_fine USING gist
    (geom)
    TABLESPACE pg_default;

-- View: data_biodiv.observation_grid_taxon

-- DROP MATERIALIZED VIEW IF EXISTS data_biodiv.observation_grid_taxon;

CREATE MATERIALIZED VIEW IF NOT EXISTS data_biodiv.observation_grid_taxon
TABLESPACE pg_default
AS
 WITH main AS (
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'SPECIE'::text AS taxon_rank,
            b.taxonomy_specie AS taxon,
            count(o.taxon_original) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN observations_backbone b ON b.taxonomy_specie_id = o.taxon_id
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.taxon_original, alpsgrid.res, alpsgrid.geom, b.taxonomy_specie
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'GENERE'::text AS taxon_rank,
            b.taxonomy_genus AS taxon,
            count(o.taxon_original) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN observations_backbone b ON b.taxonomy_specie_id = o.taxon_id
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.taxon_original, alpsgrid.res, alpsgrid.geom, b.taxonomy_genus
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'FAMIGLIA'::text AS taxon_rank,
            b.taxonomy_family AS taxon,
            count(o.taxon_original) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN observations_backbone b ON b.taxonomy_specie_id = o.taxon_id
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.taxon_original, alpsgrid.res, alpsgrid.geom, b.taxonomy_family
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'ORDINE'::text AS taxon_rank,
            b.taxonomy_ordo AS taxon,
            count(o.taxon_original) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN observations_backbone b ON b.taxonomy_specie_id = o.taxon_id
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.taxon_original, alpsgrid.res, alpsgrid.geom, b.taxonomy_ordo
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'CLASSE'::text AS taxon_rank,
            b.taxonomy_class AS taxon,
            count(o.taxon_original) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN observations_backbone b ON b.taxonomy_specie_id = o.taxon_id
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.taxon_original, alpsgrid.res, alpsgrid.geom, b.taxonomy_class
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'PHYLUM'::text AS taxon_rank,
            b.taxonomy_phylum AS taxon,
            count(o.taxon_original) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN observations_backbone b ON b.taxonomy_specie_id = o.taxon_id
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.taxon_original, alpsgrid.res, alpsgrid.geom, b.taxonomy_phylum
        UNION ALL
         SELECT alpsgrid.id AS grid_id,
            alpsgrid.res,
            alpsgrid.geom,
            'REGNO'::text AS taxon_rank,
            b.taxonomy_kingdom AS taxon,
            count(o.taxon_original) AS obs_count
           FROM alps_convention_extended_grid alpsgrid
             LEFT JOIN observations o ON st_intersects(alpsgrid.geom, o.geom)
             LEFT JOIN observations_backbone b ON b.taxonomy_specie_id = o.taxon_id
          WHERE st_intersects(alpsgrid.geom, o.geom)
          GROUP BY alpsgrid.id, o.taxon_original, alpsgrid.res, alpsgrid.geom, b.taxonomy_kingdom
        )
 SELECT row_number() OVER () AS id,
    main.grid_id,
    main.res,
    main.geom,
    main.taxon_rank,
    main.taxon,
    main.obs_count
   FROM main
  WHERE main.taxon IS NOT NULL
WITH DATA;

ALTER TABLE IF EXISTS data_biodiv.observation_grid_taxon
    OWNER TO biostreamadmin;


CREATE INDEX observations_grid_taxon_geom_idx
    ON data_biodiv.observation_grid_taxon USING gist
    (geom)
    TABLESPACE pg_default;

-- View: data_biodiv.observation_grid_ultrafine

-- DROP MATERIALIZED VIEW IF EXISTS data_biodiv.observation_grid_ultrafine;

CREATE MATERIALIZED VIEW IF NOT EXISTS data_biodiv.observation_grid_ultrafine
TABLESPACE pg_default
AS
 SELECT DISTINCT alps_convention_extended_grid.id,
    alps_convention_extended_grid.res,
    alps_convention_extended_grid.geom,
    observations.taxon_original AS specie,
    observations.date_month,
    observations.date_year,
    sum(observations.count_min) AS count_min,
    sum(observations.count_max) AS count_max,
    array_to_string(array_agg(DISTINCT observations.recorder), ', '::text) AS recorder,
    array_to_string(array_agg(DISTINCT observations.baseoss_id), ', '::text) AS baseoss,
    array_to_string(array_agg(DISTINCT dataorigins.origin_which), ', '::text) AS dataorigins,
    array_to_string(array_agg(DISTINCT institutions.institution_name), ', '::text) AS institutions,
    array_to_string(array_agg(DISTINCT observations.sampling_method_id), ', '::text) AS sampling_methods
   FROM alps_convention_extended_grid
     LEFT JOIN observations ON st_intersects(alps_convention_extended_grid.geom, observations.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = observations.dataorigin_id
     LEFT JOIN institutions ON dataorigins.institution_id = institutions.institution_id
  WHERE st_intersects(alps_convention_extended_grid.geom, observations.geom) AND (alps_convention_extended_grid.res = ANY (ARRAY[1::double precision, 5::double precision, 10::double precision, 25::double precision, 50::double precision, 100::double precision]))
  GROUP BY alps_convention_extended_grid.id, observations.taxon_original, alps_convention_extended_grid.res, alps_convention_extended_grid.geom, observations.date_month, observations.date_year
WITH DATA;

ALTER TABLE IF EXISTS data_biodiv.observation_grid_ultrafine
    OWNER TO biostreamadmin;


CREATE INDEX observations_grid_ultrafine_geom_index
    ON data_biodiv.observation_grid_ultrafine USING gist
    (geom)
    TABLESPACE pg_default;

-- View: data_biodiv.areeprotette_checklist

-- DROP MATERIALIZED VIEW IF EXISTS data_biodiv.areeprotette_checklist;

CREATE MATERIALIZED VIEW IF NOT EXISTS data_biodiv.areeprotette_checklist
TABLESPACE pg_default
AS
 SELECT it_sic_zps.code,
    it_sic_zps.name,
    it_sic_zps.site_type,
    it_sic_zps.biogeographic_region,
    it_sic_zps.administrative_region,
    it_sic_zps.sic_zsc,
    it_sic_zps.zps,
    it_sic_zps.geom,
    array_to_string(array_agg(DISTINCT name.canonical_name), ', '::text) AS specie,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod), ', '::text) AS habitat,
    array_length(array_agg(DISTINCT observations.taxon_original), 1) AS specie_num,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_num
   FROM observations
     LEFT JOIN it_sic_zps ON st_intersects(observations.geom, it_sic_zps.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(observations.geom, habitat_natura2000.geom)
     LEFT JOIN name ON observations.taxon_id = name.id
  WHERE name.rank = 'SPECIES'::taxon.rank
  GROUP BY it_sic_zps.code, it_sic_zps.name, it_sic_zps.site_type, it_sic_zps.biogeographic_region, it_sic_zps.administrative_region, it_sic_zps.sic_zsc, it_sic_zps.zps, it_sic_zps.geom
  ORDER BY it_sic_zps.name
WITH DATA;

ALTER TABLE IF EXISTS data_biodiv.areeprotette_checklist
    OWNER TO biostreamadmin;
