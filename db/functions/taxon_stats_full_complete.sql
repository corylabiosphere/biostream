-- View: data_biodiv.taxon_stats_complete_full

--DROP MATERIALIZED VIEW data_biodiv.taxon_stats_complete_full;

CREATE MATERIALIZED VIEW data_biodiv.taxon_stats_complete_full
TABLESPACE pg_default
AS
 SELECT 'SPECIE'::text AS taxon_rank,
    b.observation_specie AS taxon,
    count(o.specie) AS observations_count,
    array_to_string(array_agg(DISTINCT dataorigins.origin_name), ','::text) AS origin,
    array_length(array_agg(DISTINCT dataorigins.origin_name), 1) AS origin_n,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod ORDER BY habitat_natura2000.cod), ','::text) AS habitat_natura2000,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_natura2000_n,
    array_to_string(array_agg(DISTINCT it_comuni.com_name ORDER BY it_comuni.com_name), ','::text) AS comuni,
    array_length(array_agg(DISTINCT it_comuni.com_name), 1) AS comuni_n,
    array_to_string(array_agg(DISTINCT it_province.den_prov ORDER BY it_province.den_prov), ','::text) AS province,
    array_length(array_agg(DISTINCT it_province.den_prov), 1) AS province_n,
    array_to_string(array_agg(DISTINCT it_regioni.den_reg ORDER BY it_regioni.den_reg), ','::text) AS regioni,
    array_length(array_agg(DISTINCT it_regioni.den_reg), 1) AS regioni_n,
    array_to_string(array_agg(DISTINCT it_sic_zps.code ORDER BY it_sic_zps.code), ','::text) AS sic_zps,
    array_length(array_agg(DISTINCT it_sic_zps.code), 1) AS sic_zps_n,
    count(o.*) FILTER (WHERE o.date_month = 1) AS month_1_n,
    count(o.*) FILTER (WHERE o.date_month = 2) AS month_2_n,
    count(o.*) FILTER (WHERE o.date_month = 3) AS month_3_n,
    count(o.*) FILTER (WHERE o.date_month = 4) AS month_4_n,
    count(o.*) FILTER (WHERE o.date_month = 5) AS month_5_n,
    count(o.*) FILTER (WHERE o.date_month = 6) AS month_6_n,
    count(o.*) FILTER (WHERE o.date_month = 7) AS month_7_n,
    count(o.*) FILTER (WHERE o.date_month = 8) AS month_8_n,
    count(o.*) FILTER (WHERE o.date_month = 9) AS month_9_n,
    count(o.*) FILTER (WHERE o.date_month = 10) AS month_10_n,
    count(o.*) FILTER (WHERE o.date_month = 11) AS month_11_n,
    count(o.*) FILTER (WHERE o.date_month = 12) AS month_12_n
   FROM observations o
     LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
     LEFT JOIN it_comuni ON st_intersects(it_comuni.geom, o.geom)
     LEFT JOIN it_province ON st_intersects(it_province.geom, o.geom)
     LEFT JOIN it_regioni ON st_intersects(it_regioni.geom, o.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(habitat_natura2000.geom, o.geom)
     LEFT JOIN it_sic_zps ON st_intersects(it_sic_zps.geom, o.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = o.dataorigin_id
     LEFT JOIN institutions ON institutions.institution_id = dataorigins.institution_id
  WHERE b.is_synonym_by_taxonomy IS FALSE
  GROUP BY b.observation_specie
UNION ALL
 SELECT DISTINCT ON (b.taxonomy_genus) 'GENUS'::text AS taxon_rank,
    b.taxonomy_genus AS taxon,
    count(o.specie) AS observations_count,
    array_to_string(array_agg(DISTINCT dataorigins.origin_name), ','::text) AS origin,
    array_length(array_agg(DISTINCT dataorigins.origin_name), 1) AS origin_n,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod ORDER BY habitat_natura2000.cod), ','::text) AS habitat_natura2000,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_natura2000_n,
    array_to_string(array_agg(DISTINCT it_comuni.com_name ORDER BY it_comuni.com_name), ','::text) AS comuni,
    array_length(array_agg(DISTINCT it_comuni.com_name), 1) AS comuni_n,
    array_to_string(array_agg(DISTINCT it_province.den_prov ORDER BY it_province.den_prov), ','::text) AS province,
    array_length(array_agg(DISTINCT it_province.den_prov), 1) AS province_n,
    array_to_string(array_agg(DISTINCT it_regioni.den_reg ORDER BY it_regioni.den_reg), ','::text) AS regioni,
    array_length(array_agg(DISTINCT it_regioni.den_reg), 1) AS regioni_n,
    array_to_string(array_agg(DISTINCT it_sic_zps.code ORDER BY it_sic_zps.code), ','::text) AS sic_zps,
    array_length(array_agg(DISTINCT it_sic_zps.code), 1) AS sic_zps_n,
    count(o.*) FILTER (WHERE o.date_month = 1) AS month_1_n,
    count(o.*) FILTER (WHERE o.date_month = 2) AS month_2_n,
    count(o.*) FILTER (WHERE o.date_month = 3) AS month_3_n,
    count(o.*) FILTER (WHERE o.date_month = 4) AS month_4_n,
    count(o.*) FILTER (WHERE o.date_month = 5) AS month_5_n,
    count(o.*) FILTER (WHERE o.date_month = 6) AS month_6_n,
    count(o.*) FILTER (WHERE o.date_month = 7) AS month_7_n,
    count(o.*) FILTER (WHERE o.date_month = 8) AS month_8_n,
    count(o.*) FILTER (WHERE o.date_month = 9) AS month_9_n,
    count(o.*) FILTER (WHERE o.date_month = 10) AS month_10_n,
    count(o.*) FILTER (WHERE o.date_month = 11) AS month_11_n,
    count(o.*) FILTER (WHERE o.date_month = 12) AS month_12_n
   FROM observations o
     LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
     LEFT JOIN it_comuni ON st_intersects(it_comuni.geom, o.geom)
     LEFT JOIN it_province ON st_intersects(it_province.geom, o.geom)
     LEFT JOIN it_regioni ON st_intersects(it_regioni.geom, o.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(habitat_natura2000.geom, o.geom)
     LEFT JOIN it_sic_zps ON st_intersects(it_sic_zps.geom, o.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = o.dataorigin_id
     LEFT JOIN institutions ON institutions.institution_id = dataorigins.institution_id
  WHERE b.is_synonym_by_taxonomy IS FALSE
  GROUP BY b.taxonomy_genus
UNION ALL
 SELECT DISTINCT ON (b.taxonomy_family) 'FAMILY'::text AS taxon_rank,
    b.taxonomy_family AS taxon,
    count(o.specie) AS observations_count,
    array_to_string(array_agg(DISTINCT dataorigins.origin_name), ','::text) AS origin,
    array_length(array_agg(DISTINCT dataorigins.origin_name), 1) AS origin_n,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod ORDER BY habitat_natura2000.cod), ','::text) AS habitat_natura2000,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_natura2000_n,
    array_to_string(array_agg(DISTINCT it_comuni.com_name ORDER BY it_comuni.com_name), ','::text) AS comuni,
    array_length(array_agg(DISTINCT it_comuni.com_name), 1) AS comuni_n,
    array_to_string(array_agg(DISTINCT it_province.den_prov ORDER BY it_province.den_prov), ','::text) AS province,
    array_length(array_agg(DISTINCT it_province.den_prov), 1) AS province_n,
    array_to_string(array_agg(DISTINCT it_regioni.den_reg ORDER BY it_regioni.den_reg), ','::text) AS regioni,
    array_length(array_agg(DISTINCT it_regioni.den_reg), 1) AS regioni_n,
    array_to_string(array_agg(DISTINCT it_sic_zps.code ORDER BY it_sic_zps.code), ','::text) AS sic_zps,
    array_length(array_agg(DISTINCT it_sic_zps.code), 1) AS sic_zps_n,
    count(o.*) FILTER (WHERE o.date_month = 1) AS month_1_n,
    count(o.*) FILTER (WHERE o.date_month = 2) AS month_2_n,
    count(o.*) FILTER (WHERE o.date_month = 3) AS month_3_n,
    count(o.*) FILTER (WHERE o.date_month = 4) AS month_4_n,
    count(o.*) FILTER (WHERE o.date_month = 5) AS month_5_n,
    count(o.*) FILTER (WHERE o.date_month = 6) AS month_6_n,
    count(o.*) FILTER (WHERE o.date_month = 7) AS month_7_n,
    count(o.*) FILTER (WHERE o.date_month = 8) AS month_8_n,
    count(o.*) FILTER (WHERE o.date_month = 9) AS month_9_n,
    count(o.*) FILTER (WHERE o.date_month = 10) AS month_10_n,
    count(o.*) FILTER (WHERE o.date_month = 11) AS month_11_n,
    count(o.*) FILTER (WHERE o.date_month = 12) AS month_12_n
   FROM observations o
     LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
     LEFT JOIN it_comuni ON st_intersects(it_comuni.geom, o.geom)
     LEFT JOIN it_province ON st_intersects(it_province.geom, o.geom)
     LEFT JOIN it_regioni ON st_intersects(it_regioni.geom, o.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(habitat_natura2000.geom, o.geom)
     LEFT JOIN it_sic_zps ON st_intersects(it_sic_zps.geom, o.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = o.dataorigin_id
     LEFT JOIN institutions ON institutions.institution_id = dataorigins.institution_id
  WHERE b.is_synonym_by_taxonomy IS FALSE
  GROUP BY b.taxonomy_family
UNION ALL
 SELECT DISTINCT ON (b.taxonomy_ordo) 'ORDO'::text AS taxon_rank,
    b.taxonomy_ordo AS taxon,
    count(o.specie) AS observations_count,
    array_to_string(array_agg(DISTINCT dataorigins.origin_name), ','::text) AS origin,
    array_length(array_agg(DISTINCT dataorigins.origin_name), 1) AS origin_n,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod ORDER BY habitat_natura2000.cod), ','::text) AS habitat_natura2000,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_natura2000_n,
    array_to_string(array_agg(DISTINCT it_comuni.com_name ORDER BY it_comuni.com_name), ','::text) AS comuni,
    array_length(array_agg(DISTINCT it_comuni.com_name), 1) AS comuni_n,
    array_to_string(array_agg(DISTINCT it_province.den_prov ORDER BY it_province.den_prov), ','::text) AS province,
    array_length(array_agg(DISTINCT it_province.den_prov), 1) AS province_n,
    array_to_string(array_agg(DISTINCT it_regioni.den_reg ORDER BY it_regioni.den_reg), ','::text) AS regioni,
    array_length(array_agg(DISTINCT it_regioni.den_reg), 1) AS regioni_n,
    array_to_string(array_agg(DISTINCT it_sic_zps.code ORDER BY it_sic_zps.code), ','::text) AS sic_zps,
    array_length(array_agg(DISTINCT it_sic_zps.code), 1) AS sic_zps_n,
    count(o.*) FILTER (WHERE o.date_month = 1) AS month_1_n,
    count(o.*) FILTER (WHERE o.date_month = 2) AS month_2_n,
    count(o.*) FILTER (WHERE o.date_month = 3) AS month_3_n,
    count(o.*) FILTER (WHERE o.date_month = 4) AS month_4_n,
    count(o.*) FILTER (WHERE o.date_month = 5) AS month_5_n,
    count(o.*) FILTER (WHERE o.date_month = 6) AS month_6_n,
    count(o.*) FILTER (WHERE o.date_month = 7) AS month_7_n,
    count(o.*) FILTER (WHERE o.date_month = 8) AS month_8_n,
    count(o.*) FILTER (WHERE o.date_month = 9) AS month_9_n,
    count(o.*) FILTER (WHERE o.date_month = 10) AS month_10_n,
    count(o.*) FILTER (WHERE o.date_month = 11) AS month_11_n,
    count(o.*) FILTER (WHERE o.date_month = 12) AS month_12_n
   FROM observations o
     LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
     LEFT JOIN it_comuni ON st_intersects(it_comuni.geom, o.geom)
     LEFT JOIN it_province ON st_intersects(it_province.geom, o.geom)
     LEFT JOIN it_regioni ON st_intersects(it_regioni.geom, o.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(habitat_natura2000.geom, o.geom)
     LEFT JOIN it_sic_zps ON st_intersects(it_sic_zps.geom, o.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = o.dataorigin_id
     LEFT JOIN institutions ON institutions.institution_id = dataorigins.institution_id
  WHERE b.is_synonym_by_taxonomy IS FALSE
  GROUP BY b.taxonomy_ordo
UNION ALL
 SELECT DISTINCT ON (b.taxonomy_class) 'CLASS'::text AS taxon_rank,
    b.taxonomy_class AS taxon,
    count(o.specie) AS observations_count,
    array_to_string(array_agg(DISTINCT dataorigins.origin_name), ','::text) AS origin,
    array_length(array_agg(DISTINCT dataorigins.origin_name), 1) AS origin_n,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod ORDER BY habitat_natura2000.cod), ','::text) AS habitat_natura2000,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_natura2000_n,
    array_to_string(array_agg(DISTINCT it_comuni.com_name ORDER BY it_comuni.com_name), ','::text) AS comuni,
    array_length(array_agg(DISTINCT it_comuni.com_name), 1) AS comuni_n,
    array_to_string(array_agg(DISTINCT it_province.den_prov ORDER BY it_province.den_prov), ','::text) AS province,
    array_length(array_agg(DISTINCT it_province.den_prov), 1) AS province_n,
    array_to_string(array_agg(DISTINCT it_regioni.den_reg ORDER BY it_regioni.den_reg), ','::text) AS regioni,
    array_length(array_agg(DISTINCT it_regioni.den_reg), 1) AS regioni_n,
    array_to_string(array_agg(DISTINCT it_sic_zps.code ORDER BY it_sic_zps.code), ','::text) AS sic_zps,
    array_length(array_agg(DISTINCT it_sic_zps.code), 1) AS sic_zps_n,
    count(o.*) FILTER (WHERE o.date_month = 1) AS month_1_n,
    count(o.*) FILTER (WHERE o.date_month = 2) AS month_2_n,
    count(o.*) FILTER (WHERE o.date_month = 3) AS month_3_n,
    count(o.*) FILTER (WHERE o.date_month = 4) AS month_4_n,
    count(o.*) FILTER (WHERE o.date_month = 5) AS month_5_n,
    count(o.*) FILTER (WHERE o.date_month = 6) AS month_6_n,
    count(o.*) FILTER (WHERE o.date_month = 7) AS month_7_n,
    count(o.*) FILTER (WHERE o.date_month = 8) AS month_8_n,
    count(o.*) FILTER (WHERE o.date_month = 9) AS month_9_n,
    count(o.*) FILTER (WHERE o.date_month = 10) AS month_10_n,
    count(o.*) FILTER (WHERE o.date_month = 11) AS month_11_n,
    count(o.*) FILTER (WHERE o.date_month = 12) AS month_12_n
   FROM observations o
     LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
     LEFT JOIN it_comuni ON st_intersects(it_comuni.geom, o.geom)
     LEFT JOIN it_province ON st_intersects(it_province.geom, o.geom)
     LEFT JOIN it_regioni ON st_intersects(it_regioni.geom, o.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(habitat_natura2000.geom, o.geom)
     LEFT JOIN it_sic_zps ON st_intersects(it_sic_zps.geom, o.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = o.dataorigin_id
     LEFT JOIN institutions ON institutions.institution_id = dataorigins.institution_id
  WHERE b.is_synonym_by_taxonomy IS FALSE
  GROUP BY b.taxonomy_class
UNION ALL
 SELECT DISTINCT ON (b.taxonomy_phylum) 'PHYLUM'::text AS taxon_rank,
    b.taxonomy_phylum AS taxon,
    count(o.specie) AS observations_count,
    array_to_string(array_agg(DISTINCT dataorigins.origin_name), ','::text) AS origin,
    array_length(array_agg(DISTINCT dataorigins.origin_name), 1) AS origin_n,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod ORDER BY habitat_natura2000.cod), ','::text) AS habitat_natura2000,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_natura2000_n,
    array_to_string(array_agg(DISTINCT it_comuni.com_name ORDER BY it_comuni.com_name), ','::text) AS comuni,
    array_length(array_agg(DISTINCT it_comuni.com_name), 1) AS comuni_n,
    array_to_string(array_agg(DISTINCT it_province.den_prov ORDER BY it_province.den_prov), ','::text) AS province,
    array_length(array_agg(DISTINCT it_province.den_prov), 1) AS province_n,
    array_to_string(array_agg(DISTINCT it_regioni.den_reg ORDER BY it_regioni.den_reg), ','::text) AS regioni,
    array_length(array_agg(DISTINCT it_regioni.den_reg), 1) AS regioni_n,
    array_to_string(array_agg(DISTINCT it_sic_zps.code ORDER BY it_sic_zps.code), ','::text) AS sic_zps,
    array_length(array_agg(DISTINCT it_sic_zps.code), 1) AS sic_zps_n,
    count(o.*) FILTER (WHERE o.date_month = 1) AS month_1_n,
    count(o.*) FILTER (WHERE o.date_month = 2) AS month_2_n,
    count(o.*) FILTER (WHERE o.date_month = 3) AS month_3_n,
    count(o.*) FILTER (WHERE o.date_month = 4) AS month_4_n,
    count(o.*) FILTER (WHERE o.date_month = 5) AS month_5_n,
    count(o.*) FILTER (WHERE o.date_month = 6) AS month_6_n,
    count(o.*) FILTER (WHERE o.date_month = 7) AS month_7_n,
    count(o.*) FILTER (WHERE o.date_month = 8) AS month_8_n,
    count(o.*) FILTER (WHERE o.date_month = 9) AS month_9_n,
    count(o.*) FILTER (WHERE o.date_month = 10) AS month_10_n,
    count(o.*) FILTER (WHERE o.date_month = 11) AS month_11_n,
    count(o.*) FILTER (WHERE o.date_month = 12) AS month_12_n
   FROM observations o
     LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
     LEFT JOIN it_comuni ON st_intersects(it_comuni.geom, o.geom)
     LEFT JOIN it_province ON st_intersects(it_province.geom, o.geom)
     LEFT JOIN it_regioni ON st_intersects(it_regioni.geom, o.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(habitat_natura2000.geom, o.geom)
     LEFT JOIN it_sic_zps ON st_intersects(it_sic_zps.geom, o.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = o.dataorigin_id
     LEFT JOIN institutions ON institutions.institution_id = dataorigins.institution_id
  WHERE b.is_synonym_by_taxonomy IS FALSE
  GROUP BY b.taxonomy_phylum
UNION ALL
 SELECT DISTINCT ON (b.taxonomy_kingdom) 'KINGDOM'::text AS taxon_rank,
    b.taxonomy_kingdom AS taxon,
    count(o.specie) AS observations_count,
    array_to_string(array_agg(DISTINCT dataorigins.origin_name), ','::text) AS origin,
    array_length(array_agg(DISTINCT dataorigins.origin_name), 1) AS origin_n,
    array_to_string(array_agg(DISTINCT habitat_natura2000.cod ORDER BY habitat_natura2000.cod), ','::text) AS habitat_natura2000,
    array_length(array_agg(DISTINCT habitat_natura2000.cod), 1) AS habitat_natura2000_n,
    array_to_string(array_agg(DISTINCT it_comuni.com_name ORDER BY it_comuni.com_name), ','::text) AS comuni,
    array_length(array_agg(DISTINCT it_comuni.com_name), 1) AS comuni_n,
    array_to_string(array_agg(DISTINCT it_province.den_prov ORDER BY it_province.den_prov), ','::text) AS province,
    array_length(array_agg(DISTINCT it_province.den_prov), 1) AS province_n,
    array_to_string(array_agg(DISTINCT it_regioni.den_reg ORDER BY it_regioni.den_reg), ','::text) AS regioni,
    array_length(array_agg(DISTINCT it_regioni.den_reg), 1) AS regioni_n,
    array_to_string(array_agg(DISTINCT it_sic_zps.code ORDER BY it_sic_zps.code), ','::text) AS sic_zps,
    array_length(array_agg(DISTINCT it_sic_zps.code), 1) AS sic_zps_n,
    count(o.*) FILTER (WHERE o.date_month = 1) AS month_1_n,
    count(o.*) FILTER (WHERE o.date_month = 2) AS month_2_n,
    count(o.*) FILTER (WHERE o.date_month = 3) AS month_3_n,
    count(o.*) FILTER (WHERE o.date_month = 4) AS month_4_n,
    count(o.*) FILTER (WHERE o.date_month = 5) AS month_5_n,
    count(o.*) FILTER (WHERE o.date_month = 6) AS month_6_n,
    count(o.*) FILTER (WHERE o.date_month = 7) AS month_7_n,
    count(o.*) FILTER (WHERE o.date_month = 8) AS month_8_n,
    count(o.*) FILTER (WHERE o.date_month = 9) AS month_9_n,
    count(o.*) FILTER (WHERE o.date_month = 10) AS month_10_n,
    count(o.*) FILTER (WHERE o.date_month = 11) AS month_11_n,
    count(o.*) FILTER (WHERE o.date_month = 12) AS month_12_n
   FROM observations o
     LEFT JOIN taxonomy_complete b ON b.observation_specie = o.specie
     LEFT JOIN it_comuni ON st_intersects(it_comuni.geom, o.geom)
     LEFT JOIN it_province ON st_intersects(it_province.geom, o.geom)
     LEFT JOIN it_regioni ON st_intersects(it_regioni.geom, o.geom)
     LEFT JOIN habitat_natura2000 ON st_intersects(habitat_natura2000.geom, o.geom)
     LEFT JOIN it_sic_zps ON st_intersects(it_sic_zps.geom, o.geom)
     LEFT JOIN dataorigins ON dataorigins.dataorigin_id = o.dataorigin_id
     LEFT JOIN institutions ON institutions.institution_id = dataorigins.institution_id
  WHERE b.is_synonym_by_taxonomy IS FALSE
  GROUP BY b.taxonomy_kingdom
WITH DATA;

ALTER TABLE data_biodiv.taxon_stats_complete_full
    OWNER TO biostreamadmin;


CREATE INDEX taxon_stats_complete_full_rank_idx
    ON data_biodiv.taxon_stats_complete_full USING btree
    (taxon_rank COLLATE pg_catalog."default")
    TABLESPACE pg_default;
CREATE INDEX taxon_stats_complete_full_taxon_idx
    ON data_biodiv.taxon_stats_complete_full USING btree
    (taxon COLLATE pg_catalog."default")
    TABLESPACE pg_default;