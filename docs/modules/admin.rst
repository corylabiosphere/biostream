Admin
=====

Generali
--------

.. automodule:: biostream.admin
    :members:

Biodiversità
------------

.. automodule:: biodiv.admin
    :members:

Richieste dati
--------------

.. automodule:: data_requests.admin
    :members:

Taxon
-----

.. automodule:: taxon.admin
    :members:

Profili
-------

.. automodule:: profiles.admin
    :members: