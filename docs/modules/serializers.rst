Serializzatori
==============

Generali
----------

.. automodule:: biostream.serializers
    :members:

Biodiversità
------------

.. automodule:: biodiv.serializers
    :members:

Mappe
-----

.. automodule:: map.serializers
    :members:

Taxon
-----

.. automodule:: taxon.serializers
    :members: