Viste
=====

Biodiversità
------------

.. automodule:: biodiv.views
    :members:

Richieste dati
--------------

.. automodule:: data_requests.views
    :members:

Mappa
-----

.. automodule:: map.views
    :members:

Profili
-------

.. automodule:: profiles.views
    :members: